-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 04, 2017 at 10:23 AM
-- Server version: 5.6.32-78.1
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chloemar_arduino`
--

-- --------------------------------------------------------

--
-- Table structure for table `Wellbeing_monitor`
--

CREATE TABLE `Wellbeing_monitor` (
  `Id` int(12) NOT NULL,
  `IntTemp` int(2) NOT NULL,
  `ExtTemp` int(2) NOT NULL,
  `HeartRate` int(3) NOT NULL,
  `Time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Wellbeing_monitor`
--

INSERT INTO `Wellbeing_monitor` (`Id`, `IntTemp`, `ExtTemp`, `HeartRate`, `Time`) VALUES
(138, 36, 26, 116, '2017-12-04 12:30:21'),
(137, 36, 24, 112, '2017-12-04 12:28:21'),
(136, 36, 26, 112, '2017-12-04 12:26:20'),
(135, 36, 24, 108, '2017-12-04 12:22:50'),
(134, 36, 24, 116, '2017-12-04 12:20:39'),
(133, 36, 24, 116, '2017-12-04 12:18:39'),
(132, 36, 24, 116, '2017-12-04 12:16:39'),
(131, 46, 24, 108, '2017-12-04 11:10:59'),
(130, 24, 24, 108, '2017-12-04 11:09:01'),
(129, 24, 24, 116, '2017-12-04 11:06:53'),
(128, 24, 46, 116, '2017-12-04 11:04:55'),
(127, 24, 24, 116, '2017-12-04 11:02:57'),
(126, 24, 24, 116, '2017-12-04 11:00:49'),
(125, 24, 24, 112, '2017-12-04 10:58:42'),
(124, 24, 24, 116, '2017-12-04 10:56:37'),
(123, 24, 24, 112, '2017-12-04 10:54:37'),
(122, 24, 24, 108, '2017-12-04 10:52:37'),
(121, 40, 24, 108, '2017-12-04 10:50:36'),
(120, 24, 24, 104, '2017-12-04 10:47:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Wellbeing_monitor`
--
ALTER TABLE `Wellbeing_monitor`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Wellbeing_monitor`
--
ALTER TABLE `Wellbeing_monitor`
  MODIFY `Id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
