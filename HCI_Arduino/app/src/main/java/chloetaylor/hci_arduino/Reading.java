package chloetaylor.hci_arduino;

/**
 * Created by Chloe on 04/12/2017.
 */

public class Reading {

    int IntTemp;
    int ExtTemp;
    int HeartRate;
    String Time;

    Reading(int IntTemp, int ExtTemp, int HeartRate, String TimeStamp){

        this.IntTemp = IntTemp;
        this.ExtTemp = ExtTemp;
        this.HeartRate = HeartRate;
        this.Time = Time;


    }

    Reading(){

    }

    public int getIntTemp() {
        return IntTemp;
    }

    public String getIntTempString()
    {
        String intTempString = Integer.toString(IntTemp) + "c";
        return intTempString;
    }


    public void setIntTemp(int intTemp) {
        IntTemp = intTemp;
    }

    public int getExtTemp() {
        return ExtTemp;
    }

    public String getExtTempString()
    {
        String extTempString = Integer.toString(ExtTemp) + "c";
        return extTempString;
    }

    public void setExtTemp(int extTemp) {
        ExtTemp = extTemp;
    }

    public int getHeartRate() {
        return HeartRate;
    }


    public String getHeartRateString()
    {
        String extHeartRateString = Integer.toString(HeartRate) + "bpm";
        return extHeartRateString;
    }

    public void setHeartRate(int heartRate) {
        HeartRate = heartRate;
    }

    public String getTime() {
        return Time;
    }

    public void setTimeStamp(String time) {
        Time = time;
    }
}
