package chloetaylor.hci_arduino;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    List<Reading> readingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Please confirm")
                        .setCancelable(false)
                        .setMessage("Are you sure you wish to refresh the data from the database?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int whichButton) {

                                makeShortToastMessage("Connecting to the online database...");
// action - reload database
                                new Thread(new Runnable() {
                                    public void run() {



                                        final OnlineSqlDatabaseHandler getdb = new OnlineSqlDatabaseHandler();


                                        final String data = getdb.getDataFromDB();

                                        final String response = parseJSON(data);



                                        runOnUiThread(new Runnable() {

                                            @Override
                                            public void run() {



                                                if (response.equals("success")) {

                                                    System.out.println("The data was successfully refreshed from the online database.");
                                                    makeShortToastMessage("The data was successfully refreshed from the online database.");

                                                    List<LinearLayout> rowList = new ArrayList<>();

                                                    LinearLayout row1 = (LinearLayout) findViewById(R.id.row1);
                                                    LinearLayout row2 = (LinearLayout) findViewById(R.id.row2);
                                                    LinearLayout row3 = (LinearLayout) findViewById(R.id.row3);
                                                    LinearLayout row4 = (LinearLayout) findViewById(R.id.row4);
                                                    LinearLayout row5 = (LinearLayout) findViewById(R.id.row5);
                                                    LinearLayout row6 = (LinearLayout) findViewById(R.id.row6);
                                                    LinearLayout row7 = (LinearLayout) findViewById(R.id.row7);
                                                    LinearLayout row8 = (LinearLayout) findViewById(R.id.row8);

                                                    rowList.add(row1);
                                                    rowList.add(row2);
                                                    rowList.add(row3);
                                                    rowList.add(row4);
                                                    rowList.add(row5);
                                                    rowList.add(row6);
                                                    rowList.add(row7);
                                                    rowList.add(row8);

                                                    Collections.sort(readingList, new Comparator<Reading>() {
                                                        @Override
                                                        public int compare(Reading o1, Reading o2) {
                                                            return o2.getTime().compareTo(o1.getTime());
                                                        }
                                                    });

                                                    int index = 0;

                                                    for (LinearLayout linearLayout : rowList){

                                                        TextView timeStamp = linearLayout.findViewById(R.id.timeStamp);
                                                        timeStamp.setText(readingList.get(index).getTime());
                                                        TextView extTemp = linearLayout.findViewById(R.id.extTemp);
                                                        setColourTextExtTemp(extTemp, readingList.get(index).getExtTemp());
                                                        extTemp.setText(readingList.get(index).getExtTempString());
                                                        TextView intTemp = linearLayout.findViewById(R.id.intTemp);
                                                        setColourTextIntTemp(intTemp, readingList.get(index).getIntTemp());
                                                        intTemp.setText(readingList.get(index).getIntTempString());
                                                        TextView heartRate = linearLayout.findViewById(R.id.heartRate);
                                                        setColourTextHeartRate(heartRate, readingList.get(index).getHeartRate());
                                                        heartRate.setText(readingList.get(index).getHeartRateString());

                                                        index = index + 1;

                                                    }


                                                } else if (response.equals("failure")){


                                                    System.out.println("There was a connection error.");
                                                    makeShortToastMessage("There was a connection error.");


                                                }

                                            }
                                        });

                                    }
                                }).start();


                            }
                        })
                        .setNegativeButton("No", null).show();

            }
        });


        readingList = new ArrayList<>();







    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void setColourTextIntTemp(TextView textView, int temp){



        int value = temp;

        int upperThreshold = 39;
        int lowerThreshold = 35;


        if (value >= upperThreshold){
            textView.setTextColor(getResources().getColor(R.color.customRed));
        } else if (value <= lowerThreshold){
            textView.setTextColor(getResources().getColor(R.color.customBlue));
        } else {
            textView.setTextColor(getResources().getColor(R.color.customGreen));
        }

    }

    private void setColourTextExtTemp(TextView textView, int temp){



        int value = temp;

        int upperThreshold = 26;
        int lowerThreshold = 18;


        if (value >= upperThreshold){
            textView.setTextColor(getResources().getColor(R.color.customRed));
        } else if (value <= lowerThreshold){
            textView.setTextColor(getResources().getColor(R.color.customBlue));
        } else {
            textView.setTextColor(getResources().getColor(R.color.customGreen));
        }

    }

    private void setColourTextHeartRate(TextView textView, int heartRate){


        int value = heartRate;

        int upperThreshold = 150;
        int lowerThreshold = 55;


        if (value >= upperThreshold){
            textView.setTextColor(getResources().getColor(R.color.customRed));
        } else if (value <= lowerThreshold){
            textView.setTextColor(getResources().getColor(R.color.customBlue));
        } else {
            textView.setTextColor(getResources().getColor(R.color.customGreen));
        }

    }


    private void makeToastMessage(String toastMessage) {

        Spannable centeredText = new SpannableString(toastMessage);
        centeredText.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
                0, toastMessage.length() - 1,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        Toast.makeText(getApplicationContext(), centeredText, Toast.LENGTH_LONG).show();

    }


    private void makeShortToastMessage(String toastMessage) {

        Spannable centeredText = new SpannableString(toastMessage);
        centeredText.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
                0, toastMessage.length() - 1,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        Toast.makeText(getApplicationContext(), centeredText, Toast.LENGTH_SHORT).show();

    }

    public String parseJSON(String result) {
        String returnMessage = "";

        List<Reading> tempList = new ArrayList<>();

        try {
            JSONArray jArray = new JSONArray(result);
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject json_data = jArray.getJSONObject(i);
                Reading reading = new Reading();
                reading.setIntTemp(json_data.getInt("IntTemp") );
                reading.setExtTemp(json_data.getInt("ExtTemp"));
                reading.setHeartRate(json_data.getInt("HeartRate"));
                reading.setTimeStamp(json_data.getString("Time"));
                tempList.add(reading);
                returnMessage = "success";
            }
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString());
            returnMessage = "failure";
        }

        if (returnMessage.equals("success")){
            readingList.clear();
            readingList = tempList;
        }

        return returnMessage;
    }

}