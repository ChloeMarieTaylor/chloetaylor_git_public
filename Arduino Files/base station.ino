/* 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Reciever - base station
     
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/

#include <bcconfig.h>
#include <BigNumber.h>
#include <number.h>

#include <Bridge.h>
#include <BridgeClient.h>
#include <BridgeServer.h>

BridgeClient client;

// DEFINE CHANNELS AND OPTION LINKS 
#define  OP1 2  // All channels are inputs on the Arduino when configured as a RECEIVER
#define  OP2 3
#define  OP3 4
#define  OP4 5
#define  OP5 6
#define  OP6 7
#define  OP7 8
#define  OP8 9





int prime1 = 19; // this normally would be hidden as it is the primes
int prime2 = 29;

BigNumber modulus = prime1 * prime2;
BigNumber phiM = (prime1 - 1)*(prime2 - 1); // not used but was needed to work out the keys

BigNumber decryptNumber = 101;  // this normally would be hidden as it is the unlock key
BigNumber encryptNumber = 5; 

  
  
byte receivedPacket = B00000000;     // Clear all inputs flags before use  

int check[] = {0,0,0};
int gapCounter = 0;

int intTemp;
int extTemp;
int heartRate;

String type[] = {"intTemp","extTemp","heartRate"};
  
void setup() {
  // put your setup code here, to run once:
  pinMode(2, INPUT);            // all outputs to control BRAVO in TX Mode 
  pinMode(3, INPUT);
  pinMode(4, INPUT);
  pinMode(5, INPUT);
  pinMode(6, INPUT);
  pinMode(7, INPUT);
  pinMode(8, INPUT);
  pinMode(9, INPUT);

  
  Serial.begin(19200);         // enable serial to see what button is pressed on a terminal program


  delay(5000);
  Bridge.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
  
if ((digitalRead(OP1)) == HIGH) { 

  gapCounter = 0;

  String receivedByte = "B1";

Serial.print("Packet Recieved - ");
String thisType = "";     
    
  
  
   if(digitalRead(OP2) == LOW){
    bitSet(receivedPacket,6);
    receivedByte = receivedByte + "1";
    } else {
      bitClear(receivedPacket,6);
      receivedByte = receivedByte + "0";
    }
  
  
 if(digitalRead(OP3) == LOW){
    bitSet(receivedPacket,5);
    receivedByte = receivedByte + "1";
    } else {
      bitClear(receivedPacket,5);
      receivedByte = receivedByte + "0";
    }

  int op2 = bitRead(receivedPacket,6);
  int op3 = bitRead(receivedPacket,5);
  
  if (op2 == 0 && op3 == 1){
      thisType = type[0];
  } else if (op2 == 1 && op3 == 0){
    thisType = type[1];
  } else if (op2 == 1 && op3 == 1){
    thisType = type[2];
  }
  
  Serial.println(thisType);
  if (!thisType.equals("")){
  
    if(digitalRead(OP4) == LOW){
    bitSet(receivedPacket,4);
    receivedByte = receivedByte + "1";
    } else {
      bitClear(receivedPacket,4);
      receivedByte = receivedByte + "0";
    }
  
   if(digitalRead(OP5) == LOW){
    bitSet(receivedPacket,3);
    receivedByte = receivedByte + "1";
    } else {
      bitClear(receivedPacket,3);
      receivedByte = receivedByte + "0";
    }
  
   if(digitalRead(OP6) == LOW){
    bitSet(receivedPacket,2);
    receivedByte = receivedByte + "1";
    } else {
      bitClear(receivedPacket,2);
      receivedByte = receivedByte + "0";
    }
  
    
    if(digitalRead(OP7) == LOW){
    bitSet(receivedPacket,1);
    receivedByte = receivedByte + "1";
    } else {
      bitClear(receivedPacket,1);
      receivedByte = receivedByte + "0";
    }
  
    
    if(digitalRead(OP8) == LOW){
    bitSet(receivedPacket,0);
    receivedByte = receivedByte + "1";
    } else {
      bitClear(receivedPacket,0);
      receivedByte = receivedByte + "0";
    }

    Serial.println(receivedByte);
  
  int value = 0;
  
  if (bitRead(receivedPacket,4) == 1){
  value = value + 16;
  }
  if (bitRead(receivedPacket,3) == 1){
  value = value + 8;
  }
  if (bitRead(receivedPacket,2) == 1){
  value = value + 4;
  }
  if (bitRead(receivedPacket,1) == 1){
  value = value + 2;
  }
  if (bitRead(receivedPacket,0) == 1){
  value = value + 1;
  }
    
  if (thisType.equals(type[0])){
  value = value * 2;
  check[0] = 1;
  intTemp = value;
  } else if (thisType.equals(type[1])){
  value = value * 2;
  check[1] = 1;
  extTemp = value;
  } else if (thisType.equals(type[2])){
  value = value * 4;
  check[2] = 1;
  heartRate = value;
  }

  
  checkFullSet(thisType, value);
  
  
  }

delay(30000);

    
  } else {
    gapCounter = gapCounter + 1;
    Serial.println("---- " + String(gapCounter));
    if (gapCounter > 8) {
      check[0] = 0;
      check[1] = 0;
      check[2] = 0;
    }
    delay(10000);
  }

}

void checkFullSet(String type, int value){

  Serial.println("type = " + type);
Serial.println("value = " + String(value));


  if (check[0] ==1 && check[1] == 1 && check[2] == 1){

    Serial.println("Full dataset...");
    
      check[0] = 0;
      check[1] = 0;
      check[2] = 0;

      submitToDatabase();

    
  } else {
    
Serial.println("Still collecting data...");

    }


}

void submitToDatabase(){

// create JSON string read for encrypting

BigNumber totalUnconverted = (intTemp /2) + (extTemp * 5) + (heartRate / 4);

BigNumber total = ((totalUnconverted.pow(encryptNumber)) % modulus) * modulus;

String unencryptedJSON = "{\"check\":\"" + String(total) + "\",\"intTemp\":\"" + String(intTemp) + "\",\"extTemp\":\"" + String(extTemp) + "\",\"heartRate\":\"" + String(heartRate) + "\"}";

 Serial.println(unencryptedJSON);
String stringNumber = StringToInt8(unencryptedJSON);

httpRequest(stringNumber);

}




String StringToInt8 (String message){

  Serial.println("calculating stringNumber");

int messageLength = message.length();

String numberString = "";

for (int i = 0; i < messageLength; i++){


int dec = (int8_t)message[i];

String hexNumberString = String(dec, HEX);

numberString = numberString + hexNumberString;

  }

Serial.println(numberString);


return numberString;
  
}


void httpRequest(String data) {

  String postedData = "payload=" + data;

  

  Serial.println(postedData);
    
  // close any connection before send a new request.
  // This will free the socket on the WiFi shield

  // if there's a successful connection:
  
  if (client.connect("www.chloemarietaylor.com",80)) {
Serial.println("connected");

client.println("POST /--DoNotDelete--/Arduino/insertData.php HTTP/1.1");
  client.println("Host: www.chloemarietaylor.com");
  client.println("User-Agent: Arduino/1.0");
  client.println("Connection: close");
  client.println("Content-Type: application/x-www-form-urlencoded;");
  client.print("Content-Length: ");
  client.println(postedData.length());
  client.println();
  client.println(postedData);

delay(2000);

 Serial.println("Data sent");
 
while(client.available()) {
    String line = client.readStringUntil('\r');
    Serial.println(line);
  }

  delay(2000);

if(client.connected()){
  Serial.println("disconnecting.");
}
  
} else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
  }

client.flush();
  delay(5000);
  
client.stop();   //disconnect from server
}



