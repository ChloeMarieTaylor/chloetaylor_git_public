/* 
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Transmitter - Watch
     
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/



#define  CH1 2  // All channels are outputs on the Arduino when configured as a transmitter
#define  CH2 3
#define  CH3 4
#define  CH4 5
#define  CH5 6
#define  CH6 7
#define  CH7 8
#define  CH8 9

int clockPin = 12; //chip pin 11
int latchPin = 11; //chip pin 12
int dataPin = 10; //chip pin 14

int pulseSwitch = 13; 




                       //       0    1      2            3   4      5       6       7
byte leds = B00000000; // rgb1(red, Blue, Green), rgb2(red, Blue, Green), green, yellow

byte tempInternal = B00000000;
byte tempExternal = B00000000;
byte pulseRate = B00000000;

float intTempC;
int intReading;
const int intTempPin = 1;

float extTempC;
int extReading;
const int extTempPin = 2;

const float intTempCorrection = 12.98;

int prevCount=1;

int countbeats[] = {
  0, 0, 0};
int prevbeat[] = {
  0, 0, 0};

//VARIABLES
int pulsePin = 0;  

int SignalAnalogue;                // holds the incoming raw data. Signal value can range from 0-1024
int Threshold = 550; // Determine which Signal to "count as a beat", and which to ingore.

// these variables are volatile because they are used during the interrupt service routine!
volatile int BPM;                   // used to hold the pulse rate
volatile int Signal;                // holds the incoming raw data
volatile int IBI = 2000;             // holds the time between beats, must be seeded!
volatile boolean Pulse = false;     // true when pulse wave is high, false when it's low
volatile boolean QS = false;        // becomes true when Arduoino finds a beat.

 
void setup()
{

interruptSetup();

  
  pinMode(2, OUTPUT);    // all outputs to control GAMMA-868 in TX Mode 
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);  

  pinMode(13, OUTPUT);  

  digitalWrite(CH1, HIGH);    // defalt start values for all inputs on GAMMA   
  digitalWrite(CH2, HIGH);
  digitalWrite(CH3, HIGH);
  digitalWrite(CH4, HIGH);
  digitalWrite(CH5, HIGH);
  digitalWrite(CH6, HIGH);
  digitalWrite(CH7, HIGH);
  digitalWrite(CH8, HIGH);

  Serial.begin(19200);

   

// clear register and set to all off
  pinMode(latchPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
 

  //power on
  updateRedLED(1); // power on
  updateShiftRegister();

  switchPulseSensorEnable(false);

  Serial.print("Initializing...   ");
  for (int j = 5; j > 0; j = j - 1) {
     delay(500);
   updateRedLED(0);
  delay(500);
   updateRedLED(1);
   Serial.print(String(j) + "...   ");
  }
delay(1000);
Serial.println("Ready!");
Serial.println("");
delay(1000);

  
  intReading = analogRead(intTempPin); // make an initial reading to set up the ADC
intReading = analogRead(intTempPin);  // toss the first reading and take one we will keep
intTempC = (5.0 * intReading * 100.0)/1024.0 + intTempCorrection;

extReading = analogRead(extTempPin); // make an initial reading to set up the ADC
extReading = analogRead(extTempPin);  // toss the first reading and take one we will keep
extTempC = (5.0 * extReading * 100.0)/1024.0;

if (intTempC > 39){
  updateInternalTempLED(1, 0, 0);
} else if (intTempC < 35){
  updateInternalTempLED(0, 1, 0);
} else {
  updateInternalTempLED(0, 0, 1);
}

if (extTempC > 26){
  updateExternalTempLED(1, 0, 0);
} else if (extTempC < 18){
  updateExternalTempLED(0, 1, 0);
} else {
  updateExternalTempLED(0, 0, 1);
}

updateShiftRegister();
 
}


 
void loop()
{

intReading = analogRead(intTempPin); // make an initial reading to set up the ADC
intReading = analogRead(intTempPin);  // toss the first reading and take one we will keep


intTempC = (5.0 * intReading * 100.0)/1024.0 + intTempCorrection;
Serial.println("internal temp = " + String(intTempC));
delay(1000);

if (intTempC > 39){
  updateInternalTempLED(1, 0, 0);
} else if (intTempC < 35){
  updateInternalTempLED(0, 1, 0);
} else {
  updateInternalTempLED(0, 0, 1);
}

if (intTempC < 0){
intTempC = 0;
} else if (intTempC > 62){
intTempC = 62;
}


updateShiftRegister();
sendInternalTemp(intTempC);

extReading = analogRead(extTempPin); // make an initial reading to set up the ADC
extReading = analogRead(extTempPin);  // toss the first reading and take one we will keep



extTempC = (5.0 * extReading * 100.0)/1024.0;
Serial.println("external temp = " + String(extTempC));
delay(1000);

if (extTempC > 26){
  updateExternalTempLED(1, 0, 0);
} else if (extTempC < 18){
  updateExternalTempLED(0, 1, 0);
} else {
  updateExternalTempLED(0, 0, 1);
}

if (extTempC < 0){
extTempC = 0;
} else if (extTempC > 63){
extTempC = 63;
}

sendExternalTemp(extTempC);

switchPulseSensorEnable(true);
int heartRate = heartBeat();
delay(30000);


heartRate = heartBeat();

Serial.println("heartRate = " + String(heartRate));

switchPulseSensorEnable(false);


if (heartRate < 0){
heartRate = 0;
} else if (heartRate > 248){
heartRate = 248;
}

updateShiftRegister();

 
 
 
 sendHeartRate(heartRate);
 
 
}

void sendInternalTemp(float intTempC){

  float value = intTempC /2;

 int intValue = round(value);

  Serial.println(intValue);
 
 if (intValue >= 16){
  bitSet(tempInternal,4);
  intValue = intValue - 16;
 }
 if (intValue >= 8){
  bitSet(tempInternal,3);
  intValue = intValue - 8;
 }
 if (intValue >= 4){
  bitSet(tempInternal,2);
  intValue = intValue - 4;
 }
 if (intValue >= 2){
  bitSet(tempInternal,1);
  intValue = intValue - 2;
 }
 if (intValue >= 1){
  bitSet(tempInternal,0);
 }

// flag CFFXXXXX    clock C, flag 01, XXXXX value

bitClear(tempInternal, 6);
bitSet(tempInternal, 5);

transmit(tempInternal);
tempInternal = B00000000;

}

void sendExternalTemp(float extTempC){

 //round to nearest value
 float value = extTempC /2;

 int intValue = round(value);

  Serial.println(intValue);
 
 if (intValue >= 16){
  bitSet(tempExternal,4);
  intValue = intValue - 16;
 }
 if (intValue >= 8){
  bitSet(tempExternal,3);
  intValue = intValue - 8;
 }
 if (intValue >= 4){
  bitSet(tempExternal,2);
  intValue = intValue - 4;
 }
 if (intValue >= 2){
  bitSet(tempExternal,1);
  intValue = intValue - 2;
 }
 if (intValue >= 1){
  bitSet(tempExternal,0);
 }

// flag CFFXXXXX    clock C, flag 10, XXXXX value

bitSet(tempExternal, 6);
bitClear(tempExternal, 5);


transmit(tempExternal);
tempExternal = B00000000;

}

void sendHeartRate(float heartRate){

 //round to nearest value
 float value = heartRate /8;

 int intValue = round(value);

  Serial.println(intValue);
 
 if (intValue >= 16){
  bitSet(pulseRate,4);
  intValue = intValue - 16;
 }
 if (intValue >= 8){
  bitSet(pulseRate,3);
  intValue = intValue - 8;
 }
 if (intValue >= 4){
  bitSet(pulseRate,2);
  intValue = intValue - 4;
 }
 if (intValue >= 2){
  bitSet(pulseRate,1);
  intValue = intValue - 2;
 }
 if (intValue >= 1){
  bitSet(pulseRate,0);
 }

// flag CFFXXXXX    clock C, flag 11, XXXXX value

bitSet(pulseRate, 6);
bitSet(pulseRate, 5);


transmit(pulseRate);
pulseRate = B00000000;

}

void transmit(byte data){

Serial.print("Transmitted - B1");
 
   if (bitRead(data, 6) == 1){
      digitalWrite(CH2, HIGH);
      Serial.print("1");
   } else {
    digitalWrite(CH2,  LOW);
      Serial.print("0");
   }
     
  if (bitRead(data, 5) == 1){
      digitalWrite(CH3, HIGH);
      Serial.print("1");
   } else {
    digitalWrite(CH3,  LOW);
      Serial.print("0");
   }
     
  if (bitRead(data, 4) == 1){
      digitalWrite(CH4, HIGH);
      Serial.print("1");
   } else {
    digitalWrite(CH4,  LOW);
      Serial.print("0");
   }
     
  if (bitRead(data, 3) == 1){
      digitalWrite(CH5, HIGH);
      Serial.print("1");
   } else {
    digitalWrite(CH5,  LOW);
      Serial.print("0");
   }
     
  if (bitRead(data, 2) == 1){
      digitalWrite(CH6, HIGH);
      Serial.print("1");
   } else {
    digitalWrite(CH6,  LOW);
      Serial.print("0");
   }
     
  if (bitRead(data, 1) == 1){
      digitalWrite(CH7, HIGH);
      Serial.print("1");
   } else {
    digitalWrite(CH7,  LOW);
      Serial.print("0");
   }
     
  if (bitRead(data, 0) == 1){
      digitalWrite(CH8, HIGH);
      Serial.println("1");
   } else {
    digitalWrite(CH8,  LOW);
      Serial.println("0");
   }

     //flip the finished flag


      digitalWrite(CH1, LOW);

      Serial.println("Transmitting window...");
   
     for (int j = 30; j > 0; j = j - 1) {
     delay(500);
   updateYellowLED(0);
  delay(500);
   updateYellowLED(1);
   if (j % 5 == 0 ) {
   Serial.print(String(j) + "...   ");
   }
  }
  Serial.println("Continuing!");
   
   digitalWrite(CH1,  HIGH);

  
}


 
void updateShiftRegister(){
   digitalWrite(latchPin, LOW);
   shiftOut(dataPin, clockPin, MSBFIRST, leds);
   digitalWrite(latchPin, HIGH);
}

void updateExternalTempLED(int red, int blue, int green){
if (red == 1){
  bitSet(leds,0);
} else {
  bitClear(leds,0);
}

if (blue == 1){
  bitSet(leds,1);
} else {
  bitClear(leds,1);
}

if (green == 1){
  bitSet(leds,2);
} else {
  bitClear(leds,2);
}

  updateShiftRegister();
   
}

void updateInternalTempLED(int red, int blue, int green){
if (red == 1){
  bitSet(leds,3);
} else {
  bitClear(leds,3);
}

if (blue == 1){
  bitSet(leds,4);
} else {
  bitClear(leds,4);
}

if (green == 1){
  bitSet(leds,5);
} else {
  bitClear(leds,5);
}

  updateShiftRegister();
   
}

void switchPulseSensorEnable(bool state) {

if (state == true) {

digitalWrite(pulseSwitch, HIGH);
updateYellowLED(1);
//interruptSetup();  // sets up to read Pulse Sensor signal every 2mS
} else {
digitalWrite(pulseSwitch, LOW);
updateYellowLED(0);
}
}

void updateRedLED(int Red){

if (Red == 1){
  bitSet(leds,7);
} else {
  bitClear(leds,7);
}

  updateShiftRegister();
   
}

void updateYellowLED(int yellow){

if (yellow == 1){
  bitSet(leds,6);
} else {
  bitClear(leds,6);
}

  updateShiftRegister();
   
}

// pulse readings

int heartBeat(){
  
if (QS == true){ 

    countbeats[2] = BPM % 10;
    //How to handle the middle digit depends on if the
    //the speed is a two or three digit number
    if(BPM > 99){
      countbeats[1] = (BPM / 10) % 10;
    }
    else{
      countbeats[1] = BPM / 10;
    }
    //Grab the first digit
    countbeats[0] = BPM / 100;
   
    prevbeat[2] = prevCount % 10;
    if(prevCount > 99){
      prevbeat[1] = (prevCount / 10) % 10;
    }
    else{
      prevbeat[1] = prevCount / 10;
    }
    prevbeat[0] = prevCount / 100;
 
    QS = false;  

    return BPM;
  }
}


volatile int rate[10];                    // array to hold last ten IBI values
volatile unsigned long sampleCounter = 0;          // used to determine pulse timing
volatile unsigned long lastBeatTime = 0;           // used to find IBI
volatile int P =512;                      // used to find peak in pulse wave, seeded
volatile int T = 512;                     // used to find trough in pulse wave, seeded
volatile int thresh = 512;                // used to find instant moment of heart beat, seeded
volatile int amp = 100;                   // used to hold amplitude of pulse waveform, seeded
volatile boolean firstBeat = true;        // used to seed rate array so we startup with reasonable BPM
volatile boolean secondBeat = false;      // used to seed rate array so we startup with reasonable BPM



void interruptSetup(){    
//   Initializes Timer2 to throw an interrupt every 2mS.
  TCCR2A = 0x02;     // DISABLE PWM ON DIGITAL PINS 3 AND 11, AND GO INTO CTC MODE
  TCCR2B = 0x06;     // DON'T FORCE COMPARE, 256 PRESCALER
  OCR2A = 0X7C;      // SET THE TOP OF THE COUNT TO 124 FOR 500Hz SAMPLE RATE
  TIMSK2 = 0x02;     // ENABLE INTERRUPT ON MATCH BETWEEN TIMER2 AND OCR2A
  sei();             // MAKE SURE GLOBAL INTERRUPTS ARE ENABLED    
}

// THIS IS THE TIMER 2 INTERRUPT SERVICE ROUTINE.
// Timer 2 makes sure that we take a reading every 2 miliseconds
ISR(TIMER2_COMPA_vect){                         // triggered when Timer2 counts to 124
  cli();                                      // disable interrupts while we do this
  Signal = analogRead(pulsePin);              // read the Pulse Sensor
  sampleCounter += 2;                         // keep track of the time in mS with this variable
  int N = sampleCounter - lastBeatTime;       // monitor the time since the last beat to avoid noise

    //  find the peak and trough of the pulse wave
  if(Signal < thresh && N > (IBI/5)*3){       // avoid dichrotic noise by waiting 3/5 of last IBI
    if (Signal < T){                        // T is the trough
      T = Signal;                         // keep track of lowest point in pulse wave
    }
  }

  if(Signal > thresh && Signal > P){          // thresh condition helps avoid noise
    P = Signal;                             // P is the peak
  }                                        // keep track of highest point in pulse wave

  //  NOW IT'S TIME TO LOOK FOR THE HEART BEAT
  // signal surges up in value every time there is a pulse
  if (N > 250){                                   // avoid high frequency noise
    if ( (Signal > thresh) && (Pulse == false) && (N > (IBI/5)*3) ){       
      Pulse = true;                               // set the Pulse flag when we think there is a pulse

        IBI = sampleCounter - lastBeatTime;         // measure time between beats in mS
      lastBeatTime = sampleCounter;               // keep track of time for next pulse

      if(secondBeat){                        // if this is the second beat, if secondBeat == TRUE
        secondBeat = false;                  // clear secondBeat flag
        for(int i=0; i<=9; i++){             // seed the running total to get a realisitic BPM at startup
          rate[i] = IBI;                     
        }
      }

      if(firstBeat){                         // if it's the first time we found a beat, if firstBeat == TRUE
        firstBeat = false;                   // clear firstBeat flag
        secondBeat = true;                   // set the second beat flag
        sei();                               // enable interrupts again
        return;                              // IBI value is unreliable so discard it
      }  


      // keep a running total of the last 10 IBI values
      word runningTotal = 0;                  // clear the runningTotal variable   

      for(int i=0; i<=8; i++){                // shift data in the rate array
        rate[i] = rate[i+1];                  // and drop the oldest IBI value
        runningTotal += rate[i];              // add up the 9 oldest IBI values
      }

      rate[9] = IBI;                          // add the latest IBI to the rate array
      runningTotal += rate[9];                // add the latest IBI to runningTotal
      runningTotal /= 10;                     // average the last 10 IBI values
      BPM = 60000/runningTotal;               // how many beats can fit into a minute? that's BPM!
      QS = true;                              // set Quantified Self flag
      // QS FLAG IS NOT CLEARED INSIDE THIS ISR
    }                      
  }

  if (Signal < thresh && Pulse == true){   // when the values are going down, the beat is over

    Pulse = false;                         // reset the Pulse flag so we can do it again
    amp = P - T;                           // get amplitude of the pulse wave
    thresh = amp/2 + T;                    // set thresh at 50% of the amplitude
    P = thresh;                            // reset these for next time
    T = thresh;
  }

  if (N > 2500){                           // if 2.5 seconds go by without a beat
    thresh = 512;                          // set thresh default
    P = 512;                               // set P default
    T = 512;                               // set T default
    lastBeatTime = sampleCounter;          // bring the lastBeatTime up to date       
    firstBeat = true;                      // set these to avoid noise
    secondBeat = false;                    // when we get the heartbeat back
  }

  sei();                                   // enable interrupts when youre done!
}// end isr