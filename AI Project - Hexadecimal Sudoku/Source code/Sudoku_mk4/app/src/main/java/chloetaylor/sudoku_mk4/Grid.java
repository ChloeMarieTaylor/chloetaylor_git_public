package chloetaylor.sudoku_mk4;

import android.app.Activity;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

/**
 * Created by Chloe on 26/02/2018.
 */

public class Grid extends Activity implements Serializable {

    private ArrayList<GridButton> gridButtonArrayList = new ArrayList<>();

    private LinkedList<byte[]> gameStateLinkedList = new LinkedList<>();

    private TextView saveStateIterationTextView;
    private TextView totalIterationTextView;

    private ArrayList<Square> allSquares;
    private ArrayList<Square> allSquaresReversedOrder;

    private ArrayList<Square> cell_0;
    private ArrayList<Square> cell_1;
    private ArrayList<Square> cell_2;
    private ArrayList<Square> cell_3;
    private ArrayList<Square> cell_4;
    private ArrayList<Square> cell_5;
    private ArrayList<Square> cell_6;
    private ArrayList<Square> cell_7;
    private ArrayList<Square> cell_8;
    private ArrayList<Square> cell_9;
    private ArrayList<Square> cell_A;
    private ArrayList<Square> cell_B;
    private ArrayList<Square> cell_C;
    private ArrayList<Square> cell_D;
    private ArrayList<Square> cell_E;
    private ArrayList<Square> cell_F;


    private ArrayList<Square> row_0;
    private ArrayList<Square> row_1;
    private ArrayList<Square> row_2;
    private ArrayList<Square> row_3;
    private ArrayList<Square> row_4;
    private ArrayList<Square> row_5;
    private ArrayList<Square> row_6;
    private ArrayList<Square> row_7;
    private ArrayList<Square> row_8;
    private ArrayList<Square> row_9;
    private ArrayList<Square> row_A;
    private ArrayList<Square> row_B;
    private ArrayList<Square> row_C;
    private ArrayList<Square> row_D;
    private ArrayList<Square> row_E;
    private ArrayList<Square> row_F;


    private ArrayList<Square> column_0;
    private ArrayList<Square> column_1;
    private ArrayList<Square> column_2;
    private ArrayList<Square> column_3;
    private ArrayList<Square> column_4;
    private ArrayList<Square> column_5;
    private ArrayList<Square> column_6;
    private ArrayList<Square> column_7;
    private ArrayList<Square> column_8;
    private ArrayList<Square> column_9;
    private ArrayList<Square> column_A;
    private ArrayList<Square> column_B;
    private ArrayList<Square> column_C;
    private ArrayList<Square> column_D;
    private ArrayList<Square> column_E;
    private ArrayList<Square> column_F;


    private ArrayList<ArrayList<Square>> cell_row_0;
    private ArrayList<ArrayList<Square>> cell_row_1;
    private ArrayList<ArrayList<Square>> cell_row_2;
    private ArrayList<ArrayList<Square>> cell_row_3;


    private ArrayList<ArrayList<Square>> cell_column_0;
    private ArrayList<ArrayList<Square>> cell_column_1;
    private ArrayList<ArrayList<Square>> cell_column_2;
    private ArrayList<ArrayList<Square>> cell_column_3;


    // singleton

    private static Grid instance = null;

    private Grid() {
        // Exists only to defeat instantiation.
    }


    public static Grid getInstance() {
        if (instance == null) {
            instance = new Grid();
        }
        return instance;
    }


    public void setSavedStateIterationTextView(TextView saveStateIterationTextView) {
        this.saveStateIterationTextView = saveStateIterationTextView;
    }

    public void setTotalIterationTextView(TextView totalIterationTextView) {
        this.totalIterationTextView = totalIterationTextView;
    }

    public ArrayList<GridButton> getGridButtonArrayList() {
        return gridButtonArrayList;
    }

    public void setGridButtonArrayList(ArrayList<GridButton> gridButtonArrayList) {

        // should not be needed but just in case, order by square number so it can be used as an index for quick finding

        Collections.sort(gridButtonArrayList, new Comparator<GridButton>() {

            public int compare(GridButton o1, GridButton o2) {
                return Integer.valueOf(o1.getSquareNumber()).compareTo(Integer.valueOf(o2.getSquareNumber()));
            }
        });


        this.gridButtonArrayList = gridButtonArrayList;
    }

    protected void setAllLists(ArrayList<Square> allSquares) {


        this.allSquares = allSquares;

        cell_0 = new ArrayList<>();
        cell_1 = new ArrayList<>();
        cell_2 = new ArrayList<>();
        cell_3 = new ArrayList<>();
        cell_4 = new ArrayList<>();
        cell_5 = new ArrayList<>();
        cell_6 = new ArrayList<>();
        cell_7 = new ArrayList<>();
        cell_8 = new ArrayList<>();
        cell_9 = new ArrayList<>();
        cell_A = new ArrayList<>();
        cell_B = new ArrayList<>();
        cell_C = new ArrayList<>();
        cell_D = new ArrayList<>();
        cell_E = new ArrayList<>();
        cell_F = new ArrayList<>();


        row_0 = new ArrayList<>();
        row_1 = new ArrayList<>();
        row_2 = new ArrayList<>();
        row_3 = new ArrayList<>();
        row_4 = new ArrayList<>();
        row_5 = new ArrayList<>();
        row_6 = new ArrayList<>();
        row_7 = new ArrayList<>();
        row_8 = new ArrayList<>();
        row_9 = new ArrayList<>();
        row_A = new ArrayList<>();
        row_B = new ArrayList<>();
        row_C = new ArrayList<>();
        row_D = new ArrayList<>();
        row_E = new ArrayList<>();
        row_F = new ArrayList<>();


        column_0 = new ArrayList<>();
        column_1 = new ArrayList<>();
        column_2 = new ArrayList<>();
        column_3 = new ArrayList<>();
        column_4 = new ArrayList<>();
        column_5 = new ArrayList<>();
        column_6 = new ArrayList<>();
        column_7 = new ArrayList<>();
        column_8 = new ArrayList<>();
        column_9 = new ArrayList<>();
        column_A = new ArrayList<>();
        column_B = new ArrayList<>();
        column_C = new ArrayList<>();
        column_D = new ArrayList<>();
        column_E = new ArrayList<>();
        column_F = new ArrayList<>();


        cell_row_0 = new ArrayList<>();
        cell_row_1 = new ArrayList<>();
        cell_row_2 = new ArrayList<>();
        cell_row_3 = new ArrayList<>();


        cell_column_0 = new ArrayList<>();
        cell_column_1 = new ArrayList<>();
        cell_column_2 = new ArrayList<>();
        cell_column_3 = new ArrayList<>();


        this.allSquares = allSquares;

        // should not be needed but just in case, order by square number so it can be used as an index for quick finding

        Collections.sort(allSquares, new Comparator<Square>() {

            public int compare(Square o1, Square o2) {
                return Integer.valueOf(o1.getSquareNumber()).compareTo(Integer.valueOf(o2.getSquareNumber()));
            }
        });


        allSquaresReversedOrder = (ArrayList<Square>) allSquares.clone();

        // reversed order for when doing the verify, should speed up backtracking with certain heuristic methods, especially square number order

        Collections.sort(allSquaresReversedOrder, new Comparator<Square>() {

            public int compare(Square o1, Square o2) {
                return Integer.valueOf(o2.getSquareNumber()).compareTo(Integer.valueOf(o1.getSquareNumber()));
            }
        });


        for (Square square : allSquares) {

            // cells
            switch (square.getCellNumber()) {

                case 0:
                    cell_0.add(square);
                    break;
                case 1:
                    cell_1.add(square);
                    break;
                case 2:
                    cell_2.add(square);
                    break;
                case 3:
                    cell_3.add(square);
                    break;
                case 4:
                    cell_4.add(square);
                    break;
                case 5:
                    cell_5.add(square);
                    break;
                case 6:
                    cell_6.add(square);
                    break;
                case 7:
                    cell_7.add(square);
                    break;
                case 8:
                    cell_8.add(square);
                    break;
                case 9:
                    cell_9.add(square);
                    break;
                case 10:
                    cell_A.add(square);
                    break;
                case 11:
                    cell_B.add(square);
                    break;
                case 12:
                    cell_C.add(square);
                    break;
                case 13:
                    cell_D.add(square);
                    break;
                case 14:
                    cell_E.add(square);
                    break;
                case 15:
                    cell_F.add(square);
                    break;
                default:
                    break;
            }

            // rows
            switch (square.getGridRowNumber()) {

                case 0:
                    row_0.add(square);
                    break;
                case 1:
                    row_1.add(square);
                    break;
                case 2:
                    row_2.add(square);
                    break;
                case 3:
                    row_3.add(square);
                    break;
                case 4:
                    row_4.add(square);
                    break;
                case 5:
                    row_5.add(square);
                    break;
                case 6:
                    row_6.add(square);
                    break;
                case 7:
                    row_7.add(square);
                    break;
                case 8:
                    row_8.add(square);
                    break;
                case 9:
                    row_9.add(square);
                    break;
                case 10:
                    row_A.add(square);
                    break;
                case 11:
                    row_B.add(square);
                    break;
                case 12:
                    row_C.add(square);
                    break;
                case 13:
                    row_D.add(square);
                    break;
                case 14:
                    row_E.add(square);
                    break;
                case 15:
                    row_F.add(square);
                    break;
                default:
                    break;
            }

            // columns
            switch (square.getGridColumnNumber()) {

                case 0:
                    column_0.add(square);
                    break;
                case 1:
                    column_1.add(square);
                    break;
                case 2:
                    column_2.add(square);
                    break;
                case 3:
                    column_3.add(square);
                    break;
                case 4:
                    column_4.add(square);
                    break;
                case 5:
                    column_5.add(square);
                    break;
                case 6:
                    column_6.add(square);
                    break;
                case 7:
                    column_7.add(square);
                    break;
                case 8:
                    column_8.add(square);
                    break;
                case 9:
                    column_9.add(square);
                    break;
                case 10:
                    column_A.add(square);
                    break;
                case 11:
                    column_B.add(square);
                    break;
                case 12:
                    column_C.add(square);
                    break;
                case 13:
                    column_D.add(square);
                    break;
                case 14:
                    column_E.add(square);
                    break;
                case 15:
                    column_F.add(square);
                    break;
                default:
                    break;
            }

        }
        // cell row

        cell_row_0.add(cell_0);
        cell_row_0.add(cell_1);
        cell_row_0.add(cell_2);
        cell_row_0.add(cell_3);

        cell_row_1.add(cell_4);
        cell_row_1.add(cell_5);
        cell_row_1.add(cell_6);
        cell_row_1.add(cell_7);

        cell_row_2.add(cell_8);
        cell_row_2.add(cell_9);
        cell_row_2.add(cell_A);
        cell_row_2.add(cell_B);

        cell_row_3.add(cell_C);
        cell_row_3.add(cell_D);
        cell_row_3.add(cell_E);
        cell_row_3.add(cell_F);

        // cell column

        cell_column_0.add(cell_0);
        cell_column_0.add(cell_4);
        cell_column_0.add(cell_8);
        cell_column_0.add(cell_C);

        cell_column_1.add(cell_1);
        cell_column_1.add(cell_5);
        cell_column_1.add(cell_9);
        cell_column_1.add(cell_D);

        cell_column_2.add(cell_2);
        cell_column_2.add(cell_6);
        cell_column_2.add(cell_A);
        cell_column_2.add(cell_E);

        cell_column_3.add(cell_3);
        cell_column_3.add(cell_7);
        cell_column_3.add(cell_B);
        cell_column_3.add(cell_F);



    }


    protected ArrayList<Square> getAllSquaresOrderedBySquareNumber() {

        return allSquares;

    }

    public ArrayList<Square> getAllSquaresOrderedBySquareNumberReversedOrder() {
        return allSquaresReversedOrder;
    }

    protected ArrayList<ArrayList<Square>> getAllSquaresSeperatedByColumns() {

        ArrayList<ArrayList<Square>> returnArrayList = new ArrayList<>();

        returnArrayList.add(column_0);
        returnArrayList.add(column_1);
        returnArrayList.add(column_2);
        returnArrayList.add(column_3);
        returnArrayList.add(column_4);
        returnArrayList.add(column_5);
        returnArrayList.add(column_6);
        returnArrayList.add(column_7);
        returnArrayList.add(column_8);
        returnArrayList.add(column_9);
        returnArrayList.add(column_A);
        returnArrayList.add(column_B);
        returnArrayList.add(column_C);
        returnArrayList.add(column_D);
        returnArrayList.add(column_E);
        returnArrayList.add(column_F);


        return returnArrayList;

    }

    protected ArrayList<ArrayList<Square>> getAllSquaresSeperatedByRows() {

        ArrayList<ArrayList<Square>> returnArrayList = new ArrayList<>();

        returnArrayList.add(row_0);
        returnArrayList.add(row_1);
        returnArrayList.add(row_2);
        returnArrayList.add(row_3);
        returnArrayList.add(row_4);
        returnArrayList.add(row_5);
        returnArrayList.add(row_6);
        returnArrayList.add(row_7);
        returnArrayList.add(row_8);
        returnArrayList.add(row_9);
        returnArrayList.add(row_A);
        returnArrayList.add(row_B);
        returnArrayList.add(row_C);
        returnArrayList.add(row_D);
        returnArrayList.add(row_E);
        returnArrayList.add(row_F);


        return returnArrayList;

    }


    protected Square getSingleSquare(int squareNumber) {


        return allSquares.get(squareNumber);

    }


    protected ArrayList<Square> getAllSquaresInACell(int cellNumber) {

        switch (cellNumber) {

            case 0:
                return cell_0;
            case 1:
                return cell_1;
            case 2:
                return cell_2;
            case 3:
                return cell_3;
            case 4:
                return cell_4;
            case 5:
                return cell_5;
            case 6:
                return cell_6;
            case 7:
                return cell_7;
            case 8:
                return cell_8;
            case 9:
                return cell_9;
            case 10:
                return cell_A;
            case 11:
                return cell_B;
            case 12:
                return cell_C;
            case 13:
                return cell_D;
            case 14:
                return cell_E;
            case 15:
                return cell_F;
            default:
                return null;
        }

    }


    protected ArrayList<Square> getAllSquaresInAGridRow(int rowNumber) {

        switch (rowNumber) {

            case 0:
                return row_0;
            case 1:
                return row_1;
            case 2:
                return row_2;
            case 3:
                return row_3;
            case 4:
                return row_4;
            case 5:
                return row_5;
            case 6:
                return row_6;
            case 7:
                return row_7;
            case 8:
                return row_8;
            case 9:
                return row_9;
            case 10:
                return row_A;
            case 11:
                return row_B;
            case 12:
                return row_C;
            case 13:
                return row_D;
            case 14:
                return row_E;
            case 15:
                return row_F;
            default:
                return null;
        }

    }


    protected ArrayList<Square> getAllSquaresInAGridColumn(int columnNumber) {


        switch (columnNumber) {

            case 0:
                return column_0;
            case 1:
                return column_1;
            case 2:
                return column_2;
            case 3:
                return column_3;
            case 4:
                return column_4;
            case 5:
                return column_5;
            case 6:
                return column_6;
            case 7:
                return column_7;
            case 8:
                return column_8;
            case 9:
                return column_9;
            case 10:
                return column_A;
            case 11:
                return column_B;
            case 12:
                return column_C;
            case 13:
                return column_D;
            case 14:
                return column_E;
            case 15:
                return column_F;
            default:
                return null;
        }

    }


    protected ArrayList<ArrayList<Square>> getAllSquaresInACellColumn(int cellColumnNumber) {

        switch (cellColumnNumber) {

            case 0:
                return cell_column_0;
            case 1:
                return cell_column_1;
            case 2:
                return cell_column_2;
            case 3:
                return cell_column_3;
            default:
                return null;
        }

    }

    protected ArrayList<ArrayList<Square>> getAllColumnsInACellColumn(int cellColumnNumber) {


        ArrayList<ArrayList<Square>> columnsInACellColumn = new ArrayList<>();

        switch (cellColumnNumber) {

            case 0:
                columnsInACellColumn.add(column_0);
                columnsInACellColumn.add(column_1);
                columnsInACellColumn.add(column_2);
                columnsInACellColumn.add(column_3);
                break;
            case 1:
                columnsInACellColumn.add(column_4);
                columnsInACellColumn.add(column_5);
                columnsInACellColumn.add(column_6);
                columnsInACellColumn.add(column_7);
                break;
            case 2:
                columnsInACellColumn.add(column_8);
                columnsInACellColumn.add(column_9);
                columnsInACellColumn.add(column_A);
                columnsInACellColumn.add(column_B);
                break;
            case 3:
                columnsInACellColumn.add(column_C);
                columnsInACellColumn.add(column_D);
                columnsInACellColumn.add(column_E);
                columnsInACellColumn.add(column_F);
                break;
            default:
                return null;
        }

        return columnsInACellColumn;

    }

    protected ArrayList<ArrayList<Square>> getAllRowsInACellRow(int cellRowNumber) {


        ArrayList<ArrayList<Square>> rowsInACellRow = new ArrayList<>();

        switch (cellRowNumber) {

            case 0:
                rowsInACellRow.add(row_0);
                rowsInACellRow.add(row_1);
                rowsInACellRow.add(row_2);
                rowsInACellRow.add(row_3);
                break;
            case 1:
                rowsInACellRow.add(row_4);
                rowsInACellRow.add(row_5);
                rowsInACellRow.add(row_6);
                rowsInACellRow.add(row_7);
                break;
            case 2:
                rowsInACellRow.add(row_8);
                rowsInACellRow.add(row_9);
                rowsInACellRow.add(row_A);
                rowsInACellRow.add(row_B);
                break;
            case 3:
                rowsInACellRow.add(row_C);
                rowsInACellRow.add(row_D);
                rowsInACellRow.add(row_E);
                rowsInACellRow.add(row_F);
                break;
            default:
                return null;
        }

        return rowsInACellRow;

    }


    protected ArrayList<ArrayList<Square>> getAllSquaresInACellRow(int cellRowNumber) {

        switch (cellRowNumber) {

            case 0:
                return cell_row_0;
            case 1:
                return cell_row_1;
            case 2:
                return cell_row_2;
            case 3:
                return cell_row_3;
            default:
                return null;
        }

    }


    protected boolean isASquareInGridWithDiscoveredSquareNotSet() {

        for (Square square : allSquares) {

            if (square.getDiscoveredAnswer() == -1) {
                return true;
            }

        }

        return false;
    }


    protected int getNumberOfSolvedSquares() {

        int count = 0;

        for (Square square : allSquares) {

            if (square.isHasBeenSolved() == true && square.isWasAPuzzleStarterSolution() == false) {

                count = count + 1;

            }


        }


        return count;
    }


    protected ArrayList<Square> getAllUnsolvedSquaresSortedBySquareNumber() {

        ArrayList<Square> squareList = new ArrayList<>();

        for (Square square : allSquares) {

            if (square.isHasBeenSolved() == false) {
                squareList.add(square);
            }
        }


        Collections.sort(squareList, new Comparator<Square>() {

            public int compare(Square o1, Square o2) {
                return Integer.valueOf(o1.getSquareNumber()).compareTo(Integer.valueOf(o2.getSquareNumber()));
            }
        });


        return squareList;

    }


    protected ArrayList<Square> getAllUnsolvedSquaresSortedByCustomCellOrder() {

        ArrayList<Square> squareList = new ArrayList<>();

        int[] cellOrder = {0, 1, 2, 3, 7, 11, 15, 14, 13, 12, 8, 4, 5, 6, 10, 9};

        for (int cellNumber : cellOrder) {

            for (Square square : getAllSquaresInACell(cellNumber)) {

                if (square.isHasBeenSolved() == false) {
                    squareList.add(square);
                }
            }

        }


        return squareList;

    }


    protected ArrayList<Square> getAllUnsolvedSquaresSortedByHeuristic1(){

        // attempt to speed the process by finding cellColumns in order of lowest number of option possibilities
        // and a column row the same remaining, then columns and rows can be added by lowest to highest

        ArrayList<Square> squareList = new ArrayList<>();

        ArrayList<int []> sortingListColumns =  new ArrayList<>();
        ArrayList<int []> sortingListRows =  new ArrayList<>();

        int indexs[] = {0,1,2,3};

        for (int index : indexs){

            ArrayList<ArrayList<Square>> cellColumn = getAllSquaresInACellColumn(index);

            int countOfOptions = 0;

            for (ArrayList<Square> cell : cellColumn){


                for (Square square : cell) {

                    countOfOptions = countOfOptions + square.getNumberOfRemainingOptions();

                }

            }

            int[] cellTotalColumn = {index, countOfOptions};

            sortingListColumns.add(cellTotalColumn);



            ArrayList<ArrayList<Square>> cellRow = getAllSquaresInACellRow(index);


            countOfOptions = 0;

            for (ArrayList<Square> cell : cellRow){

                for (Square square : cell) {

                    countOfOptions = countOfOptions + square.getNumberOfRemainingOptions();

                }
            }

            int[] cellTotalRow = {index, countOfOptions};

            sortingListRows.add(cellTotalRow);

        }



        // sort by number of options in a cellcolumn/row

        Collections.sort(sortingListColumns, new Comparator<int[]>() {

            public int compare(int[] o1, int[] o2) {
                return Integer.valueOf(o1[1]).compareTo(Integer.valueOf(o2[1]));
            }
        });

        Collections.sort(sortingListRows, new Comparator<int[]>() {

            public int compare(int[] o1, int[] o2) {
                return Integer.valueOf(o1[1]).compareTo(Integer.valueOf(o2[1]));
            }
        });

        // decide to if to do rows or columns first

        if (sortingListColumns.get(0)[1] >= sortingListRows.get(0)[1]){

            // columns first

            for (int index : indexs){

                // column then row each time

                ArrayList<ArrayList<Square>> cellColumn = getAllSquaresInACellColumn(sortingListColumns.get(index)[0]);

                // add squares to square list

                for (ArrayList<Square> cell : cellColumn){

                    for (Square square : cell){

                        if (!square.isHasBeenSolved()){

                            if (!squareList.contains(square)){

                                squareList.add(square);

                            }

                        }

                    }


                }

                ArrayList<ArrayList<Square>> cellRow = getAllSquaresInACellRow(sortingListRows.get(index)[0]);

                // add squares to square list

                for (ArrayList<Square> cell : cellRow){

                    for (Square square : cell){

                        if (!square.isHasBeenSolved()){

                            if (!squareList.contains(square)){

                                squareList.add(square);

                            }

                        }

                    }


                }


            }



        } else {

            // rows first then column

            for (int index : indexs){

            ArrayList<ArrayList<Square>> cellRow = getAllSquaresInACellRow(sortingListRows.get(index)[0]);

            // add squares to square list

            for (ArrayList<Square> cell : cellRow) {

                for (Square square : cell) {

                    if (!square.isHasBeenSolved()) {

                        if (!squareList.contains(square)) {

                            squareList.add(square);

                        }

                    }

                }


            }

            ArrayList<ArrayList<Square>> cellColumn = getAllSquaresInACellColumn(sortingListColumns.get(index)[0]);

            // add squares to square list

            for (ArrayList<Square> cell : cellColumn) {

                for (Square square : cell) {

                    if (!square.isHasBeenSolved()) {

                        if (!squareList.contains(square)) {

                            squareList.add(square);

                        }

                    }

                }


            }

        }


        }


        return squareList;
    }

    protected ArrayList<Square> getAllUnsolvedSquaresSortedByHeuristic2(){

        // attempt to speed the process by finding cellColumns in order of lowest number of option possibilities
        // and a column row the same remaining, then columns and rows can be added by lowest to highest

        ArrayList<Square> squareList = new ArrayList<>();

        ArrayList<int []> sortingListColumns =  new ArrayList<>();
        ArrayList<int []> sortingListRows =  new ArrayList<>();

        int indexs[] = {0,1,2,3};

        for (int index : indexs){

            ArrayList<ArrayList<Square>> cellColumn = getAllSquaresInACellColumn(index);

            int countOfOptions = 0;

            for (ArrayList<Square> cell : cellColumn){


                for (Square square : cell) {

                    countOfOptions = countOfOptions + square.getNumberOfRemainingOptions();

                }

            }

            int[] cellTotalColumn = {index, countOfOptions};

            sortingListColumns.add(cellTotalColumn);



            ArrayList<ArrayList<Square>> cellRow = getAllSquaresInACellRow(index);


            countOfOptions = 0;

            for (ArrayList<Square> cell : cellRow){

                for (Square square : cell) {

                    countOfOptions = countOfOptions + square.getNumberOfRemainingOptions();

                }
            }

            int[] cellTotalRow = {index, countOfOptions};

            sortingListRows.add(cellTotalRow);

        }



        // sort by number of options in a cellcolumn/row

        Collections.sort(sortingListColumns, new Comparator<int[]>() {

            public int compare(int[] o1, int[] o2) {
                return Integer.valueOf(o2[1]).compareTo(Integer.valueOf(o1[1]));
            }
        });

        Collections.sort(sortingListRows, new Comparator<int[]>() {

            public int compare(int[] o1, int[] o2) {
                return Integer.valueOf(o2[1]).compareTo(Integer.valueOf(o1[1]));
            }
        });

        // decide to if to do rows or columns first

        if (sortingListColumns.get(0)[1] >= sortingListRows.get(0)[1]){

            // columns first

            for (int index : indexs){

                // column then row each time

                ArrayList<ArrayList<Square>> cellColumn = getAllSquaresInACellColumn(sortingListColumns.get(index)[0]);

                // add squares to square list

                for (ArrayList<Square> cell : cellColumn){

                    for (Square square : cell){

                        if (!square.isHasBeenSolved()){

                            if (!squareList.contains(square)){

                                squareList.add(square);

                            }

                        }

                    }


                }

                ArrayList<ArrayList<Square>> cellRow = getAllSquaresInACellRow(sortingListRows.get(index)[0]);

                // add squares to square list

                for (ArrayList<Square> cell : cellRow){

                    for (Square square : cell){

                        if (!square.isHasBeenSolved()){

                            if (!squareList.contains(square)){

                                squareList.add(square);

                            }

                        }

                    }


                }


            }



        } else {

            // rows first then column

            for (int index : indexs){

                ArrayList<ArrayList<Square>> cellRow = getAllSquaresInACellRow(sortingListRows.get(index)[0]);

                // add squares to square list

                for (ArrayList<Square> cell : cellRow) {

                    for (Square square : cell) {

                        if (!square.isHasBeenSolved()) {

                            if (!squareList.contains(square)) {

                                squareList.add(square);

                            }

                        }

                    }


                }

                ArrayList<ArrayList<Square>> cellColumn = getAllSquaresInACellColumn(sortingListColumns.get(index)[0]);

                // add squares to square list

                for (ArrayList<Square> cell : cellColumn) {

                    for (Square square : cell) {

                        if (!square.isHasBeenSolved()) {

                            if (!squareList.contains(square)) {

                                squareList.add(square);

                            }

                        }

                    }


                }

            }


        }


        return squareList;
    }

    protected void updateDisplayToShowAllDiscoveredSolutions() {

        for (GridButton gridButton : gridButtonArrayList) {

            Square square = getSingleSquare(gridButton.getSquareNumber());


            gridButton.setTextToFoundSolution();


        }


    }

    protected int getGameStateSaveListSize() {

        return gameStateLinkedList.size();

    }

    protected void resetGameStateSaveList() {

        gameStateLinkedList.clear();

    }

    protected void saveGameState() {


        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = null;
        try {
            out = new ObjectOutputStream(bos);
            out.writeObject(allSquares);
            out.flush();
            byte[] saveBytes = bos.toByteArray();

            if (gameStateLinkedList.size() > 0) {

                if (!Arrays.equals(saveBytes, gameStateLinkedList.getLast())) {

                    gameStateLinkedList.addLast(saveBytes);


                }
            } else {

                gameStateLinkedList.addLast(saveBytes);


            }

            updateSavedStateTextViewCount();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bos.close();
            } catch (IOException ex) {
                // ignore close exception
            }
        }


    }

    protected void revertGameStateAStep() {


        if (gameStateLinkedList.size() > 1) {


            gameStateLinkedList.removeLast();


            ByteArrayInputStream bis = new ByteArrayInputStream(gameStateLinkedList.getLast());
            ObjectInput in = null;
            try {
                in = new ObjectInputStream(bis);
                Object o = in.readObject();

                ArrayList<Square> oldAllSquares = (ArrayList<Square>) o;

                // restore the lists 


                setAllLists(oldAllSquares);


                System.out.println("Game State reverted to #State" + Integer.toString(gameStateLinkedList.size() - 1));

                updateSavedStateTextViewCount();
                updateDisplayToShowAllDiscoveredSolutions();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        totalIterationTextView.setText("");

                    }
                });


            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException ex) {
                    // ignore close exception
                }
            }


        }

    }

    protected void restoreGameState() {


        if (!gameStateLinkedList.isEmpty()) {


            ByteArrayInputStream bis = new ByteArrayInputStream(gameStateLinkedList.getLast());
            ObjectInput in = null;
            try {
                in = new ObjectInputStream(bis);
                Object o = in.readObject();

                ArrayList<Square> oldAllSquares = (ArrayList<Square>) o;

                // restore the lists


                setAllLists(oldAllSquares);

                updateSavedStateTextViewCount();
                updateDisplayToShowAllDiscoveredSolutions();


            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException ex) {
                    // ignore close exception
                }
            }


        }

    }

    protected void updateSavedStateTextViewCount() {

        Grid my_grid = Grid.getInstance();

        final int numberOfSolvedSquares = my_grid.getNumberOfSolvedSquares();

        // set solved squares textview

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // WORK on UI thread here

                saveStateIterationTextView.setText("Save State #" + Integer.toString(Grid.getInstance().getGameStateSaveListSize() - 1));

            }
        });


    }


}
