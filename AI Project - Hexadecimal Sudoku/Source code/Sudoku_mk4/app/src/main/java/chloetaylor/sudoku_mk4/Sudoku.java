package chloetaylor.sudoku_mk4;

import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AlignmentSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Sudoku extends AppCompatActivity {

    private Ai my_ai = Ai.getInstance();
    private Grid my_grid = Grid.getInstance();

    private static List<Button> allFunctionButtons = new ArrayList<>();

    private List<Puzzle> allPuzzles = new ArrayList<>();

    private Puzzle selectedPuzzle = null;

    private static AsyncTask backTrackingTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sudoku);

        // set textviews for ai and grid class : important needs to be done
        my_ai.setIterationTextView((TextView) findViewById(R.id.totalIterationTextView));
        my_ai.setSolvedSquaresTextView((TextView) findViewById(R.id.solvedTextView));
        my_grid.setSavedStateIterationTextView((TextView) findViewById(R.id.savedStateIterationTextView));
        my_grid.setTotalIterationTextView((TextView) findViewById(R.id.totalIterationTextView));
        // puzzle change popup

        Button puzzleChange = findViewById(R.id.toolbar_Button);

        allFunctionButtons.add(puzzleChange);

        puzzleChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup();
            }
        });


        Button heuristicChange = findViewById(R.id.toolbar_button2);

        allFunctionButtons.add(heuristicChange);

        heuristicChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup2();
            }
        });

        Puzzle puzzleInitialisation = new Puzzle();

        puzzleInitialisation.initialisePuzzles();

        allPuzzles = puzzleInitialisation.getPuzzleList();



        initialiseTheFunctionButtons();

        // disabled until puzzle loaded

        disableAllFunctionButtons();

        // needs to be enabled to load the first puzzle
        puzzleChange.setEnabled(true);

        heuristicChange.setEnabled(true);

    }





    private void initialiseTheGrid(Puzzle puzzle) {


        final TableLayout mainGridLayout = findViewById(R.id.main_grid);

        mainGridLayout.removeAllViews();

        mainGridLayout.setShrinkAllColumns(true);
        mainGridLayout.setStretchAllColumns(true);


        ArrayList<Square> allSquares = new ArrayList<>();
        ArrayList<GridButton> allGridButtons = new ArrayList<>();

        int squareNumber = 0;
        int cellNumber = 0;
        int cellRowNumber = 0;
        int cellColumnNumber = 0;
        int columnWithinACellNumber;
        int rowWithinACellNumber;

        final int numberOfCellsInEachDirection = 4;
        int numberOfCellsInPuzzle = numberOfCellsInEachDirection * numberOfCellsInEachDirection;

        for (int i = 0; i < numberOfCellsInPuzzle; i = i + 1) {

            final TableRow tableRow = new TableRow(this);

            tableRow.setLayoutParams(new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT, 1.0f));
            tableRow.setBaselineAligned(false);


            cellRowNumber = (int) (Math.floor(numberOfCellsInPuzzle - (numberOfCellsInPuzzle - i)) / numberOfCellsInEachDirection);

            rowWithinACellNumber = i % 4;


            for (int j = 0; j < numberOfCellsInPuzzle; j = j + 1) {

                cellColumnNumber = (int) Math.floor((numberOfCellsInPuzzle - (numberOfCellsInPuzzle - j)) / numberOfCellsInEachDirection);

                cellNumber = cellColumnNumber + (cellRowNumber * numberOfCellsInEachDirection);

                columnWithinACellNumber = j % 4;




                final Square square = new Square(squareNumber, cellNumber, cellColumnNumber, cellRowNumber, columnWithinACellNumber, rowWithinACellNumber, j, i);


                square.setPuzzleStarterFromHex(puzzle.getPuzzleSetSquare(squareNumber));
                square.setCorrectAnswerAsHex(puzzle.getPuzzleSolutionSquare(squareNumber));


                final int finalSquareNumber = squareNumber;

                final GridButton gridSquareButton = new GridButton(this, squareNumber);

                allGridButtons.add(gridSquareButton);

                gridSquareButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        Square squareForButton = my_grid.getSingleSquare(finalSquareNumber);

                        String output = "";

                        String discovered = "";

                        if(squareForButton.getDiscoveredAnswer() != -1){
                            discovered = squareForButton.getDiscoveredAnswerAsHexString();
                        } else {
                            discovered = "false";
                        }

                        output = output + "Square Number: " + Integer.toString(squareForButton.getSquareNumber()) + "<br><br>" +
                                "Cell Number: " + Integer.toString(squareForButton.getCellNumber()) + "<br>" +
                                "Cell (" + Integer.toString(squareForButton.getCellColumnNumber()) + "," +
                                Integer.toString(squareForButton.getCellRowNumber()) + ")<br>" +
                                "Grid (" + Integer.toString(squareForButton.getGridColumnNumber()) + "," +
                                Integer.toString(squareForButton.getGridRowNumber()) + ")<br>" +
                                "Within Cell (" + Integer.toString(squareForButton.getColumnWithinACellNumber()) + "," +
                                Integer.toString(squareForButton.getRowWithinACellNumber()) + ")<br><br>" +
                                "Marked As Solved: " + Boolean.toString(squareForButton.isHasBeenSolved()) + "<br>" +
                        "Discovered: " + discovered + "<br>" +
                                squareForButton.getRemainingOptionsString();

                                Spanned spanned = Html.fromHtml(output, Html.FROM_HTML_MODE_LEGACY);

                        makeToastMessageHTML(spanned);


                    }

                });




                if (cellNumber % 2 == 0) {
                    if (numberOfCellsInEachDirection % 2 == 0 && cellRowNumber % 2 != 0) {
                        gridSquareButton.setBackgroundResource(R.drawable.grid_border_odd);
                    } else {
                        gridSquareButton.setBackgroundResource(R.drawable.grid_border_even);
                    }
                } else {
                    if (numberOfCellsInEachDirection % 2 == 0 && cellRowNumber % 2 != 0) {
                        gridSquareButton.setBackgroundResource(R.drawable.grid_border_even);
                    } else {
                        gridSquareButton.setBackgroundResource(R.drawable.grid_border_odd);
                    }
                }




                tableRow.addView(gridSquareButton);


                allSquares.add(square);

                squareNumber = squareNumber + 1;

            }

            mainGridLayout.addView(tableRow);

        }

                my_grid.setAllLists(allSquares);
                my_grid.setGridButtonArrayList(allGridButtons);



    }





    private void initialiseTheFunctionButtons() {

        initialisePreviousStateButton();
        initialiseAutoCycleButton();
        initialiseNextStepButton();
        initialiseBackTrackingButton();
        initialiseRule1Button();
        initialiseRule2Button();
        initialiseRule3Button();
        initialiseRule4Button();
        initialiseRule5Button();
        initialiseRule6Button();
        initialiseRule7Button();
        initialiseRule8Button();
        initialiseRule9Button();
        initialiseRule10Button();
        initialiseRule11Button();
        initialiseRule12Button();
        initialiseRule13Button();

        // send button list to ai class (nasty hack)

        my_ai.setAllFunctionButtons(allFunctionButtons);

    }


    private void initialiseBackTrackingButton(){


        final Button backTracking = findViewById(R.id.backtracking);

        backTracking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (backTrackingTask == null) {


                    Grid grid = Grid.getInstance();


                    View rootView = findViewById(R.id.rootView);

                    backTrackingTask = new AiAsyncTask(getApplicationContext(), rootView).execute("backTrack");

                    backTracking.setText("Cancel Back Tracking");

                    disableAllFunctionButtonsExceptBackTrackingCancel();
                }
                else {

                    if (backTrackingTask.getStatus() == AsyncTask.Status.PENDING) {

                        backTrackingTask.cancel(true);
                        makeToastMessage("Cancelling Back Tracking");
                        disableAllFunctionButtons();

                    }

                    if (backTrackingTask.getStatus() == AsyncTask.Status.RUNNING) {

                        backTrackingTask.cancel(true);
                        makeToastMessage("Cancelling Back Tracking");
                        disableAllFunctionButtons();

                    }


                    if (backTrackingTask.getStatus() == AsyncTask.Status.FINISHED) {

                        backTrackingTask = null;
                        backTracking.setText("Back Tracking");
                        disableAllFunctionButtons();

                    }

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 1000ms


                            backTrackingTask = null;

                                backTracking.setText("Back Tracking");

                                my_grid.restoreGameState();

                            enableAllFunctionButtons();

                            TextView totalIterations = findViewById(R.id.totalIterationTextView);
                            totalIterations.setText("");


                        }
                    }, 1000);
                }


            }

        });

        allFunctionButtons.add(backTracking);

    }



    private void initialisePreviousStateButton(){

        final Button stateRevert = findViewById(R.id.previousSnapshot);

        stateRevert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                my_grid.revertGameStateAStep();




                my_ai.updateSolvedSquaresTextView();

            }

        });

        allFunctionButtons.add(stateRevert);

    }


    private void initialiseNextStepButton(){


        Button nextStep = findViewById(R.id.next_step);

        nextStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("nextStep");

            }

        });

        allFunctionButtons.add(nextStep);

    }


    private void initialiseAutoCycleButton(){


        Button cycleButton = findViewById(R.id.cycle);

        final Button backTracking = findViewById(R.id.backtracking);

        cycleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                Grid grid = Grid.getInstance();

                TextView durationDisplay = findViewById(R.id.timed_duration);

                boolean validFullCycle = false;

                if(grid.getGameStateSaveListSize() == 1) {

                    validFullCycle = true;
                }

                View rootView = findViewById(R.id.rootView);


                if (backTrackingTask == null) {



                    backTrackingTask = new AiAsyncTask(getApplicationContext(), rootView).execute("autoCycle");


                    disableAllFunctionButtons();
                }
                else {

                    if (backTrackingTask.getStatus() == AsyncTask.Status.PENDING) {

                        backTrackingTask.cancel(true);
                        makeToastMessage("Cancelling Back Tracking");
                        disableAllFunctionButtons();

                    }

                    if (backTrackingTask.getStatus() == AsyncTask.Status.RUNNING) {

                        backTrackingTask.cancel(true);
                        makeToastMessage("Cancelling Back Tracking");
                        disableAllFunctionButtons();

                    }


                    if (backTrackingTask.getStatus() == AsyncTask.Status.FINISHED) {

                        backTrackingTask = null;
                        backTracking.setText("Back Tracking");
                        disableAllFunctionButtons();

                    }

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 1000ms


                            backTrackingTask = null;

                            backTracking.setText("Back Tracking");

                            my_grid.restoreGameState();

                            enableAllFunctionButtons();

                            TextView totalIterations = findViewById(R.id.totalIterationTextView);
                            totalIterations.setText("");


                        }
                    }, 1000);
                }




            }

        });


        allFunctionButtons.add(cycleButton);
    }


    private void initialiseRule1Button(){


        final Button button = findViewById(R.id.rule1);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule1");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule2Button(){


        Button button = findViewById(R.id.rule2);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule2");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule3Button(){


        Button button = findViewById(R.id.rule3);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule3");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule4Button(){


        Button button = findViewById(R.id.rule4);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule4");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule5Button(){


        Button button = findViewById(R.id.rule5);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule5");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule6Button(){


        Button button = findViewById(R.id.rule6);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule6");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule7Button(){


        Button button = findViewById(R.id.rule7);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule7");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule8Button(){


        Button button = findViewById(R.id.rule8);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule8");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule9Button(){


        Button button = findViewById(R.id.rule9);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule9");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule10Button(){


        Button button = findViewById(R.id.rule10);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule10");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule11Button(){


        Button button = findViewById(R.id.rule11);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule11");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule12Button(){


        Button button = findViewById(R.id.rule12);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule12");

            }

        });

        allFunctionButtons.add(button);

    }

    private void initialiseRule13Button(){


        Button button = findViewById(R.id.rule13);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View rootView = findViewById(R.id.rootView);

                new AiAsyncTask(getApplicationContext(), rootView).execute("rule13");

            }

        });

        allFunctionButtons.add(button);

    }

      private void disableAllFunctionButtons(){

        for (Button button : allFunctionButtons){

            button.setEnabled(false);

        }

    }

    private void disableAllFunctionButtonsExceptBackTrackingCancel(){

        for (Button button : allFunctionButtons){

            if (button.getTag() == null) {
                button.setEnabled(false);
            }

        }

    }

    protected void enableAllFunctionButtons(){



        for (Button button : allFunctionButtons){

            button.setEnabled(true);

        }

    }



    private void makeToastMessage(String toastMessage) {

        Spannable centeredText = new SpannableString(toastMessage);
        centeredText.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
                0, toastMessage.length() - 1,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);


        Toast toast = Toast.makeText(getApplicationContext(), centeredText, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 32);
        toast.show();
    }

    private void makeToastMessageHTML(Spanned toastMessage) {

        Spannable centeredText = new SpannableString(toastMessage);
        centeredText.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
                0, toastMessage.length() - 1,
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);


        Toast toast = Toast.makeText(getApplicationContext(), centeredText, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM, 0, 32);
        toast.show();
    }



    public void showPopup() {


        final View popupView = getLayoutInflater().inflate(R.layout.pop_up_main_page, null, false);

        final PopupWindow popupWindow = new PopupWindow(
                popupView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);


        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(false);

        popupWindow.setBackgroundDrawable(new ColorDrawable());




        final Spinner puzzle_spinner = popupView.findViewById(R.id.puzzleSpinner);
        ArrayAdapter<Puzzle> spinnerArrayAdapter = new ArrayAdapter<Puzzle>
                (this, android.R.layout.simple_spinner_item,
                        allPuzzles);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        puzzle_spinner.setAdapter(spinnerArrayAdapter);

        puzzle_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {


            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {




            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Button confirmButton = popupView.findViewById(R.id.popupConfirmButton);

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedPuzzle = (Puzzle) puzzle_spinner.getSelectedItem();

                TextView hint = findViewById(R.id.gridTextView);
                hint.setText("");

                initialiseTheGrid(selectedPuzzle);

                my_grid.resetGameStateSaveList();

                my_ai.removeAllInvalidOptions();
                my_grid.updateDisplayToShowAllDiscoveredSolutions();

                my_grid.saveGameState();

                my_ai.updateSolvedSquaresTextView();

                enableAllFunctionButtons();

                popupWindow.dismiss();




            }

        });


        Button cancelButton = popupView.findViewById(R.id.popupCancelButton);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();

            }

        });

    }


    public void showPopup2() {


        final View popupView = getLayoutInflater().inflate(R.layout.pop_up_main_page2, null, false);

        final PopupWindow popupWindow = new PopupWindow(
                popupView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);


        // If the PopupWindow should be focusable
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(false);

        popupWindow.setBackgroundDrawable(new ColorDrawable());

        final RadioButton radioButton1 = popupView.findViewById(R.id.radioButton1);
        final RadioButton radioButton2 = popupView.findViewById(R.id.radioButton2);
        final RadioButton radioButton3 = popupView.findViewById(R.id.radioButton3);
        final RadioButton radioButton4 = popupView.findViewById(R.id.radioButton4);

        int heuristic = my_ai.getHeuristicType();

        switch (heuristic){

            case 0:  radioButton1.setChecked(true);
                break;
            case 1:  radioButton2.setChecked(true);
                break;
            case 2:  radioButton3.setChecked(true);
                break;
            case 3:  radioButton4.setChecked(true);
                break;
            default: radioButton3.setChecked(true);
                    heuristic = 2;
                    my_ai.setHeuristicType(heuristic);
                    break;

        }




        Button confirmButton = popupView.findViewById(R.id.popupConfirmButton);

        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int heuristicValue = 2;

                if (radioButton1.isChecked()){
                    heuristicValue = 0;
                } else if (radioButton2.isChecked()){
                    heuristicValue = 1;
                } else if (radioButton3.isChecked()){
                    heuristicValue = 2;
                } else if (radioButton4.isChecked()){
                    heuristicValue = 3;
                }

                my_ai.setHeuristicType(heuristicValue);

                popupWindow.dismiss();




            }

        });


        Button cancelButton = popupView.findViewById(R.id.popupCancelButton);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();

            }

        });

    }


    public static void nullifyBackTrackingTask() {
        backTrackingTask = null;
    }
}