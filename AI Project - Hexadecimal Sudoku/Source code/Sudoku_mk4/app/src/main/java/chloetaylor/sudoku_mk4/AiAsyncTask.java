package chloetaylor.sudoku_mk4;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Chloe on 04/03/2018.
 */

public class AiAsyncTask extends AsyncTask<String, Integer, Boolean> {

    Context context;
    View rootView; // can only be used in post on ui thread





    public AiAsyncTask(Context context, View rootView) {
        this.context = context;
        this.rootView = rootView;
    }


    String[] method = {"nextStep", "autoCycle", "backTrack", "rule1", "rule2", "rule3",
            "rule4", "rule5", "rule6", "rule7", "rule8", "rule9", "rule10", "rule11", "rule12", "rule13"};


    private boolean rule1Enabled = true;
    private boolean rule2Enabled = true;
    private boolean rule3Enabled = true;
    private boolean rule4Enabled = true;
    private boolean rule5Enabled = true;
    private boolean rule6Enabled = true;
    private boolean rule7Enabled = true;
    private boolean rule8Enabled = true;
    private boolean rule9Enabled = true;
    private boolean rule10Enabled = true;
    private boolean rule11Enabled = true;
    private boolean rule12Enabled = true;
    private boolean rule13Enabled = true;
    private boolean backTrackingEnabled = true;

    String selectedMethod = "";



    // Runs in UI before background thread is called
    @Override
    protected void onPreExecute() {
        super.onPreExecute();


        // Do something like display a progress bar

    }

    // This is run in a background thread
    protected Boolean doInBackground(String... passedParameter) {



            String parameter = passedParameter[0];

        selectedMethod = parameter;

            if (parameter.equals(method[0])) {

                // nextStep

                return nextStepReturnSuccess();
                // successful ai step

            } else if (parameter.equals(method[1])) {


                return autoCycleReturnSuccess();

            } else if (parameter.equals(method[2])) {


                return backTrackingReturnSuccess();


            } else if (parameter.equals(method[3])) {

                if (selectedMethod.equals(method[3])) {
                    return rule1ReturnSuccess(true);
                } else {
                    return rule1ReturnSuccess(false);
                }


            } else if (parameter.equals(method[4])) {

                if (selectedMethod.equals(method[4])) {
                    return rule2ReturnSuccess(true);
                } else {
                    return rule2ReturnSuccess(false);
                }

            } else if (parameter.equals(method[5])) {

                if (selectedMethod.equals(method[5])) {
                    return rule3ReturnSuccess(true);
                } else {
                    return rule3ReturnSuccess(false);
                }

            } else if (parameter.equals(method[6])) {

                if (selectedMethod.equals(method[6])) {
                    return rule4ReturnSuccess(true);
                } else {
                    return rule4ReturnSuccess(false);
                }

            } else if (parameter.equals(method[7])) {

                if (selectedMethod.equals(method[7])) {
                    return rule5ReturnSuccess(true);
                } else {
                    return rule5ReturnSuccess(false);
                }

            } else if (parameter.equals(method[8])) {

                if (selectedMethod.equals(method[8])) {
                    return rule6ReturnSuccess(true);
                } else {
                    return rule6ReturnSuccess(false);
                }

            } else if (parameter.equals(method[9])) {

                if (selectedMethod.equals(method[9])) {
                    return rule7ReturnSuccess(true);
                } else {
                    return rule7ReturnSuccess(false);
                }

            } else if (parameter.equals(method[10])) {

                if (selectedMethod.equals(method[10])) {
                    return rule8ReturnSuccess(true);
                } else {
                    return rule8ReturnSuccess(false);
                }

            } else if (parameter.equals(method[11])) {

                if (selectedMethod.equals(method[11])) {
                    return rule9ReturnSuccess(true);
                } else {
                    return rule9ReturnSuccess(false);
                }

            } else if (parameter.equals(method[12])) {

                if (selectedMethod.equals(method[12])) {
                    return rule10ReturnSuccess(true);
                } else {
                    return rule10ReturnSuccess(false);
                }

            } else if (parameter.equals(method[13])) {

                if (selectedMethod.equals(method[13])) {
                    return rule11ReturnSuccess(true);
                } else {
                    return rule11ReturnSuccess(false);
                }

            } else if (parameter.equals(method[14])) {

                if (selectedMethod.equals(method[14])) {
                    return rule12ReturnSuccess(true);
                } else {
                    return rule12ReturnSuccess(false);
                }

            } else if (parameter.equals(method[15])) {

                if (selectedMethod.equals(method[15])) {
                    return rule13ReturnSuccess(true);
                } else {
                    return rule13ReturnSuccess(false);
                }

            }


            return false;





    }



    protected void onProgressUpdate(Integer... progress) {

        super.onProgressUpdate(progress);

        // publishProgress(i); i from doInBackground would be picked up as progress




    }


    // This runs in UI when background thread finishes
    protected void onPostExecute(Boolean success) {

        super.onPostExecute(success);

        Grid grid = Grid.getInstance();


        grid.updateDisplayToShowAllDiscoveredSolutions();

        TextView iteration = rootView.findViewById(R.id.savedStateIterationTextView);

        iteration.setText("Save State #" + Integer.toString(grid.getGameStateSaveListSize() - 1));



    }



    private boolean autoCycleReturnSuccess(){



        Ai myAi = Ai.getInstance();
        Grid myGrid = Grid.getInstance();


        boolean isComplete = false;
        boolean escapeLoop = false;



        while (escapeLoop == false) {

            boolean proceedToNext = true;




            if (rule1Enabled) {
                // rule 1

                if (myAi.checkGridForSoleRemainingOptionSolutions_Rule1_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }

            // rule 2

            if (rule2Enabled && proceedToNext == true) {

                if (myAi.checkGridForExclusiveOptionSolutions_Rule2_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }


            if (rule3Enabled && proceedToNext == true) {

                if (myAi.checkGridForNumberClaiming_Rule3_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }

            if (rule4Enabled && proceedToNext == true) {

                if (myAi.checkGridForPairs_Rule4_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }

            if (rule5Enabled && proceedToNext == true) {

                if (myAi.checkGridForTriples_Rule5_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }


            if (rule6Enabled && proceedToNext == true) {

                if (myAi.checkGridForQuads_Rule6_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }

            if (rule7Enabled && proceedToNext == true) {

                if (myAi.checkGridForQuints_Rule7_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }

            if (rule8Enabled && proceedToNext == true){

                if (myAi.checkGridForCellLineReduction_Rule8_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }

            if (rule9Enabled && proceedToNext == true){

                if (myAi.checkGridForXWings_Rule9_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }

            if (rule10Enabled && proceedToNext == true){

                if (myAi.checkGridForHiddenPairs_Rule10_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }

            if (rule11Enabled && proceedToNext == true){

                if (myAi.checkGridForHiddenTriples_Rule11_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }

            if (rule12Enabled && proceedToNext == true){

                if (myAi.checkGridForHiddenQuad_Rule12_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }

            if (rule13Enabled && proceedToNext == true){

                if (myAi.checkGridForYWings_Rule13_ReturnTrueOnEvent(false)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }

            if (backTrackingEnabled && proceedToNext == true){

                if (myAi.backTrackingBFSUninformedSearchReturnSuccess(this)) {

                    proceedToNext = false;
                    myAi.removeAllInvalidOptions();

                }

            }


            isComplete = myAi.isPuzzleCompleted_FullVerify();

            if (isComplete){
                    myGrid.saveGameState();


                return true;
            } else if (proceedToNext) {
                // stalled for solutions
                 escapeLoop = true;
            } else {
                // successful event but more to find
                myGrid.saveGameState();
            }

            }



                return false;


        }






    private boolean nextStepReturnSuccess(){



        boolean proceedToNext = true;

        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();

        if (rule1Enabled) {
            // rule 1

            if (myAi.checkGridForSoleRemainingOptionSolutions_Rule1_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        // rule 2

        if (rule2Enabled && proceedToNext == true) {

            if (myAi.checkGridForExclusiveOptionSolutions_Rule2_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }


        if (rule3Enabled && proceedToNext == true) {

            if (myAi.checkGridForNumberClaiming_Rule3_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        if (rule4Enabled && proceedToNext == true) {

            if (myAi.checkGridForPairs_Rule4_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        if (rule5Enabled && proceedToNext == true) {

            if (myAi.checkGridForTriples_Rule5_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }


        if (rule6Enabled && proceedToNext == true) {

            if (myAi.checkGridForQuads_Rule6_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        if (rule7Enabled && proceedToNext == true) {

            if (myAi.checkGridForQuints_Rule7_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        if (rule8Enabled && proceedToNext == true){

            if (myAi.checkGridForCellLineReduction_Rule8_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        if (rule9Enabled && proceedToNext == true){

            if (myAi.checkGridForXWings_Rule9_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        if (rule10Enabled && proceedToNext == true){

            if (myAi.checkGridForHiddenPairs_Rule10_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        if (rule11Enabled && proceedToNext == true){

            if (myAi.checkGridForHiddenTriples_Rule11_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        if (rule12Enabled && proceedToNext == true){

            if (myAi.checkGridForHiddenQuad_Rule12_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        if (rule13Enabled && proceedToNext == true){

            if (myAi.checkGridForYWings_Rule13_ReturnTrueOnEvent(false)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        if (backTrackingEnabled && proceedToNext == true){

            if (myAi.backTrackingBFSUninformedSearchReturnSuccess(this)) {

                proceedToNext = false;
                myAi.removeAllInvalidOptions();

            }

        }

        if (proceedToNext == true){

            // no change found
            return false;

        } else {
            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;

        }
    }


    private boolean backTrackingReturnSuccess() {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();



                if (myAi.backTrackingBFSUninformedSearchReturnSuccess(this)) {

                    boolean isComplete = myAi.isPuzzleCompleted_FullVerify();

                    if (isComplete) {
                        myGrid.saveGameState();
                        return true;
                    } else {
                        return false;
                    }

                } else {
                    return false;
                }



    }

    private boolean rule1ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();


        if (myAi.checkGridForSoleRemainingOptionSolutions_Rule1_ReturnTrueOnEvent(singleAction)) {

            myAi.removeAllInvalidOptions();

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }

    private boolean rule2ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();


        if (myAi.checkGridForExclusiveOptionSolutions_Rule2_ReturnTrueOnEvent(singleAction)) {

            myAi.removeAllInvalidOptions();

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }

    private boolean rule3ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();


        if (myAi.checkGridForNumberClaiming_Rule3_ReturnTrueOnEvent(singleAction)) {

            myAi.removeAllInvalidOptions();

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }

    private boolean rule4ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();


        if (myAi.checkGridForPairs_Rule4_ReturnTrueOnEvent(singleAction)) {

            myAi.removeAllInvalidOptions();

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }

    private boolean rule5ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();


        if (myAi.checkGridForTriples_Rule5_ReturnTrueOnEvent(singleAction)) {

            myAi.removeAllInvalidOptions();

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }

    private boolean rule6ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();


        if (myAi.checkGridForQuads_Rule6_ReturnTrueOnEvent(singleAction)) {

            myAi.removeAllInvalidOptions();

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }

    private boolean rule7ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();

        if (myAi.checkGridForQuints_Rule7_ReturnTrueOnEvent(singleAction)) {

            myAi.removeAllInvalidOptions();

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }

    private boolean rule8ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();



        if (myAi.checkGridForCellLineReduction_Rule8_ReturnTrueOnEvent(singleAction)) {

            myAi.removeAllInvalidOptions();

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }

    private boolean rule9ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();

        myAi.removeAllInvalidOptions();

        if (myAi.checkGridForXWings_Rule9_ReturnTrueOnEvent(singleAction)) {

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }


    private boolean rule10ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();

        myAi.removeAllInvalidOptions();

        if (myAi.checkGridForHiddenPairs_Rule10_ReturnTrueOnEvent(singleAction)) {

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }

    private boolean rule11ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();

        myAi.removeAllInvalidOptions();

        if (myAi.checkGridForHiddenTriples_Rule11_ReturnTrueOnEvent(singleAction)) {

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }


    private boolean rule12ReturnSuccess(boolean singleAction) {



        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();

        myAi.removeAllInvalidOptions();

        if (myAi.checkGridForHiddenQuad_Rule12_ReturnTrueOnEvent(singleAction)) {

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }

    private boolean rule13ReturnSuccess(boolean singleAction) {


        Grid myGrid = Grid.getInstance();
        Ai myAi = Ai.getInstance();

        myAi.removeAllInvalidOptions();

        if (myAi.checkGridForYWings_Rule13_ReturnTrueOnEvent(singleAction)) {

            boolean isComplete = myAi.isPuzzleCompleted_FullVerify();
            myGrid.saveGameState();
            return true;


        } else {
            return false;
        }


    }


}
