package chloetaylor.sudoku_mk4;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Created by Chloe on 26/02/2018.
 */

public class Ai extends Activity {

    TextView iterationTextView;
    TextView solvedSquaresTextView;

    private List<Button> allFunctionButtons;

    private AsyncTask backTrackingTask = null;

    double iterations = 0;

    int heuristicType = 2; // 0: no sorting, 1: set patterned sorting 2: Cells sorted by number of solved squares

    // singleton

    private static Ai instance = null;

    private Ai() {
        // Exists only to defeat instantiation.
    }

    public static Ai getInstance() {
        if (instance == null) {
            instance = new Ai();
        }
        return instance;
    }

    public int getHeuristicType() {
        return heuristicType;
    }

    public void setHeuristicType(int heuristicType) {
        this.heuristicType = heuristicType;
    }

    public TextView getIterationTextView() {
        return iterationTextView;
    }

    public void setIterationTextView(TextView iterationTextView) {
        this.iterationTextView = iterationTextView;
    }

    public void setIterationTextViewText(final String string) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                iterationTextView.setText(string);

            }
        });
    }

    public void setAllFunctionButtons(List<Button> allFunctionButtons) {
        this.allFunctionButtons = allFunctionButtons;
    }

    public TextView getSolvedSquaresTextView() {
        return solvedSquaresTextView;
    }

    public void setSolvedSquaresTextView(TextView solvedSquaresTextView) {
        this.solvedSquaresTextView = solvedSquaresTextView;
    }

    public void updateIterationsTextView(final double iterationsCount, final long timeDuration) {

        iterations = iterationsCount;

        final DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        df.setMaximumFractionDigits(340); // 340 = DecimalFormat.DOUBLE_FRACTION_DIGITS

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // WORK on UI thread here

                long seconds;
                long minutes;
                long hours;

                seconds = timeDuration / 1000;
                minutes = seconds / 60;
                seconds = seconds % 60;
                hours = minutes / 60;
                minutes = minutes % 60;

                String time = Long.toString(hours) + " H : " + Long.toString(minutes) + " M : " + Long.toString(seconds) + " S";

                iterationTextView.setText("Back Tracking Iterations = " + df.format(iterationsCount) + "\n" + time);

            }
        });


    }

    public void clearIterationsTextView() {


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // WORK on UI thread here

                iterationTextView.setText("");

            }
        });


    }


    public void setIterations(double iterations) {
        this.iterations = iterations;
    }

    public void resetIterations() {
        iterations = 0;
    }


    public void removeAllInvalidOptions() {


        List<Square> allSquares = Grid.getInstance().getAllSquaresOrderedBySquareNumber();

        for (Square square : allSquares) {

            if (square.isHasBeenSolved() == false) {

                int[] optionArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

                for (int option : optionArray) {

                    if (doesSquareProposedSolutionGiveAnyGridConflicts(square, option) == true) {
                        // invalid needs eliminating
                        square.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option);

                    }
                }
            }
        }
    }

    protected void updateSolvedSquaresTextView() {

        Grid my_grid = Grid.getInstance();

        final int numberOfSolvedSquares = my_grid.getNumberOfSolvedSquares();

        // set solved squares textview

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // WORK on UI thread here

                solvedSquaresTextView.setText("Solved Squares = " + Integer.toString(numberOfSolvedSquares));

            }
        });


    }


    protected boolean isPuzzleCompleted_FullVerify() {

        updateSolvedSquaresTextView();

        Grid my_grid = Grid.getInstance();

        ArrayList<Square> allSquares = my_grid.getAllSquaresOrderedBySquareNumberReversedOrder();

        for (Square square : allSquares) {

            if (!square.isHasBeenSolved()){
                return false;
            }

            if (square.getDiscoveredAnswer() == -1) {
                // included as during backtracking a square is not set as solved
                return false;
            }


            if (doesSquareProposedSolutionGiveAnyGridConflicts(square, square.getDiscoveredAnswer())) {
                return false;
            }


        }

        reenableAllButtons();

        return true;
    }


    protected boolean doesSquareProposedSolutionGiveAnyGridConflicts(Square selectedSquare, int proposedValue) {


        int cellNumber = selectedSquare.getCellNumber();
        int rowNumber = selectedSquare.getGridRowNumber();
        int columnNumber = selectedSquare.getGridColumnNumber();

        ArrayList<Square> cellArray = Grid.getInstance().getAllSquaresInACell(cellNumber);
        ArrayList<Square> rowArray = Grid.getInstance().getAllSquaresInAGridRow(rowNumber);
        ArrayList<Square> columnArray = Grid.getInstance().getAllSquaresInAGridColumn(columnNumber);

        for (Square square : cellArray) {
            if (square.getDiscoveredAnswer() == proposedValue && selectedSquare.getSquareNumber() != square.getSquareNumber()) {
                return true;
            }
        }

        for (Square square : rowArray) {
            if (square.getDiscoveredAnswer() == proposedValue && selectedSquare.getSquareNumber() != square.getSquareNumber()) {
                return true;
            }
        }

        for (Square square : columnArray) {
            if (square.getDiscoveredAnswer() == proposedValue && selectedSquare.getSquareNumber() != square.getSquareNumber()) {
                return true;
            }
        }

        return false;
    }


    // ---------------- rule 1 -----------------------

    // When a square has just one candidate, that number goes into the square.

    protected boolean checkGridForSoleRemainingOptionSolutions_Rule1_ReturnTrueOnEvent(boolean singleAction) {


        for (final Square square : Grid.getInstance().getAllSquaresOrderedBySquareNumber()) {
            if (square.setSolutionIfThereIsASoleRemainingOptionReturnIfSquareWasAlreadyMarkedAsDiscovered()) {
                System.out.println("----- #Rule1 -----  Sole Option! Square " + square.getSquareNumberAsString()
                        + " was correctly set to the single remaining option " + square.getDiscoveredAnswerAsHexString());

                setIterationTextViewText("----- #Rule1 ----- Sole Option!\nSquare " + square.getSquareNumberAsString()
                        + " was correctly set to the single remaining option " + square.getDiscoveredAnswerAsHexString());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // WORK on UI thread here

                        Grid grid = Grid.getInstance();

                        GridButton gridButton = grid.getGridButtonArrayList().get(square.getSquareNumber());

                        gridButton.setTextToFoundSolution();

                    }
                });

                return true;

            }
        }

        if (singleAction){

            setIterationTextViewText("----- #Rule1 ----- Sole Option!\nNo solution found");
        }

        return false;
    }


    // ---------------- rule 2 -----------------------

    // When a candidate number appears just once in an area (row, column or cell), that number goes into that square.

    protected boolean checkGridForExclusiveOptionSolutions_Rule2_ReturnTrueOnEvent(boolean singleAction) {


        ArrayList<ArrayList<Square>> collection = new ArrayList<>();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};


        for (int index : indexArray) {

            // check each cell, column, and row
            collection.add(Grid.getInstance().getAllSquaresInACell(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridColumn(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridRow(index));

        }

        // iterate through the collection

        for (ArrayList<Square> gridSquaresList : collection) {


            int numberOfZeros = 0;
            int numberOfOnes = 0;
            int numberOfTwos = 0;
            int numberOfThrees = 0;
            int numberOfFours = 0;
            int numberOfFives = 0;
            int numberOfSixes = 0;
            int numberOfSevens = 0;
            int numberOfEights = 0;
            int numberOfNines = 0;
            int numberOfTens = 0;
            int numberOfElevens = 0;
            int numberOfTwelves = 0;
            int numberOfThirteens = 0;
            int numberOfFourteens = 0;
            int numberOfFifteens = 0;


            // get the tallies
            for (Square gridSquare : gridSquaresList) {


                ArrayList<Integer> remainingOptions = gridSquare.getOptionsStillPossible();

                for (int number : remainingOptions) {


                    switch (number) {
                        case 0:
                            numberOfZeros = numberOfZeros + 1;
                            break;
                        case 1:
                            numberOfOnes = numberOfOnes + 1;
                            break;
                        case 2:
                            numberOfTwos = numberOfTwos + 1;
                            break;
                        case 3:
                            numberOfThrees = numberOfThrees + 1;
                            break;
                        case 4:
                            numberOfFours = numberOfFours + 1;
                            break;
                        case 5:
                            numberOfFives = numberOfFives + 1;
                            break;
                        case 6:
                            numberOfSixes = numberOfSixes + 1;
                            break;
                        case 7:
                            numberOfSevens = numberOfSevens + 1;
                            break;
                        case 8:
                            numberOfEights = numberOfEights + 1;
                            break;
                        case 9:
                            numberOfNines = numberOfNines + 1;
                            break;
                        case 10:
                            numberOfTens = numberOfTens + 1;
                            break;
                        case 11:
                            numberOfElevens = numberOfElevens + 1;
                            break;
                        case 12:
                            numberOfTwelves = numberOfTwelves + 1;
                            break;
                        case 13:
                            numberOfThirteens = numberOfThirteens + 1;
                            break;
                        case 14:
                            numberOfFourteens = numberOfFourteens + 1;
                            break;
                        case 15:
                            numberOfFifteens = numberOfFifteens + 1;
                            break;
                        default:
                            break;
                    }

                }

            }

            //  collect the options which have a single tally
            ArrayList<Integer> solutionsWithOneInstance = new ArrayList<>();

            if (numberOfZeros == 1) {
                solutionsWithOneInstance.add(0);
            }
            if (numberOfOnes == 1) {
                solutionsWithOneInstance.add(1);
            }
            if (numberOfTwos == 1) {
                solutionsWithOneInstance.add(2);
            }
            if (numberOfThrees == 1) {
                solutionsWithOneInstance.add(3);
            }
            if (numberOfFours == 1) {
                solutionsWithOneInstance.add(4);
            }
            if (numberOfFives == 1) {
                solutionsWithOneInstance.add(5);
            }
            if (numberOfSixes == 1) {
                solutionsWithOneInstance.add(6);
            }
            if (numberOfSevens == 1) {
                solutionsWithOneInstance.add(7);
            }
            if (numberOfEights == 1) {
                solutionsWithOneInstance.add(8);
            }
            if (numberOfNines == 1) {
                solutionsWithOneInstance.add(9);
            }
            if (numberOfTens == 1) {
                solutionsWithOneInstance.add(10);
            }
            if (numberOfElevens == 1) {
                solutionsWithOneInstance.add(11);
            }
            if (numberOfTwelves == 1) {
                solutionsWithOneInstance.add(12);
            }
            if (numberOfThirteens == 1) {
                solutionsWithOneInstance.add(13);
            }
            if (numberOfFourteens == 1) {
                solutionsWithOneInstance.add(14);
            }
            if (numberOfFifteens == 1) {
                solutionsWithOneInstance.add(15);
            }


            // iterate through single solution squares
            for (int numberToCheckFor : solutionsWithOneInstance) {

                for (final Square square : gridSquaresList) {

                    if (square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor) && !square.isHasBeenSolved()) {

                        // safe set as is only square with this number

                        if (square.setSolutionWithoutEliminationReturnTrueIfStoredNumberMatchedTheCorrectAnswer(numberToCheckFor)) {

                            System.out.println("----- #Rule2 -----  Exclusive! Square " + square.getSquareNumberAsString() + " was correctly set to " + square.getDiscoveredAnswerAsHexString());

                            setIterationTextViewText("----- #Rule2 ----- Exclusive!\nSquare " + square.getSquareNumberAsString() + " was correctly set to " + square.getDiscoveredAnswerAsHexString());

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // WORK on UI thread here


                                    Grid grid = Grid.getInstance();

                                    GridButton gridButton = grid.getGridButtonArrayList().get(square.getSquareNumber());

                                    gridButton.setTextToFoundSolution();

                                }
                            });
                            return true;
                        } else {

                            System.out.println("----- ERROR #Rule2 -----Square " + square.getSquareNumberAsString() + " was set to the wrong answer " + square.getDiscoveredAnswerAsHexString()
                                    + ", expected the answer" + square.getCorrectAnswerAsHexString());

                            setIterationTextViewText("----- ERROR #Rule2 -----\nSquare " + square.getSquareNumberAsString() + " was set to the wrong answer " + square.getDiscoveredAnswerAsHexString()
                                    + ", expected the answer" + square.getCorrectAnswerAsHexString());

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    // WORK on UI thread here

                                    Grid grid = Grid.getInstance();

                                    GridButton gridButton = grid.getGridButtonArrayList().get(square.getSquareNumber());

                                    gridButton.setTextToFoundSolution();

                                }
                            });
                            return true;
                        }

                    }


                }

            }

        }

        if (singleAction){

            setIterationTextViewText("----- #Rule2 ----- Exclusive!\nNo solution found");
        }

        return false;
    }


    // --------------------- rule 3 ----------------------

    // When a candidate number only appears in one row or column of a cell, the cell 'claims' that number within the entire row or column.

    public boolean checkGridForNumberClaiming_Rule3_ReturnTrueOnEvent(boolean singleAction) {

        Grid my_grid = Grid.getInstance();

        ArrayList<ArrayList<ArrayList<Square>>> collection = new ArrayList<>();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

        int[] cellIndexArray = {0, 1, 2, 3};


        // columns first then rows
        for (int cellIndex : cellIndexArray) {

            collection.add(my_grid.getAllSquaresInACellColumn(cellIndex));
            collection.add(my_grid.getAllSquaresInACellRow(cellIndex));
        }


        // iterate through the collection, they split by cells

        int count = -1;

        for (ArrayList<ArrayList<Square>> squaresInACellRowOrColumn : collection) {

            count = count + 1;

            // working with an array of cells in either a column or array from here

            boolean isColumnAndNotRow = false;

            if (count % 2 == 0) {
                isColumnAndNotRow = true;
            }


            ArrayList<Square> cell_a = new ArrayList<>();
            ArrayList<Square> cell_b = new ArrayList<>();
            ArrayList<Square> cell_c = new ArrayList<>();
            ArrayList<Square> cell_d = new ArrayList<>();


            // separate the collection
            cell_a.addAll(squaresInACellRowOrColumn.get(0));
            cell_b.addAll(squaresInACellRowOrColumn.get(1));
            cell_c.addAll(squaresInACellRowOrColumn.get(2));
            cell_d.addAll(squaresInACellRowOrColumn.get(3));


            if (isColumnAndNotRow) {

                // method for columns


                // for the cells in the collection

                int cellIndex = -1;

                for (ArrayList<Square> cell : squaresInACellRowOrColumn) {


                    cellIndex = cellIndex + 1;

                    for (int numberToCheckFor : indexArray) {


                        int countOfIndexInColumn0 = 0;
                        int countOfIndexInColumn1 = 0;
                        int countOfIndexInColumn2 = 0;
                        int countOfIndexInColumn3 = 0;


                        for (Square square : cell) {


                            if (square.isHasBeenSolved() == false) {

                                if (square.getColumnWithinACellNumber() == 0 && square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {
                                    countOfIndexInColumn0 = countOfIndexInColumn0 + 1;
                                } else if (square.getColumnWithinACellNumber() == 1 && square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {
                                    countOfIndexInColumn1 = countOfIndexInColumn1 + 1;
                                } else if (square.getColumnWithinACellNumber() == 2 && square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {
                                    countOfIndexInColumn2 = countOfIndexInColumn2 + 1;
                                } else if (square.getColumnWithinACellNumber() == 3 && square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {
                                    countOfIndexInColumn3 = countOfIndexInColumn3 + 1;
                                }
                            }

                        }

                        int selectedColumnInTheCell = -1;
                        boolean okToProceed = false;

                        if (countOfIndexInColumn0 > 0 && countOfIndexInColumn1 == 0 && countOfIndexInColumn2 == 0 && countOfIndexInColumn3 == 0) {
                            selectedColumnInTheCell = 0;
                            okToProceed = true;

                        } else if (countOfIndexInColumn0 == 0 && countOfIndexInColumn1 > 0 && countOfIndexInColumn2 == 0 && countOfIndexInColumn3 == 0) {
                            selectedColumnInTheCell = 1;
                            okToProceed = true;

                        } else if (countOfIndexInColumn0 == 0 && countOfIndexInColumn1 == 0 && countOfIndexInColumn2 > 0 && countOfIndexInColumn3 == 0) {
                            selectedColumnInTheCell = 2;
                            okToProceed = true;

                        } else if (countOfIndexInColumn0 == 0 && countOfIndexInColumn1 == 0 && countOfIndexInColumn2 == 0 && countOfIndexInColumn3 > 0) {
                            selectedColumnInTheCell = 3;
                            okToProceed = true;

                        }


                        if (okToProceed == true) {
                            // if here then the index is exclusively in this column of the cell and can't be present in the same column for the other cells

                            int squaresAltered = 0;

                            // remove the option from the same column in the other cells

                            ArrayList<ArrayList<Square>> otherCells = new ArrayList<>();

                            if (cellIndex != 0) {
                                otherCells.add(cell_a);
                            }
                            if (cellIndex != 1) {
                                otherCells.add(cell_b);
                            }
                            if (cellIndex != 2) {
                                otherCells.add(cell_c);
                            }
                            if (cellIndex != 3) {
                                otherCells.add(cell_d);
                            }


                            for (ArrayList<Square> otherCell : otherCells) {

                                for (Square square : otherCell) {


                                    if (square.getColumnWithinACellNumber() == selectedColumnInTheCell && !square.isHasBeenSolved()) {


                                        // try to eliminate the number


                                        if (square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {

                                            if (square.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(numberToCheckFor)) {


                                                System.out.println("----- #Rule3 -----Number Claim! Square " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());

                                                setIterationTextViewText("----- #Rule3 ----- Number Claim!\nSquare " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());

                                                squaresAltered = squaresAltered + 1;

                                            } else {

                                                System.out.println("----- ERROR #Rule3 -----  Error attempting to remove option " + Integer.toHexString(numberToCheckFor) + " from Square " + square.getSquareNumberAsString());
                                                setIterationTextViewText("----- ERROR #Rule3 -----\nError attempting to remove option " + Integer.toHexString(numberToCheckFor) + " from Square " + square.getSquareNumberAsString());

                                                squaresAltered = squaresAltered + 1;

                                            }

                                        }

                                    }

                                }


                            }

                            if (squaresAltered > 0) {
                                return true;
                            }

                        }


                    }

                }


            } else {

                // is row


                // for the cells in the collection

                int cellIndex = -1;

                for (ArrayList<Square> cell : squaresInACellRowOrColumn) {

                    cellIndex = cellIndex + 1;

                    for (int numberToCheckFor : indexArray) {


                        int countOfIndexInRow0 = 0;
                        int countOfIndexInRow1 = 0;
                        int countOfIndexInRow2 = 0;
                        int countOfIndexInRow3 = 0;


                        for (Square square : cell) {


                            if (square.isHasBeenSolved() == false) {

                                if (square.getRowWithinACellNumber() == 0 && square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {
                                    countOfIndexInRow0 = countOfIndexInRow0 + 1;
                                } else if (square.getRowWithinACellNumber() == 1 && square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {
                                    countOfIndexInRow1 = countOfIndexInRow1 + 1;
                                } else if (square.getRowWithinACellNumber() == 2 && square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {
                                    countOfIndexInRow2 = countOfIndexInRow2 + 1;
                                } else if (square.getRowWithinACellNumber() == 3 && square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {
                                    countOfIndexInRow3 = countOfIndexInRow3 + 1;
                                }
                            }

                        }

                        int selectedRowInTheCell = -1;
                        boolean okToProceed = false;

                        if (countOfIndexInRow0 > 0 && countOfIndexInRow1 == 0 && countOfIndexInRow2 == 0 && countOfIndexInRow3 == 0) {
                            selectedRowInTheCell = 0;
                            okToProceed = true;

                        } else if (countOfIndexInRow0 == 0 && countOfIndexInRow1 > 0 && countOfIndexInRow2 == 0 && countOfIndexInRow3 == 0) {
                            selectedRowInTheCell = 1;
                            okToProceed = true;

                        } else if (countOfIndexInRow0 == 0 && countOfIndexInRow1 == 0 && countOfIndexInRow2 > 0 && countOfIndexInRow3 == 0) {
                            selectedRowInTheCell = 2;
                            okToProceed = true;

                        } else if (countOfIndexInRow0 == 0 && countOfIndexInRow1 == 0 && countOfIndexInRow2 == 0 && countOfIndexInRow3 > 0) {
                            selectedRowInTheCell = 3;
                            okToProceed = true;

                        }


                        if (okToProceed == true) {

                            int squaresAltered = 0;


                            // if here then the index is exclusively in this row of the cell and can't be present in the same row for the other cells

                            // remove the option from the same row in the other cells

                            ArrayList<ArrayList<Square>> otherCells = new ArrayList<>();

                            if (cellIndex != 0) {
                                otherCells.add(cell_a);
                            }
                            if (cellIndex != 1) {
                                otherCells.add(cell_b);
                            }
                            if (cellIndex != 2) {
                                otherCells.add(cell_c);
                            }
                            if (cellIndex != 3) {
                                otherCells.add(cell_d);
                            }


                            for (ArrayList<Square> otherCell : otherCells) {

                                for (final Square square : otherCell) {


                                    if (square.getRowWithinACellNumber() == selectedRowInTheCell && !square.isHasBeenSolved()) {


                                        // try to eliminate the number


                                        if (square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {

                                            if (square.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(numberToCheckFor)) {


                                                System.out.println("----- #Rule3 -----  Number Claim! Square " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());

                                                setIterationTextViewText("----- #Rule3 ----- Number Claim!\nSquare " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());


                                                squaresAltered = squaresAltered + 1;

                                            } else {

                                                System.out.println("----- ERROR #Rule3 -----  Error attempting to remove option " + Integer.toHexString(numberToCheckFor) + " from Square " + square.getSquareNumberAsString());

                                                setIterationTextViewText("----- ERROR #Rule3 -----\nError attempting to remove option " + Integer.toHexString(numberToCheckFor) + " from Square " + square.getSquareNumberAsString());

                                                squaresAltered = squaresAltered + 1;

                                            }

                                        }

                                    }

                                }

                            }

                            if (squaresAltered > 0) {
                                return true;
                            }

                        }

                    }

                }


            }


        }


        if (singleAction){

            setIterationTextViewText("----- #Rule3 ----- Number Claim!\nNo solution found");
        }

        return false;
    }


// ----------------- rule 4 -------------------

    // When two squares in the same area (row, column or cell) have identical two-number candidate lists, you can remove both numbers from other
    // candidate lists in that area.

    public boolean checkGridForPairs_Rule4_ReturnTrueOnEvent(boolean singleAction) {

        ArrayList<ArrayList<Square>> collection = new ArrayList<>();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

        for (int index : indexArray) {

            // check each cell, column, and row
            collection.add(Grid.getInstance().getAllSquaresInACell(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridColumn(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridRow(index));

        }


        // iterate through the collection


        for (ArrayList<Square> setList : collection) {


            // working with an array of cells in either a column or array from here


            List<Square> gridSquaresWithTwoOptions = new ArrayList<>();
            List<Square> gridSquaresWithoutTwoOptions = new ArrayList<>();

            for (Square square : setList) {


                if (square.getNumberOfRemainingOptions() == 2) {
                    gridSquaresWithTwoOptions.add(square);
                } else {
                    gridSquaresWithoutTwoOptions.add(square);
                }


            }


            if (gridSquaresWithTwoOptions.size() >= 2) {

                // search for matching pairs


                for (int i = 0; i < gridSquaresWithTwoOptions.size(); i = i + 1) {
                    for (int j = i + 1; j < gridSquaresWithTwoOptions.size(); j = j + 1) {


                        if (gridSquaresWithTwoOptions.get(i).getOptionsStillPossible().containsAll(gridSquaresWithTwoOptions.get(j).getOptionsStillPossible())) {


                            int option_1 = gridSquaresWithTwoOptions.get(i).getOptionsStillPossible().get(0);
                            int option_2 = gridSquaresWithTwoOptions.get(i).getOptionsStillPossible().get(1);


                            // remove these two number from rest of options

                            for (Square square : gridSquaresWithoutTwoOptions) {


                                boolean option1Succcess = square.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option_1);
                                boolean option2Succcess = square.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option_2);

                                if (option1Succcess && option2Succcess) {

                                    System.out.println("----- #Rule4 -----  Pair found! Square " + square.getSquareNumberAsString() + " could not be " + Integer.toHexString(option_1).toUpperCase() +
                                            " or " + Integer.toHexString(option_2).toUpperCase());

                                    setIterationTextViewText("----- #Rule4 ----- Pair found!\nSquare " + square.getSquareNumberAsString() + " could not be " + Integer.toHexString(option_1).toUpperCase() +
                                            " or " + Integer.toHexString(option_2).toUpperCase());
                                    return true;

                                } else if (option1Succcess && !option2Succcess) {

                                    System.out.println("----- #Rule4 -----  Pair found! Square " + square.getSquareNumberAsString() + " could not be " + Integer.toHexString(option_1).toUpperCase());
                                    setIterationTextViewText("----- #Rule4 ----- Pair found!\nSquare " + square.getSquareNumberAsString() + " could not be " + Integer.toHexString(option_1).toUpperCase());
                                    return true;

                                } else if (!option1Succcess && option2Succcess) {

                                    System.out.println("----- #Rule4 -----  Pair found! Square " + square.getSquareNumberAsString() + " could not be " + Integer.toHexString(option_2).toUpperCase());
                                    setIterationTextViewText("----- #Rule4 ----- Pair found!\nSquare " + square.getSquareNumberAsString() + " could not be " + Integer.toHexString(option_2).toUpperCase());
                                    return true;

                                }


                            }
                        }
                    }


                }

            }

        }

        if (singleAction){

            setIterationTextViewText("----- #Rule4 ----- Pairs!\nNo solution found");
        }

        return false;
    }


    // ---------------- rule 5 -----------------

    // Three squares in an area (row, column or box) form a triple when:
    // None of them has more than three candidates.
    // Their candidate lists are all full or sub sets of the same three-candidate list.
    // You can remove numbers that appear in the triple from other candidate lists in the same area.


    public boolean checkGridForTriples_Rule5_ReturnTrueOnEvent(boolean singleAction) {

        ArrayList<ArrayList<Square>> collection = new ArrayList<>();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};


        for (int index : indexArray) {

            // check each cell, column, and row


            collection.add(Grid.getInstance().getAllSquaresInACell(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridColumn(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridRow(index));

        }


        for (ArrayList<Square> setList : collection) {


            ArrayList<Square> gridSquaresWithTwoOrThreeOptions = new ArrayList<>();
            ArrayList<Square> gridSquaresWithoutTwoOrThreeOptions = new ArrayList<>();


            for (Square square : setList) {


                if (square.getNumberOfRemainingOptions() == 2 || square.getNumberOfRemainingOptions() == 3) {
                    gridSquaresWithTwoOrThreeOptions.add(square);
                } else {
                    if (square.isHasBeenSolved() == false) {
                        gridSquaresWithoutTwoOrThreeOptions.add(square);
                    }
                }

            }


// find if a triplet - must be at least 3 sets

            if (gridSquaresWithTwoOrThreeOptions.size() >= 3) {

                int n = gridSquaresWithTwoOrThreeOptions.size();

                for (int i = 0; i < n; i = i + 1) {

                    for (int j = i + 1; j < n; j = j + 1) {

                        for (int k = j + 1; k < n; k = k + 1) {


                            Square gridSquare_i = gridSquaresWithTwoOrThreeOptions.get(i);
                            Square gridSquare_j = gridSquaresWithTwoOrThreeOptions.get(j);
                            Square gridSquare_k = gridSquaresWithTwoOrThreeOptions.get(k);

                            ArrayList<Square> possibleTriple = new ArrayList<>();

                            possibleTriple.add(gridSquare_i);
                            possibleTriple.add(gridSquare_j);
                            possibleTriple.add(gridSquare_k);

                            // sort possibleTriple to make sure largest set at the front

                            Collections.sort(possibleTriple, new Comparator<Square>() {

                                public int compare(Square o1, Square o2) {
                                    return Integer.valueOf(o2.getNumberOfRemainingOptions()).compareTo(Integer.valueOf(o1.getNumberOfRemainingOptions()));
                                }
                            });


                            int biggestSetContainsThisMainOptions = possibleTriple.get(0).getOptionsStillPossible().size();

                            // triple rule (a) if a 3 size set contains all the other

                            int removedOptionCount = 0;

                            if (biggestSetContainsThisMainOptions == 2 || biggestSetContainsThisMainOptions == 3) {
                                // triple rule (b) if 3 x 2 size set combine for a three set number


                                // if first set contains 2 and third then its valid

                                ArrayList<Integer> set0 = possibleTriple.get(0).getOptionsStillPossible();
                                ArrayList<Integer> set1 = possibleTriple.get(1).getOptionsStillPossible();
                                ArrayList<Integer> set2 = possibleTriple.get(2).getOptionsStillPossible();

                                ArrayList<Integer> combineWithoutDuplicates = new ArrayList<>();

                                combineWithoutDuplicates.addAll(set0);
                                combineWithoutDuplicates.removeAll(set1);
                                combineWithoutDuplicates.addAll(set1);
                                combineWithoutDuplicates.removeAll(set2);
                                combineWithoutDuplicates.addAll(set2);


                                // if there are three numbers in the combine it must be three pairs containing 3 different subsets of only 3 numbers
                                // these can be treated as a triple and removed from the rest
                                if (combineWithoutDuplicates.size() == 3) {

                                    // yippee valid triple

//
//                                System.out.println("valid set -  square " + possibleTriple.get(0).getSquareNumberAsString() + " ... " + possibleTriple.get(0).getRemainingOptionsString() + "\n" +
//                                        "square " + possibleTriple.get(1).getSquareNumberAsString() + " ... " + possibleTriple.get(1).getRemainingOptionsString() + "\n" +
//                                "square " + possibleTriple.get(2).getSquareNumberAsString() + " ... " + possibleTriple.get(2).getRemainingOptionsString());
//


                                    // combine the withs squares with the withouts and remove the triple

                                    ArrayList<Square> theNoneTripleSquares = new ArrayList<>();
                                    theNoneTripleSquares.addAll(gridSquaresWithTwoOrThreeOptions);
                                    theNoneTripleSquares.addAll(gridSquaresWithoutTwoOrThreeOptions);

                                    theNoneTripleSquares.remove(possibleTriple.get(0));
                                    theNoneTripleSquares.remove(possibleTriple.get(1));
                                    theNoneTripleSquares.remove(possibleTriple.get(2));

                                    // remove the current sets from the with and add to the none triples


                                    for (Square square3 : theNoneTripleSquares) {


                                        for (int numberToCheckFor : combineWithoutDuplicates) {

                                            if (square3.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(numberToCheckFor)) {

                                                if (biggestSetContainsThisMainOptions == 2) {
                                                    System.out.println("----- #Rule5B ----  222 Triple Found! Square " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                    setIterationTextViewText("----- #Rule5B ----- 222 Triple Found!\nSquare " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                } else if (biggestSetContainsThisMainOptions == 3) {
                                                    System.out.println("----- #Rule5B ----  3xx Triple Found! Square " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                    setIterationTextViewText("----- #Rule5B ----- 3xx Triple Found!\nSquare " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                }
                                                removedOptionCount = removedOptionCount + 1;
                                            }

                                        }

                                    }


                                }


                            }

                            if (removedOptionCount > 0) {
                                return true;
                            }
                        }
                    }
                }


            }


        }

        if (singleAction){

            setIterationTextViewText("----- #Rule5B ----  Triple!\nNo solution found");
        }

        return false;
    }


    // ---------------- rule 6 -----------------

    // Fours squares in an area (row, column or box) form a quad when:
    // None of them has more than four candidates.
    // Their candidate lists are all full or sub sets of the same four-candidate list.
    // You can remove numbers that appear in the quad from other candidate lists in the same area.


    public boolean checkGridForQuads_Rule6_ReturnTrueOnEvent(boolean singleAction) {

        ArrayList<ArrayList<Square>> collection = new ArrayList<>();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};


        for (int index : indexArray) {

            // check each cell, column, and row


            collection.add(Grid.getInstance().getAllSquaresInACell(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridColumn(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridRow(index));

        }


        for (ArrayList<Square> setList : collection) {


            ArrayList<Square> gridSquaresWithTwoOrThreeOrFourOptions = new ArrayList<>();
            ArrayList<Square> gridSquaresWithoutTwoOrThreeOrFourOptions = new ArrayList<>();


            for (Square square : setList) {


                if (square.getNumberOfRemainingOptions() == 2 || square.getNumberOfRemainingOptions() == 3 || square.getNumberOfRemainingOptions() == 4) {
                    gridSquaresWithTwoOrThreeOrFourOptions.add(square);
                } else {
                    if (square.isHasBeenSolved() == false) {
                        gridSquaresWithoutTwoOrThreeOrFourOptions.add(square);
                    }
                }

            }


// find if a quad - must be at least 4 sets

            if (gridSquaresWithTwoOrThreeOrFourOptions.size() >= 4) {

                int n = gridSquaresWithTwoOrThreeOrFourOptions.size();

                for (int i = 0; i < n; i = i + 1) {

                    for (int j = i + 1; j < n; j = j + 1) {

                        for (int k = j + 1; k < n; k = k + 1) {

                            for (int m = k + 1; m < n; m = m + 1) {


                                Square gridSquare_i = gridSquaresWithTwoOrThreeOrFourOptions.get(i);
                                Square gridSquare_j = gridSquaresWithTwoOrThreeOrFourOptions.get(j);
                                Square gridSquare_k = gridSquaresWithTwoOrThreeOrFourOptions.get(k);
                                Square gridSquare_m = gridSquaresWithTwoOrThreeOrFourOptions.get(m);

                                ArrayList<Square> possibleQuad = new ArrayList<>();

                                possibleQuad.add(gridSquare_i);
                                possibleQuad.add(gridSquare_j);
                                possibleQuad.add(gridSquare_k);
                                possibleQuad.add(gridSquare_m);

                                // sort possibleTriple to make sure largest set at the front

                                Collections.sort(possibleQuad, new Comparator<Square>() {

                                    public int compare(Square o1, Square o2) {
                                        return Integer.valueOf(o2.getNumberOfRemainingOptions()).compareTo(Integer.valueOf(o1.getNumberOfRemainingOptions()));
                                    }
                                });

                                int removedOptionCount = 0;

                                int biggestSetSize = possibleQuad.get(0).getOptionsStillPossible().size();

                                // Quad rule (a) if a 4 size set contains all the other
                                if (biggestSetSize >= 2 && biggestSetSize <= 4) {


                                    ArrayList<Integer> set0 = possibleQuad.get(0).getOptionsStillPossible();
                                    ArrayList<Integer> set1 = possibleQuad.get(1).getOptionsStillPossible();
                                    ArrayList<Integer> set2 = possibleQuad.get(2).getOptionsStillPossible();
                                    ArrayList<Integer> set3 = possibleQuad.get(3).getOptionsStillPossible();

                                    ArrayList<Integer> combineWithoutDuplicates = new ArrayList<>();

                                    combineWithoutDuplicates.addAll(set0);
                                    combineWithoutDuplicates.removeAll(set1);
                                    combineWithoutDuplicates.addAll(set1);
                                    combineWithoutDuplicates.removeAll(set2);
                                    combineWithoutDuplicates.addAll(set2);
                                    combineWithoutDuplicates.removeAll(set3);
                                    combineWithoutDuplicates.addAll(set3);


                                    // if there are four numbers in the combine it must be four subests containing only 4 unique numbers
                                    // these can be treated as a quad and removed from the rest
                                    if (combineWithoutDuplicates.size() == 4) {

                                        // yippee valid quad

//


                                        // combine the withs squares with the withouts and remove the triple

                                        ArrayList<Square> theNoneQuadSquares = new ArrayList<>();
                                        theNoneQuadSquares.addAll(gridSquaresWithTwoOrThreeOrFourOptions);
                                        theNoneQuadSquares.addAll(gridSquaresWithoutTwoOrThreeOrFourOptions);

                                        theNoneQuadSquares.remove(possibleQuad.get(0));
                                        theNoneQuadSquares.remove(possibleQuad.get(1));
                                        theNoneQuadSquares.remove(possibleQuad.get(2));
                                        theNoneQuadSquares.remove(possibleQuad.get(3));

                                        // remove the current sets from the with and add to the none triples


                                        for (Square square3 : theNoneQuadSquares) {


                                            for (int numberToCheckFor : combineWithoutDuplicates) {

                                                if (square3.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(numberToCheckFor)) {

                                                    if (biggestSetSize == 4) {
                                                        System.out.println("----- #Rule6A ----  4xxx Quad Found! Square " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                        setIterationTextViewText("----- #Rule6A ----- 4xxx Quad Found!\nSquare " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());

                                                    } else if (biggestSetSize == 3) {
                                                        System.out.println("----- #Rule6B ----  3xxx Quad Found! Square " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                        setIterationTextViewText("----- #Rule6B ----- 3xxx Quad Found!\nSquare " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());

                                                    } else if (biggestSetSize == 2) {
                                                        System.out.println("----- #Rule6C ----  2222 Quad Found! Square " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                        setIterationTextViewText("----- #Rule6C ----- 2222 Quad Found!\nSquare " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());

                                                    }

                                                    removedOptionCount = removedOptionCount + 1;
                                                }

                                            }

                                        }

                                        if (removedOptionCount > 0) {

                                            return true;
                                        }

                                    }


                                }
                            }
                        }
                    }
                }


            }


        }

        if (singleAction){

            setIterationTextViewText("----- #Rule6C ----- Quad!\nNo solution found");
        }

        return false;
    }


    // ---------------- rule 7 -----------------

    // Fives squares in an area (row, column or box) form a quint when:
    // None of them has more than five candidates.
    // Their candidate lists are all full or sub sets of the same five-candidate list.
    // You can remove numbers that appear in the quint from other candidate lists in the same area.


    public boolean checkGridForQuints_Rule7_ReturnTrueOnEvent(boolean singleAction) {

        ArrayList<ArrayList<Square>> collection = new ArrayList<>();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};


        for (int index : indexArray) {

            // check each cell, column, and row


            collection.add(Grid.getInstance().getAllSquaresInACell(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridColumn(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridRow(index));

        }


        for (ArrayList<Square> setList : collection) {


            ArrayList<Square> gridSquaresWithTwoOrThreeOrFourOrFiveOptions = new ArrayList<>();
            ArrayList<Square> gridSquaresWithoutTwoOrThreeOrFourorFiveOptions = new ArrayList<>();


            for (Square square : setList) {


                if (square.getNumberOfRemainingOptions() == 2 || square.getNumberOfRemainingOptions() == 3 || square.getNumberOfRemainingOptions() == 4 || square.getNumberOfRemainingOptions() == 5) {
                    gridSquaresWithTwoOrThreeOrFourOrFiveOptions.add(square);
                } else {
                    if (square.isHasBeenSolved() == false) {
                        gridSquaresWithoutTwoOrThreeOrFourorFiveOptions.add(square);
                    }
                }

            }


// find if a quint - must be at least 5 sets

            if (gridSquaresWithTwoOrThreeOrFourOrFiveOptions.size() >= 5) {

                int x = gridSquaresWithTwoOrThreeOrFourOrFiveOptions.size();

                for (int i = 0; i < x; i = i + 1) {

                    for (int j = i + 1; j < x; j = j + 1) {

                        for (int k = j + 1; k < x; k = k + 1) {

                            for (int m = k + 1; m < x; m = m + 1) {

                                for (int n = m + 1; n < x; n = n + 1) {

                                    Square gridSquare_i = gridSquaresWithTwoOrThreeOrFourOrFiveOptions.get(i);
                                    Square gridSquare_j = gridSquaresWithTwoOrThreeOrFourOrFiveOptions.get(j);
                                    Square gridSquare_k = gridSquaresWithTwoOrThreeOrFourOrFiveOptions.get(k);
                                    Square gridSquare_m = gridSquaresWithTwoOrThreeOrFourOrFiveOptions.get(m);
                                    Square gridSquare_n = gridSquaresWithTwoOrThreeOrFourOrFiveOptions.get(n);

                                    ArrayList<Square> possibleQuint = new ArrayList<>();

                                    possibleQuint.add(gridSquare_i);
                                    possibleQuint.add(gridSquare_j);
                                    possibleQuint.add(gridSquare_k);
                                    possibleQuint.add(gridSquare_m);
                                    possibleQuint.add(gridSquare_n);

                                    // sort possibleTriple to make sure largest set at the front

                                    Collections.sort(possibleQuint, new Comparator<Square>() {

                                        public int compare(Square o1, Square o2) {
                                            return Integer.valueOf(o2.getNumberOfRemainingOptions()).compareTo(Integer.valueOf(o1.getNumberOfRemainingOptions()));
                                        }
                                    });

                                    int removedOptionCount = 0;

                                    int biggestSetSize = possibleQuint.get(0).getOptionsStillPossible().size();

                                    // Quint rule (a) if a 5 size set contains all the other

                                    if (biggestSetSize >= 2 && biggestSetSize <= 5) {


                                        ArrayList<Integer> set0 = possibleQuint.get(0).getOptionsStillPossible();
                                        ArrayList<Integer> set1 = possibleQuint.get(1).getOptionsStillPossible();
                                        ArrayList<Integer> set2 = possibleQuint.get(2).getOptionsStillPossible();
                                        ArrayList<Integer> set3 = possibleQuint.get(3).getOptionsStillPossible();
                                        ArrayList<Integer> set4 = possibleQuint.get(4).getOptionsStillPossible();

                                        ArrayList<Integer> combineWithoutDuplicates = new ArrayList<>();

                                        combineWithoutDuplicates.addAll(set0);
                                        combineWithoutDuplicates.removeAll(set1);
                                        combineWithoutDuplicates.addAll(set1);
                                        combineWithoutDuplicates.removeAll(set2);
                                        combineWithoutDuplicates.addAll(set2);
                                        combineWithoutDuplicates.removeAll(set3);
                                        combineWithoutDuplicates.addAll(set3);
                                        combineWithoutDuplicates.removeAll(set4);
                                        combineWithoutDuplicates.addAll(set4);

                                        // if there are four numbers in the combine it must be four subests containing only 4 unique numbers
                                        // these can be treated as a quad and removed from the rest
                                        if (combineWithoutDuplicates.size() == 5) {

                                            // yippee valid quint

                                            // combine the withs squares with the withouts and remove the quint

                                            ArrayList<Square> theNoneQuintSquares = new ArrayList<>();
                                            theNoneQuintSquares.addAll(gridSquaresWithTwoOrThreeOrFourOrFiveOptions);
                                            theNoneQuintSquares.addAll(gridSquaresWithoutTwoOrThreeOrFourorFiveOptions);

                                            theNoneQuintSquares.remove(possibleQuint.get(0));
                                            theNoneQuintSquares.remove(possibleQuint.get(1));
                                            theNoneQuintSquares.remove(possibleQuint.get(2));
                                            theNoneQuintSquares.remove(possibleQuint.get(3));
                                            theNoneQuintSquares.remove(possibleQuint.get(4));

                                            // remove the current sets from the with and add to the none quints


                                            for (Square square3 : theNoneQuintSquares) {


                                                for (int numberToCheckFor : combineWithoutDuplicates) {

                                                    if (square3.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(numberToCheckFor)) {

                                                        if (biggestSetSize == 5) {
                                                            System.out.println("----- #Rule7A ----  5xxxx Quint Found! Square " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                            setIterationTextViewText("----- #Rule7A ----- 5xxxx Quint Found!\nSquare " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                        } else if (biggestSetSize == 4) {
                                                            System.out.println("----- #Rule7B ----  4xxxx Quint Found! Square " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                            setIterationTextViewText("----- #Rule7B ----- xxxx Quint Found!\nSquare " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                        } else if (biggestSetSize == 3) {
                                                            System.out.println("----- #Rule7C ----  3xxxx Quint Found! Square " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                            setIterationTextViewText("----- #Rule7C ----- 3xxxx Quint Found!\nSquare " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                        } else if (biggestSetSize == 2) {
                                                            System.out.println("----- #Rule7D ----  22222 Quint Found! Square " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                            setIterationTextViewText("----- #Rule7D ----- 22222 Quint Found!\nSquare " + square3.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                                        }

                                                        removedOptionCount = removedOptionCount + 1;


                                                    }

                                                }

                                            }

                                            if (removedOptionCount > 0) {

                                                return true;
                                            }

                                        }


                                    }
                                }
                            }
                        }
                    }

                }

            }


        }

        if (singleAction){

            setIterationTextViewText("----- #Rule7A ----- Quint!\nNo solution found");
        }

        return false;
    }


    // ---------------- rule 8 -----------------
    // cell line reduction (Reverse of rule 3)

    // If all occurrences of a candidate within a row or column fall inside the same cell,
    // then other occurrences of that candidate can be removed from that cell.


    public boolean checkGridForCellLineReduction_Rule8_ReturnTrueOnEvent(boolean singleAction) {


        Grid my_grid = Grid.getInstance();

        ArrayList<ArrayList<ArrayList<Square>>> collection = new ArrayList<>();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

        int[] cellIndexArray = {0, 1, 2, 3};

        // columns first then rows
        for (int cellIndex : cellIndexArray) {

            collection.add(my_grid.getAllColumnsInACellColumn(cellIndex));
            collection.add(my_grid.getAllRowsInACellRow(cellIndex));
        }


        // iterate through the collection

        int count = -1;

        for (ArrayList<ArrayList<Square>> squaresInACellRowOrColumn : collection) {

            count = count + 1;

            // working with an array of cells in either a column or array from here

            boolean isColumnAndNotRow = false;

            if (count % 2 == 0) {
                isColumnAndNotRow = true;
            }


            for (int numberToCheckFor : indexArray) {

                int numberOfSquaresAltered = 0;


                if (isColumnAndNotRow) {
                    // cycle through the columns

                    for (ArrayList<Square> squaresInAColumn : squaresInACellRowOrColumn) {


                        int thisGridColumnNumber = squaresInAColumn.get(0).getGridColumnNumber();

                        int numberOfOptionInCellRow_0 = 0;
                        int numberOfOptionInCellRow_1 = 0;
                        int numberOfOptionInCellRow_2 = 0;
                        int numberOfOptionInCellRow_3 = 0;

                        int cellNumberForCellRow0 = -1;
                        int cellNumberForCellRow1 = -1;
                        int cellNumberForCellRow2 = -1;
                        int cellNumberForCellRow3 = -1;


                        for (Square square : squaresInAColumn) {


                            if (!square.isHasBeenSolved() && square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {

                                int cellRowNumber = square.getCellRowNumber();

                                if (cellRowNumber == 0) {
                                    numberOfOptionInCellRow_0 = numberOfOptionInCellRow_0 + 1;
                                    cellNumberForCellRow0 = square.getCellNumber();
                                } else if (cellRowNumber == 1) {
                                    numberOfOptionInCellRow_1 = numberOfOptionInCellRow_1 + 1;
                                    cellNumberForCellRow1 = square.getCellNumber();
                                } else if (cellRowNumber == 2) {
                                    numberOfOptionInCellRow_2 = numberOfOptionInCellRow_2 + 1;
                                    cellNumberForCellRow2 = square.getCellNumber();
                                } else if (cellRowNumber == 3) {
                                    numberOfOptionInCellRow_3 = numberOfOptionInCellRow_3 + 1;
                                    cellNumberForCellRow3 = square.getCellNumber();
                                }

                            }
                        }


                        boolean okToProceed = false;
                        int selectedCell = -1;

                        if (numberOfOptionInCellRow_0 > 0 && numberOfOptionInCellRow_1 == 0 && numberOfOptionInCellRow_2 == 0 && numberOfOptionInCellRow_3 == 0) {
                            selectedCell = cellNumberForCellRow0;
                            okToProceed = true;
                        } else if (numberOfOptionInCellRow_0 == 0 && numberOfOptionInCellRow_1 > 0 && numberOfOptionInCellRow_2 == 0 && numberOfOptionInCellRow_3 == 0) {
                            selectedCell = cellNumberForCellRow1;
                            okToProceed = true;
                        } else if (numberOfOptionInCellRow_0 == 0 && numberOfOptionInCellRow_1 == 0 && numberOfOptionInCellRow_2 > 0 && numberOfOptionInCellRow_3 == 0) {
                            selectedCell = cellNumberForCellRow2;
                            okToProceed = true;
                        } else if (numberOfOptionInCellRow_0 == 0 && numberOfOptionInCellRow_1 == 0 && numberOfOptionInCellRow_2 == 0 && numberOfOptionInCellRow_3 > 0) {
                            selectedCell = cellNumberForCellRow3;
                            okToProceed = true;
                        }


                        if (okToProceed) {

                            ArrayList<Square> thisCell = my_grid.getAllSquaresInACell(selectedCell);

                            // check for instances of the option in the cell that are not in this column and eliminate

                            for (Square square : thisCell) {

                                if (square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor) && square.getGridColumnNumber() != thisGridColumnNumber) {

                                    // safe to eliminate

                                    if (square.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(numberToCheckFor)) {

                                        System.out.println("----- #Rule8 -----  Cell Line Reduction Found! Square " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                        setIterationTextViewText("----- #Rule8 ----- Cell Line Reduction Found!\nSquare " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                        numberOfSquaresAltered = numberOfSquaresAltered + 1;
                                    }


                                }


                            }

                        }


                    }


                } else {
                    // row

                    for (ArrayList<Square> squaresInARow : squaresInACellRowOrColumn) {


                        int thisGridRowNumber = squaresInARow.get(0).getGridRowNumber();

                        int numberOfOptionInCellColumn_0 = 0;
                        int numberOfOptionInCellColumn_1 = 0;
                        int numberOfOptionInCellColumn_2 = 0;
                        int numberOfOptionInCellColumn_3 = 0;

                        int cellNumberForCellColumn0 = -1;
                        int cellNumberForCellColumn1 = -1;
                        int cellNumberForCellColumn2 = -1;
                        int cellNumberForCellColumn3 = -1;


                        for (Square square : squaresInARow) {


                            if (!square.isHasBeenSolved() && square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor)) {

                                int cellColumnNumber = square.getCellColumnNumber();

                                if (cellColumnNumber == 0) {
                                    numberOfOptionInCellColumn_0 = numberOfOptionInCellColumn_0 + 1;
                                    cellNumberForCellColumn0 = square.getCellNumber();
                                } else if (cellColumnNumber == 1) {
                                    numberOfOptionInCellColumn_1 = numberOfOptionInCellColumn_1 + 1;
                                    cellNumberForCellColumn1 = square.getCellNumber();
                                } else if (cellColumnNumber == 2) {
                                    numberOfOptionInCellColumn_2 = numberOfOptionInCellColumn_2 + 1;
                                    cellNumberForCellColumn2 = square.getCellNumber();
                                } else if (cellColumnNumber == 3) {
                                    numberOfOptionInCellColumn_3 = numberOfOptionInCellColumn_3 + 1;
                                    cellNumberForCellColumn3 = square.getCellNumber();
                                }

                            }
                        }


                        boolean okToProceed = false;
                        int selectedCell = -1;

                        if (numberOfOptionInCellColumn_0 > 0 && numberOfOptionInCellColumn_1 == 0 && numberOfOptionInCellColumn_2 == 0 && numberOfOptionInCellColumn_3 == 0) {
                            selectedCell = cellNumberForCellColumn0;
                            okToProceed = true;
                        } else if (numberOfOptionInCellColumn_0 == 0 && numberOfOptionInCellColumn_1 > 0 && numberOfOptionInCellColumn_2 == 0 && numberOfOptionInCellColumn_3 == 0) {
                            selectedCell = cellNumberForCellColumn1;
                            okToProceed = true;
                        } else if (numberOfOptionInCellColumn_0 == 0 && numberOfOptionInCellColumn_1 == 0 && numberOfOptionInCellColumn_2 > 0 && numberOfOptionInCellColumn_3 == 0) {
                            selectedCell = cellNumberForCellColumn2;
                            okToProceed = true;
                        } else if (numberOfOptionInCellColumn_0 == 0 && numberOfOptionInCellColumn_1 == 0 && numberOfOptionInCellColumn_2 == 0 && numberOfOptionInCellColumn_3 > 0) {
                            selectedCell = cellNumberForCellColumn3;
                            okToProceed = true;
                        }


                        if (okToProceed) {

                            ArrayList<Square> thisCell = my_grid.getAllSquaresInACell(selectedCell);

                            // check for instances of the option in the cell that are not in this column and eliminate

                            for (Square square : thisCell) {

                                if (square.checkIfOptionIsStillMarkedAsAPossibility(numberToCheckFor) && square.getGridRowNumber() != thisGridRowNumber) {

                                    // safe to eliminate

                                    if (square.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(numberToCheckFor)) {

                                        System.out.println("----- #Rule8 -----  Cell Line Reduction Found! Square " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());
                                        setIterationTextViewText("----- #Rule8 ----- Cell Line Reduction Found!\nSquare " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(numberToCheckFor).toUpperCase());

                                        numberOfSquaresAltered = numberOfSquaresAltered + 1;
                                    }


                                }


                            }

                        }


                    }

                }

                if (numberOfSquaresAltered > 0) {
                    return true;
                }
            }

        }

        if (singleAction){

            setIterationTextViewText("----- #Rule8 ----- Cell Line Reduction!\nNo solution found");
        }

        return false;
    }


    // ---------------- rule 9 -----------------
    // X-Wings

    // An X-Wing pattern occurs when two rows (or two columns) each contain only two cells that
    // hold a matching candidate. This candidate must reside in both rows and share the same two columns or vice versa.
    // Since the candidate has to be in one of these squares it can be eliminated from other squares in the identified rows and columns


    public boolean checkGridForXWings_Rule9_ReturnTrueOnEvent(boolean singleAction) {


        Grid my_grid = Grid.getInstance();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

        ArrayList<ArrayList<Square>> allSquaresByRow = my_grid.getAllSquaresSeperatedByRows();
        ArrayList<ArrayList<Square>> allSquaresByColumn = my_grid.getAllSquaresSeperatedByColumns();

        for (int option : indexArray) {

            for (ArrayList<Square> squaresByRow : allSquaresByRow) {

                int gridRowNumber = squaresByRow.get(0).getGridRowNumber();

                int numberOfInstancesOfTheOptionUpperCorners = 0;

                Square possibleXWingCornerTopLeft = null;
                Square possibleXWingCornerTopRight = null;

                for (Square square : squaresByRow) {

                    if (!square.isHasBeenSolved() && square.checkIfOptionIsStillMarkedAsAPossibility(option)) {

                        if (numberOfInstancesOfTheOptionUpperCorners == 0) {
                            possibleXWingCornerTopLeft = square;
                        } else if (numberOfInstancesOfTheOptionUpperCorners == 1) {
                            possibleXWingCornerTopRight = square;
                        }

                        numberOfInstancesOfTheOptionUpperCorners = numberOfInstancesOfTheOptionUpperCorners + 1;

                    }

                }

                // check if only two instances

                if (numberOfInstancesOfTheOptionUpperCorners == 2) {


                    // go through the rest of the rows looking for a row with two candidates only of the same option
                    // if there is check their columns match for a valid x-wing

                    for (ArrayList<Square> rowOfSquares : allSquaresByRow) {

                        if (rowOfSquares.get(0).getGridRowNumber() > gridRowNumber) {

                            Square possibleXWingCornerBottomLeft = null;
                            Square possibleXWingCornerBottomRight = null;

                            // for efficiency as working down the grid

                            int numberOfInstancesOfTheOptionLowerCorners = 0;

                            for (Square square : rowOfSquares) {

                                if (!square.isHasBeenSolved() && square.checkIfOptionIsStillMarkedAsAPossibility(option)) {

                                    if (numberOfInstancesOfTheOptionLowerCorners == 0) {
                                        possibleXWingCornerBottomLeft = square;
                                    } else if (numberOfInstancesOfTheOptionLowerCorners == 1) {
                                        possibleXWingCornerBottomRight = square;
                                    }

                                    numberOfInstancesOfTheOptionLowerCorners = numberOfInstancesOfTheOptionLowerCorners + 1;


                                }

                            }


                            if (numberOfInstancesOfTheOptionLowerCorners == 2) {

                                // all that is left is to check the corners are in the correct columns

                                if (possibleXWingCornerTopLeft.getGridColumnNumber() == possibleXWingCornerBottomLeft.getGridColumnNumber() &&
                                        possibleXWingCornerTopRight.getGridColumnNumber() == possibleXWingCornerBottomRight.getGridColumnNumber()) {
//
//                                    System.out.println("Success? (r) option = " + Integer.toHexString(option) + " Square tl = " + possibleXWingCornerTopLeft.getSquareNumberAsString() +
//                                            " Square tr = " + possibleXWingCornerTopRight.getSquareNumberAsString() + " Square bl = " + possibleXWingCornerBottomLeft.getSquareNumberAsString() +
//                                            " Square br = " + possibleXWingCornerBottomRight.getSquareNumberAsString());

                                    // as rows anything in the column that is not any of the corners can have the number eliminated

                                    int numberOfSquaresAltered = 0;

                                    ArrayList<Square> leftColumn = my_grid.getAllSquaresInAGridColumn(possibleXWingCornerTopLeft.getGridColumnNumber());
                                    ArrayList<Square> rightColumn = my_grid.getAllSquaresInAGridColumn(possibleXWingCornerTopRight.getGridColumnNumber());


                                    for (Square square : leftColumn) {

                                        if (!square.equals(possibleXWingCornerTopLeft) && !square.equals(possibleXWingCornerBottomLeft)) {

                                            // eliminate option from square

                                            if (square.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                                System.out.println("----- #Rule9 ----  X-Wing Found! Square " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());
                                                setIterationTextViewText("----- #Rule9 ----- X-Wing Found!\nSquare " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                numberOfSquaresAltered = numberOfSquaresAltered + 1;
                                            }


                                        }

                                    }

                                    for (Square square : rightColumn) {

                                        if (!square.equals(possibleXWingCornerTopRight) && !square.equals(possibleXWingCornerBottomRight)) {

                                            // eliminate option from square

                                            if (square.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                                System.out.println("----- #Rule9 ----- X-Wing Found! Square " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());
                                                setIterationTextViewText("----- #Rule9 ----- X-Wing Found!\nSquare " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                numberOfSquaresAltered = numberOfSquaresAltered + 1;
                                            }


                                        }

                                    }

                                    if (numberOfSquaresAltered > 0) {
                                        return true;
                                    }

                                }

                            }


                        }


                    }


                }

            }


            for (ArrayList<Square> squaresByColumn : allSquaresByColumn) {

                int gridColumnNumber = squaresByColumn.get(0).getGridColumnNumber();

                int numberOfInstancesOfTheOptionLeftHandCorners = 0;

                Square possibleXWingCornerTopLeft = null;
                Square possibleXWingCornerBottomLeft = null;

                for (Square square : squaresByColumn) {

                    if (!square.isHasBeenSolved() && square.checkIfOptionIsStillMarkedAsAPossibility(option)) {

                        if (numberOfInstancesOfTheOptionLeftHandCorners == 0) {
                            possibleXWingCornerTopLeft = square;
                        } else if (numberOfInstancesOfTheOptionLeftHandCorners == 1) {
                            possibleXWingCornerBottomLeft = square;
                        }

                        numberOfInstancesOfTheOptionLeftHandCorners = numberOfInstancesOfTheOptionLeftHandCorners + 1;

                    }

                }

                // check if only two instances

                if (numberOfInstancesOfTheOptionLeftHandCorners == 2) {


                    // go through the rest of the rows looking for a row with two candidates only of the same option
                    // if there is check their columns match for a valid x-wing

                    for (ArrayList<Square> columnOfSquares : allSquaresByColumn) {

                        if (columnOfSquares.get(0).getGridColumnNumber() > gridColumnNumber) {

                            Square possibleXWingCornerTopRight = null;
                            Square possibleXWingCornerBottomRight = null;

                            // for efficiency as working across the grid

                            int numberOfInstancesOfTheOptionRightHandCorners = 0;

                            for (Square square : columnOfSquares) {

                                if (!square.isHasBeenSolved() && square.checkIfOptionIsStillMarkedAsAPossibility(option)) {

                                    if (numberOfInstancesOfTheOptionRightHandCorners == 0) {
                                        possibleXWingCornerTopRight = square;
                                    } else if (numberOfInstancesOfTheOptionRightHandCorners == 1) {
                                        possibleXWingCornerBottomRight = square;
                                    }

                                    numberOfInstancesOfTheOptionRightHandCorners = numberOfInstancesOfTheOptionRightHandCorners + 1;


                                }

                            }


                            if (numberOfInstancesOfTheOptionRightHandCorners == 2) {

                                // all that is left is to check the corners are in the correct rows

                                if (possibleXWingCornerTopLeft.getGridRowNumber() == possibleXWingCornerTopRight.getGridRowNumber() &&
                                        possibleXWingCornerBottomLeft.getGridRowNumber() == possibleXWingCornerBottomRight.getGridRowNumber()) {

//                                    System.out.println("Success? (c) option = " + Integer.toHexString(option) + " Square tl = " + possibleXWingCornerTopLeft.getSquareNumberAsString() +
//                                            " Square tr = " + possibleXWingCornerTopRight.getSquareNumberAsString() + " Square bl = " + possibleXWingCornerBottomLeft.getSquareNumberAsString() +
//                                            " Square br = " + possibleXWingCornerBottomRight.getSquareNumberAsString());

                                    // as rows anything in the column that is not any of the corners can have the number eliminated

                                    int numberOfSquaresAltered = 0;

                                    ArrayList<Square> topRow = my_grid.getAllSquaresInAGridRow(possibleXWingCornerTopLeft.getGridRowNumber());
                                    ArrayList<Square> bottomRow = my_grid.getAllSquaresInAGridRow(possibleXWingCornerBottomLeft.getGridRowNumber());


                                    for (Square square : topRow) {

                                        if (!square.equals(possibleXWingCornerTopLeft) && !square.equals(possibleXWingCornerTopRight)) {

                                            // eliminate option from square

                                            if (square.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                                System.out.println("----- #Rule9 -----  X-Wing Found! Square " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());
                                                setIterationTextViewText("----- #Rule9 ----- X-Wing Found!\nSquare " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                numberOfSquaresAltered = numberOfSquaresAltered + 1;
                                            }


                                        }

                                    }

                                    for (Square square : bottomRow) {

                                        if (!square.equals(possibleXWingCornerBottomLeft) && !square.equals(possibleXWingCornerBottomRight)) {

                                            // eliminate option from square

                                            if (square.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                                System.out.println("----- #Rule9 ----  X-Wing Found! Square " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());
                                                setIterationTextViewText("----- #Rule9 ----- X-Wing Found!\nSquare " + square.getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                numberOfSquaresAltered = numberOfSquaresAltered + 1;
                                            }


                                        }

                                    }

                                    if (numberOfSquaresAltered > 0) {
                                        return true;
                                    }

                                }

                            }


                        }


                    }


                }

            }


        }

        if (singleAction){

            setIterationTextViewText("----- #Rule9 ----  X-Wing\nNo solution found");
        }

        return false;
    }


    // ---------------- rule 10 -----------------
    // Hidden Pairs

    // If a pair options only appear within the same two cells then all other candidates in them two cells can
    // be eliminated


    public boolean checkGridForHiddenPairs_Rule10_ReturnTrueOnEvent(boolean singleAction) {

        ArrayList<ArrayList<Square>> collection = new ArrayList<>();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};


        for (int index : indexArray) {

            // check each cell, column, and row
            collection.add(Grid.getInstance().getAllSquaresInACell(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridColumn(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridRow(index));

        }

        // iterate through the collection

        for (ArrayList<Square> gridSquaresList : collection) {


            int numberOfZeros = 0;
            int numberOfOnes = 0;
            int numberOfTwos = 0;
            int numberOfThrees = 0;
            int numberOfFours = 0;
            int numberOfFives = 0;
            int numberOfSixes = 0;
            int numberOfSevens = 0;
            int numberOfEights = 0;
            int numberOfNines = 0;
            int numberOfTens = 0;
            int numberOfElevens = 0;
            int numberOfTwelves = 0;
            int numberOfThirteens = 0;
            int numberOfFourteens = 0;
            int numberOfFifteens = 0;


            // get the tallies
            for (Square gridSquare : gridSquaresList) {


                ArrayList<Integer> remainingOptions = gridSquare.getOptionsStillPossible();

                for (int number : remainingOptions) {


                    switch (number) {
                        case 0:
                            numberOfZeros = numberOfZeros + 1;
                            break;
                        case 1:
                            numberOfOnes = numberOfOnes + 1;
                            break;
                        case 2:
                            numberOfTwos = numberOfTwos + 1;
                            break;
                        case 3:
                            numberOfThrees = numberOfThrees + 1;
                            break;
                        case 4:
                            numberOfFours = numberOfFours + 1;
                            break;
                        case 5:
                            numberOfFives = numberOfFives + 1;
                            break;
                        case 6:
                            numberOfSixes = numberOfSixes + 1;
                            break;
                        case 7:
                            numberOfSevens = numberOfSevens + 1;
                            break;
                        case 8:
                            numberOfEights = numberOfEights + 1;
                            break;
                        case 9:
                            numberOfNines = numberOfNines + 1;
                            break;
                        case 10:
                            numberOfTens = numberOfTens + 1;
                            break;
                        case 11:
                            numberOfElevens = numberOfElevens + 1;
                            break;
                        case 12:
                            numberOfTwelves = numberOfTwelves + 1;
                            break;
                        case 13:
                            numberOfThirteens = numberOfThirteens + 1;
                            break;
                        case 14:
                            numberOfFourteens = numberOfFourteens + 1;
                            break;
                        case 15:
                            numberOfFifteens = numberOfFifteens + 1;
                            break;
                        default:
                            break;
                    }

                }

            }

            //  collect the options which have a 2 tally
            ArrayList<Integer> solutionsWithTwoInstances = new ArrayList<>();

            if (numberOfZeros == 2) {
                solutionsWithTwoInstances.add(0);
            }
            if (numberOfOnes == 2) {
                solutionsWithTwoInstances.add(1);
            }
            if (numberOfTwos == 2) {
                solutionsWithTwoInstances.add(2);
            }
            if (numberOfThrees == 2) {
                solutionsWithTwoInstances.add(3);
            }
            if (numberOfFours == 2) {
                solutionsWithTwoInstances.add(4);
            }
            if (numberOfFives == 2) {
                solutionsWithTwoInstances.add(5);
            }
            if (numberOfSixes == 2) {
                solutionsWithTwoInstances.add(6);
            }
            if (numberOfSevens == 2) {
                solutionsWithTwoInstances.add(7);
            }
            if (numberOfEights == 2) {
                solutionsWithTwoInstances.add(8);
            }
            if (numberOfNines == 2) {
                solutionsWithTwoInstances.add(9);
            }
            if (numberOfTens == 2) {
                solutionsWithTwoInstances.add(10);
            }
            if (numberOfElevens == 2) {
                solutionsWithTwoInstances.add(11);
            }
            if (numberOfTwelves == 2) {
                solutionsWithTwoInstances.add(12);
            }
            if (numberOfThirteens == 2) {
                solutionsWithTwoInstances.add(13);
            }
            if (numberOfFourteens == 2) {
                solutionsWithTwoInstances.add(14);
            }
            if (numberOfFifteens == 2) {
                solutionsWithTwoInstances.add(15);
            }


            int n = solutionsWithTwoInstances.size();

            for (int i = 0; i < n; i = i + 1) {

                for (int j = i + 1; j < n; j = j + 1) {

                    ArrayList<Square> squaresWithPairs = new ArrayList<>();


                    int option1 = solutionsWithTwoInstances.get(i);
                    int option2 = solutionsWithTwoInstances.get(j);

                    for (Square square : gridSquaresList) {


                        if (square.checkIfOptionIsStillMarkedAsAPossibility(option1) && square.checkIfOptionIsStillMarkedAsAPossibility(option2)) {

                            squaresWithPairs.add(square);

                        }

                    }

                    if (squaresWithPairs.size() == 2) {

                        int numberOfSquaresAltered = 0;

                        // two squares containing both pairs

//                    System.out.println("hidden pair? option 1 = " + Integer.toHexString(option1) + " option 2 = " + Integer.toHexString(option2) + " Square 1 = " + squareWithPair_1.getSquareNumberAsString() +
//                    " square 2 = " + squareWithPair_2.getSquareNumberAsString());

                        // eliminate all but the two options from squareWithPair1

                        ArrayList<Integer> optionsForSquare1 = squaresWithPairs.get(0).getOptionsStillPossible();

                        for (int option : optionsForSquare1) {

                            if (option != option1 && option != option2) {

                                // safe to delete this option

                                if (squaresWithPairs.get(0).eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                    System.out.println("----- #Rule10 ----  Hidden Pair Found! Square " + squaresWithPairs.get(0).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());
                                    setIterationTextViewText("----- #Rule10 ----- Hidden Pair Found!\nSquare " + squaresWithPairs.get(0).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                    numberOfSquaresAltered = numberOfSquaresAltered + 1;

                                }


                            }

                        }

                        // eliminate all but the two options from squareWithPair1

                        ArrayList<Integer> optionsForSquare2 = squaresWithPairs.get(1).getOptionsStillPossible();

                        for (int option : optionsForSquare2) {

                            if (option != option1 && option != option2) {

                                // safe to delete this option

                                if (squaresWithPairs.get(1).eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                    System.out.println("----- #Rule10 ----  Hidden Pair Found! Square " + squaresWithPairs.get(1).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());
                                    setIterationTextViewText("----- #Rule10 ----- Hidden Pair Found!\nSquare " + squaresWithPairs.get(1).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                    numberOfSquaresAltered = numberOfSquaresAltered + 1;

                                }


                            }

                        }


                        if (numberOfSquaresAltered > 0) {
                            return true;
                        }

                    }

                }
            }


        }

        if (singleAction){

            setIterationTextViewText("----- #Rule10 ----- Hidden Pair!\nNo solution found");
        }

        return false;
    }


// ---------------- rule 11 -----------------
    // Hidden Triples

    // Same theory as for a hidden pair but obviously for a triple


    public boolean checkGridForHiddenTriples_Rule11_ReturnTrueOnEvent(boolean singleAction) {

        ArrayList<ArrayList<Square>> collection = new ArrayList<>();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};


        for (int index : indexArray) {

            // check each cell, column, and row
            collection.add(Grid.getInstance().getAllSquaresInACell(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridColumn(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridRow(index));

        }

        // iterate through the collection

        for (ArrayList<Square> gridSquaresList : collection) {


            int numberOfZeros = 0;
            int numberOfOnes = 0;
            int numberOfTwos = 0;
            int numberOfThrees = 0;
            int numberOfFours = 0;
            int numberOfFives = 0;
            int numberOfSixes = 0;
            int numberOfSevens = 0;
            int numberOfEights = 0;
            int numberOfNines = 0;
            int numberOfTens = 0;
            int numberOfElevens = 0;
            int numberOfTwelves = 0;
            int numberOfThirteens = 0;
            int numberOfFourteens = 0;
            int numberOfFifteens = 0;


            // get the tallies
            for (Square gridSquare : gridSquaresList) {


                ArrayList<Integer> remainingOptions = gridSquare.getOptionsStillPossible();

                for (int number : remainingOptions) {


                    switch (number) {
                        case 0:
                            numberOfZeros = numberOfZeros + 1;
                            break;
                        case 1:
                            numberOfOnes = numberOfOnes + 1;
                            break;
                        case 2:
                            numberOfTwos = numberOfTwos + 1;
                            break;
                        case 3:
                            numberOfThrees = numberOfThrees + 1;
                            break;
                        case 4:
                            numberOfFours = numberOfFours + 1;
                            break;
                        case 5:
                            numberOfFives = numberOfFives + 1;
                            break;
                        case 6:
                            numberOfSixes = numberOfSixes + 1;
                            break;
                        case 7:
                            numberOfSevens = numberOfSevens + 1;
                            break;
                        case 8:
                            numberOfEights = numberOfEights + 1;
                            break;
                        case 9:
                            numberOfNines = numberOfNines + 1;
                            break;
                        case 10:
                            numberOfTens = numberOfTens + 1;
                            break;
                        case 11:
                            numberOfElevens = numberOfElevens + 1;
                            break;
                        case 12:
                            numberOfTwelves = numberOfTwelves + 1;
                            break;
                        case 13:
                            numberOfThirteens = numberOfThirteens + 1;
                            break;
                        case 14:
                            numberOfFourteens = numberOfFourteens + 1;
                            break;
                        case 15:
                            numberOfFifteens = numberOfFifteens + 1;
                            break;
                        default:
                            break;
                    }

                }

            }

            //  collect the options which have a 2 tally
            ArrayList<Integer> solutionsWithTwoOrThreeInstances = new ArrayList<>();

            if (numberOfZeros == 2 || numberOfZeros == 3) {
                solutionsWithTwoOrThreeInstances.add(0);
            }
            if (numberOfOnes == 2 || numberOfOnes == 3) {
                solutionsWithTwoOrThreeInstances.add(1);
            }
            if (numberOfTwos == 2 || numberOfTwos == 3) {
                solutionsWithTwoOrThreeInstances.add(2);
            }
            if (numberOfThrees == 2 || numberOfThrees == 3) {
                solutionsWithTwoOrThreeInstances.add(3);
            }
            if (numberOfFours == 2 || numberOfFours == 3) {
                solutionsWithTwoOrThreeInstances.add(4);
            }
            if (numberOfFives == 2 || numberOfFives == 3) {
                solutionsWithTwoOrThreeInstances.add(5);
            }
            if (numberOfSixes == 2 || numberOfSixes == 3) {
                solutionsWithTwoOrThreeInstances.add(6);
            }
            if (numberOfSevens == 2 || numberOfSevens == 3) {
                solutionsWithTwoOrThreeInstances.add(7);
            }
            if (numberOfEights == 2 || numberOfEights == 3) {
                solutionsWithTwoOrThreeInstances.add(8);
            }
            if (numberOfNines == 2 || numberOfNines == 3) {
                solutionsWithTwoOrThreeInstances.add(9);
            }
            if (numberOfTens == 2 || numberOfTens == 3) {
                solutionsWithTwoOrThreeInstances.add(10);
            }
            if (numberOfElevens == 2 || numberOfElevens == 3) {
                solutionsWithTwoOrThreeInstances.add(11);
            }
            if (numberOfTwelves == 2 || numberOfTwelves == 3) {
                solutionsWithTwoOrThreeInstances.add(12);
            }
            if (numberOfThirteens == 2 || numberOfThirteens == 3) {
                solutionsWithTwoOrThreeInstances.add(13);
            }
            if (numberOfFourteens == 2 || numberOfFourteens == 3) {
                solutionsWithTwoOrThreeInstances.add(14);
            }
            if (numberOfFifteens == 2 || numberOfFifteens == 3) {
                solutionsWithTwoOrThreeInstances.add(15);
            }


            int n = solutionsWithTwoOrThreeInstances.size();

            for (int i = 0; i < n; i = i + 1) {

                for (int j = i + 1; j < n; j = j + 1) {

                    for (int k = j + 1; k < n; k = k + 1) {


                        ArrayList<Square> squaresWithTwoOrThreeInstancesOfTheOption = new ArrayList<>();


                        int option1 = solutionsWithTwoOrThreeInstances.get(i);
                        int option2 = solutionsWithTwoOrThreeInstances.get(j);
                        int option3 = solutionsWithTwoOrThreeInstances.get(k);

                        for (Square square : gridSquaresList) {

                            int numberOfMatches = 0;

                            if (square.checkIfOptionIsStillMarkedAsAPossibility(option1)) {
                                numberOfMatches = numberOfMatches + 1;
                            }

                            if (square.checkIfOptionIsStillMarkedAsAPossibility(option2)) {
                                numberOfMatches = numberOfMatches + 1;
                            }

                            if (square.checkIfOptionIsStillMarkedAsAPossibility(option3)) {
                                numberOfMatches = numberOfMatches + 1;
                            }


                            if (numberOfMatches == 2 || numberOfMatches == 3) {

                                squaresWithTwoOrThreeInstancesOfTheOption.add(square);

                            }

                        }


                        if (squaresWithTwoOrThreeInstancesOfTheOption.size() == 3) {


                            boolean proceed = true;

                            int numberOfSquaresAltered = 0;


                            ArrayList<Integer> optionsList = new ArrayList<>();
                            optionsList.add(option1);
                            optionsList.add(option2);
                            optionsList.add(option3);


                            for (int option : optionsList) {

                                int numberOfMatches = 0;

                                for (Square square : squaresWithTwoOrThreeInstancesOfTheOption) {

                                    if (square.getOptionsStillPossible().contains(option)) {
                                        numberOfMatches = numberOfMatches + 1;
                                    }
                                }

                                if (option == 0 && numberOfMatches != numberOfZeros) {
                                    proceed = false;
                                    break;
                                } else if (option == 1 && numberOfMatches != numberOfOnes) {
                                    proceed = false;
                                    break;
                                } else if (option == 2 && numberOfMatches != numberOfTwos) {
                                    proceed = false;
                                    break;
                                } else if (option == 3 && numberOfMatches != numberOfThrees) {
                                    proceed = false;
                                    break;
                                } else if (option == 4 && numberOfMatches != numberOfFours) {
                                    proceed = false;
                                    break;
                                } else if (option == 5 && numberOfMatches != numberOfFives) {
                                    proceed = false;
                                    break;
                                } else if (option == 6 && numberOfMatches != numberOfSixes) {
                                    proceed = false;
                                    break;
                                } else if (option == 7 && numberOfMatches != numberOfSevens) {
                                    proceed = false;
                                    break;
                                } else if (option == 8 && numberOfMatches != numberOfEights) {
                                    proceed = false;
                                    break;
                                } else if (option == 9 && numberOfMatches != numberOfNines) {
                                    proceed = false;
                                    break;
                                } else if (option == 10 && numberOfMatches != numberOfTens) {
                                    proceed = false;
                                    break;
                                } else if (option == 11 && numberOfMatches != numberOfElevens) {
                                    proceed = false;
                                    break;
                                } else if (option == 12 && numberOfMatches != numberOfTwelves) {
                                    proceed = false;
                                    break;
                                } else if (option == 13 && numberOfMatches != numberOfThirteens) {
                                    proceed = false;
                                    break;
                                } else if (option == 14 && numberOfMatches != numberOfFourteens) {
                                    proceed = false;
                                    break;
                                } else if (option == 15 && numberOfMatches != numberOfFifteens) {
                                    proceed = false;
                                    break;
                                }


                            }


                            if (proceed == true) {

                                // should be safe to remove all the other options


                                // eliminate all but the two options from squareWithPair1

                                ArrayList<Integer> optionsForSquare1 = squaresWithTwoOrThreeInstancesOfTheOption.get(0).getOptionsStillPossible();

                                for (int option : optionsForSquare1) {

                                    if (option != option1 && option != option2 && option != option3) {

                                        // safe to delete this option

                                        if (squaresWithTwoOrThreeInstancesOfTheOption.get(0).eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                            System.out.println("----- #Rule11 ----  Hidden Triple Found! Square " + squaresWithTwoOrThreeInstancesOfTheOption.get(0).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());
                                            setIterationTextViewText("----- #Rule11 ----- Hidden Triple Found!\nSquare " + squaresWithTwoOrThreeInstancesOfTheOption.get(0).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                            numberOfSquaresAltered = numberOfSquaresAltered + 1;

                                        }


                                    }

                                }

                                // eliminate all but the two options from squareWithPair1

                                ArrayList<Integer> optionsForSquare2 = squaresWithTwoOrThreeInstancesOfTheOption.get(1).getOptionsStillPossible();

                                for (int option : optionsForSquare2) {

                                    if (option != option1 && option != option2 && option != option3) {


//                                        System.out.println("hidden triple? option 1 = " + Integer.toHexString(option1) + " option 2 = " + Integer.toHexString(option2) + " option 3 = " + Integer.toHexString(option3) + " Square 1 = " + squaresWithTwoOrThreeInstancesOfTheOption.get(0).getSquareNumberAsString() +
//                                                " square 2 = " + squaresWithTwoOrThreeInstancesOfTheOption.get(1).getSquareNumberAsString() + " square 3 = " + squaresWithTwoOrThreeInstancesOfTheOption.get(2).getSquareNumberAsString());

                                        // safe to delete this option

                                        if (squaresWithTwoOrThreeInstancesOfTheOption.get(1).eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                            System.out.println("----- #Rule11 ----  Hidden Triple Found! Square " + squaresWithTwoOrThreeInstancesOfTheOption.get(1).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                            setIterationTextViewText("----- #Rule11 ----- Hidden Triple Found!\nSquare " + squaresWithTwoOrThreeInstancesOfTheOption.get(1).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                            numberOfSquaresAltered = numberOfSquaresAltered + 1;

                                        }


                                    }

                                }


                                ArrayList<Integer> optionsForSquare3 = squaresWithTwoOrThreeInstancesOfTheOption.get(2).getOptionsStillPossible();

                                for (int option : optionsForSquare3) {

                                    if (option != option1 && option != option2 && option != option3) {

                                        // safe to delete this option

                                        if (squaresWithTwoOrThreeInstancesOfTheOption.get(2).eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                            System.out.println("----- #Rule11 ----  Hidden Triple Found! Square " + squaresWithTwoOrThreeInstancesOfTheOption.get(2).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                            setIterationTextViewText("----- #Rule11 ----- Hidden Triple Found!\nSquare " + squaresWithTwoOrThreeInstancesOfTheOption.get(2).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                            numberOfSquaresAltered = numberOfSquaresAltered + 1;

                                        }


                                    }

                                }


                                if (numberOfSquaresAltered > 0) {
                                    return true;
                                }

                            }
                        }
                    }

                }
            }


        }

        if (singleAction){

            setIterationTextViewText("----- #Rule11 ----- Hidden Triple!\nNo solution found");
        }

        return false;
    }


    // ---------------- rule 12 -----------------
    // Hidden Quads

    // Same theory as for a hidden pair, triples but obviously for a quad

    // apparently these are found very, very seldom, for this reason a hidden quint hasn't been attempted


    public boolean checkGridForHiddenQuad_Rule12_ReturnTrueOnEvent(boolean singleAction) {

        ArrayList<ArrayList<Square>> collection = new ArrayList<>();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};


        for (int index : indexArray) {

            // check each cell, column, and row
            collection.add(Grid.getInstance().getAllSquaresInACell(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridColumn(index));

            collection.add(Grid.getInstance().getAllSquaresInAGridRow(index));

        }

        // iterate through the collection

        for (ArrayList<Square> gridSquaresList : collection) {


            int numberOfZeros = 0;
            int numberOfOnes = 0;
            int numberOfTwos = 0;
            int numberOfThrees = 0;
            int numberOfFours = 0;
            int numberOfFives = 0;
            int numberOfSixes = 0;
            int numberOfSevens = 0;
            int numberOfEights = 0;
            int numberOfNines = 0;
            int numberOfTens = 0;
            int numberOfElevens = 0;
            int numberOfTwelves = 0;
            int numberOfThirteens = 0;
            int numberOfFourteens = 0;
            int numberOfFifteens = 0;


            // get the tallies
            for (Square gridSquare : gridSquaresList) {


                ArrayList<Integer> remainingOptions = gridSquare.getOptionsStillPossible();

                for (int number : remainingOptions) {


                    switch (number) {
                        case 0:
                            numberOfZeros = numberOfZeros + 1;
                            break;
                        case 1:
                            numberOfOnes = numberOfOnes + 1;
                            break;
                        case 2:
                            numberOfTwos = numberOfTwos + 1;
                            break;
                        case 3:
                            numberOfThrees = numberOfThrees + 1;
                            break;
                        case 4:
                            numberOfFours = numberOfFours + 1;
                            break;
                        case 5:
                            numberOfFives = numberOfFives + 1;
                            break;
                        case 6:
                            numberOfSixes = numberOfSixes + 1;
                            break;
                        case 7:
                            numberOfSevens = numberOfSevens + 1;
                            break;
                        case 8:
                            numberOfEights = numberOfEights + 1;
                            break;
                        case 9:
                            numberOfNines = numberOfNines + 1;
                            break;
                        case 10:
                            numberOfTens = numberOfTens + 1;
                            break;
                        case 11:
                            numberOfElevens = numberOfElevens + 1;
                            break;
                        case 12:
                            numberOfTwelves = numberOfTwelves + 1;
                            break;
                        case 13:
                            numberOfThirteens = numberOfThirteens + 1;
                            break;
                        case 14:
                            numberOfFourteens = numberOfFourteens + 1;
                            break;
                        case 15:
                            numberOfFifteens = numberOfFifteens + 1;
                            break;
                        default:
                            break;
                    }

                }

            }

            //  collect the options which have a 2 tally
            ArrayList<Integer> solutionsWithTwoOrThreeOrFourInstances = new ArrayList<>();

            if (numberOfZeros == 2 || numberOfZeros == 3 || numberOfZeros == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(0);
            }
            if (numberOfOnes == 2 || numberOfOnes == 3 || numberOfOnes == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(1);
            }
            if (numberOfTwos == 2 || numberOfTwos == 3 || numberOfTwos == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(2);
            }
            if (numberOfThrees == 2 || numberOfThrees == 3 || numberOfThrees == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(3);
            }
            if (numberOfFours == 2 || numberOfFours == 3 || numberOfFours == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(4);
            }
            if (numberOfFives == 2 || numberOfFives == 3 || numberOfFives == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(5);
            }
            if (numberOfSixes == 2 || numberOfSixes == 3 || numberOfSixes == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(6);
            }
            if (numberOfSevens == 2 || numberOfSevens == 3 || numberOfSevens == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(7);
            }
            if (numberOfEights == 2 || numberOfEights == 3 || numberOfEights == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(8);
            }
            if (numberOfNines == 2 || numberOfNines == 3 || numberOfNines == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(9);
            }
            if (numberOfTens == 2 || numberOfTens == 3 || numberOfTens == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(10);
            }
            if (numberOfElevens == 2 || numberOfElevens == 3 || numberOfElevens == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(11);
            }
            if (numberOfTwelves == 2 || numberOfTwelves == 3 || numberOfTwelves == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(12);
            }
            if (numberOfThirteens == 2 || numberOfThirteens == 3 || numberOfThirteens == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(13);
            }
            if (numberOfFourteens == 2 || numberOfFourteens == 3 || numberOfFourteens == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(14);
            }
            if (numberOfFifteens == 2 || numberOfFifteens == 3 || numberOfFifteens == 4) {
                solutionsWithTwoOrThreeOrFourInstances.add(15);
            }


            int n = solutionsWithTwoOrThreeOrFourInstances.size();

            for (int i = 0; i < n; i = i + 1) {

                for (int j = i + 1; j < n; j = j + 1) {

                    for (int k = j + 1; k < n; k = k + 1) {

                        for (int m = k + 1; m < n; m = m + 1) {

                            ArrayList<Square> squaresWithTwoOrThreeOrFourInstancesOfTheOption = new ArrayList<>();


                            int option1 = solutionsWithTwoOrThreeOrFourInstances.get(i);
                            int option2 = solutionsWithTwoOrThreeOrFourInstances.get(j);
                            int option3 = solutionsWithTwoOrThreeOrFourInstances.get(k);
                            int option4 = solutionsWithTwoOrThreeOrFourInstances.get(m);

                            for (Square square : gridSquaresList) {

                                int numberOfMatches = 0;

                                if (square.checkIfOptionIsStillMarkedAsAPossibility(option1)) {
                                    numberOfMatches = numberOfMatches + 1;
                                }

                                if (square.checkIfOptionIsStillMarkedAsAPossibility(option2)) {
                                    numberOfMatches = numberOfMatches + 1;
                                }

                                if (square.checkIfOptionIsStillMarkedAsAPossibility(option3)) {
                                    numberOfMatches = numberOfMatches + 1;
                                }

                                if (square.checkIfOptionIsStillMarkedAsAPossibility(option4)) {
                                    numberOfMatches = numberOfMatches + 1;
                                }

                                if (numberOfMatches == 2 || numberOfMatches == 3 || numberOfMatches == 4) {

                                    squaresWithTwoOrThreeOrFourInstancesOfTheOption.add(square);

                                }

                            }


                            if (squaresWithTwoOrThreeOrFourInstancesOfTheOption.size() == 4) {


                                boolean proceed = true;

                                int numberOfSquaresAltered = 0;


                                ArrayList<Integer> optionsList = new ArrayList<>();
                                optionsList.add(option1);
                                optionsList.add(option2);
                                optionsList.add(option3);
                                optionsList.add(option4);

                                for (int option : optionsList) {

                                    int numberOfMatches = 0;

                                    for (Square square : squaresWithTwoOrThreeOrFourInstancesOfTheOption) {

                                        if (square.getOptionsStillPossible().contains(option)) {
                                            numberOfMatches = numberOfMatches + 1;
                                        }
                                    }

                                    if (option == 0 && numberOfMatches != numberOfZeros) {
                                        proceed = false;
                                        break;
                                    } else if (option == 1 && numberOfMatches != numberOfOnes) {
                                        proceed = false;
                                        break;
                                    } else if (option == 2 && numberOfMatches != numberOfTwos) {
                                        proceed = false;
                                        break;
                                    } else if (option == 3 && numberOfMatches != numberOfThrees) {
                                        proceed = false;
                                        break;
                                    } else if (option == 4 && numberOfMatches != numberOfFours) {
                                        proceed = false;
                                        break;
                                    } else if (option == 5 && numberOfMatches != numberOfFives) {
                                        proceed = false;
                                        break;
                                    } else if (option == 6 && numberOfMatches != numberOfSixes) {
                                        proceed = false;
                                        break;
                                    } else if (option == 7 && numberOfMatches != numberOfSevens) {
                                        proceed = false;
                                        break;
                                    } else if (option == 8 && numberOfMatches != numberOfEights) {
                                        proceed = false;
                                        break;
                                    } else if (option == 9 && numberOfMatches != numberOfNines) {
                                        proceed = false;
                                        break;
                                    } else if (option == 10 && numberOfMatches != numberOfTens) {
                                        proceed = false;
                                        break;
                                    } else if (option == 11 && numberOfMatches != numberOfElevens) {
                                        proceed = false;
                                        break;
                                    } else if (option == 12 && numberOfMatches != numberOfTwelves) {
                                        proceed = false;
                                        break;
                                    } else if (option == 13 && numberOfMatches != numberOfThirteens) {
                                        proceed = false;
                                        break;
                                    } else if (option == 14 && numberOfMatches != numberOfFourteens) {
                                        proceed = false;
                                        break;
                                    } else if (option == 15 && numberOfMatches != numberOfFifteens) {
                                        proceed = false;
                                        break;
                                    }


                                }


                                if (proceed == true) {

                                    // should be safe to remove all the other options


                                    // eliminate all but the two options from squareWithPair1

                                    ArrayList<Integer> optionsForSquare1 = squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(0).getOptionsStillPossible();

                                    for (int option : optionsForSquare1) {

                                        if (option != option1 && option != option2 && option != option3 && option != option4) {

                                            // safe to delete this option

                                            if (squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(0).eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                                System.out.println("----- #Rule12 ----  Hidden Quad Found! Square " + squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(0).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                setIterationTextViewText("----- #Rule12 ----- Hidden Quad Found!\nSquare " + squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(0).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                numberOfSquaresAltered = numberOfSquaresAltered + 1;

                                            }


                                        }

                                    }

                                    // eliminate all but the two options from squareWithPair1

                                    ArrayList<Integer> optionsForSquare2 = squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(1).getOptionsStillPossible();

                                    for (int option : optionsForSquare2) {

                                        if (option != option1 && option != option2 && option != option3 && option != option4) {


//                                        System.out.println("hidden triple? option 1 = " + Integer.toHexString(option1) + " option 2 = " + Integer.toHexString(option2) + " option 3 = " + Integer.toHexString(option3) + " Square 1 = " + squaresWithTwoOrThreeInstancesOfTheOption.get(0).getSquareNumberAsString() +
//                                                " square 2 = " + squaresWithTwoOrThreeInstancesOfTheOption.get(1).getSquareNumberAsString() + " square 3 = " + squaresWithTwoOrThreeInstancesOfTheOption.get(2).getSquareNumberAsString());

                                            // safe to delete this option

                                            if (squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(1).eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                                System.out.println("----- #Rule12 ----  Hidden Quad Found! Square " + squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(1).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                setIterationTextViewText("----- #Rule12 ----- Hidden Quad Found!\nSquare " + squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(1).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                numberOfSquaresAltered = numberOfSquaresAltered + 1;

                                            }


                                        }

                                    }


                                    ArrayList<Integer> optionsForSquare3 = squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(2).getOptionsStillPossible();

                                    for (int option : optionsForSquare3) {

                                        if (option != option1 && option != option2 && option != option3 && option != option4) {

                                            // safe to delete this option

                                            if (squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(2).eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                                System.out.println("----- #Rule12 ----  Hidden Quad Found! Square " + squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(2).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                setIterationTextViewText("----- #Rule12 ----- Hidden Quad Found!\nSquare " + squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(2).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                numberOfSquaresAltered = numberOfSquaresAltered + 1;

                                            }


                                        }

                                    }


                                    ArrayList<Integer> optionsForSquare4 = squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(3).getOptionsStillPossible();

                                    for (int option : optionsForSquare4) {

                                        if (option != option1 && option != option2 && option != option3 && option != option4) {

                                            // safe to delete this option

                                            if (squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(3).eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(option)) {

                                                System.out.println("----- #Rule12 ----  Hidden Quad Found! Square " + squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(3).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                setIterationTextViewText("----- #Rule12 ----- Hidden Quad Found!\nSquare " + squaresWithTwoOrThreeOrFourInstancesOfTheOption.get(3).getSquareNumberAsString() + " could not be a " + Integer.toHexString(option).toUpperCase());

                                                numberOfSquaresAltered = numberOfSquaresAltered + 1;

                                            }


                                        }

                                    }


                                    if (numberOfSquaresAltered > 0) {
                                        return true;
                                    }

                                }
                            }
                        }
                    }
                }
            }


        }

        if (singleAction){

            setIterationTextViewText("----- #Rule12 ----- Hidden Quad!\nNo solution found");
        }

        return false;
    }


// ----------------- rule 13 -------------------

    // If a right angle of 3 x 2 unique candidate set triples exist that share the 3 int same candidate set
    // then the missing corner can safely have the candidate that is not in the candiate set for the right angle
    // corner removed

    // Simple implimentation of a more complex XY wing

    public boolean checkGridForYWings_Rule13_ReturnTrueOnEvent(boolean singleAction) {

        Grid my_grid = Grid.getInstance();

        ArrayList<ArrayList<Square>> collection = new ArrayList<>();

        int[] indexArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

        for (int index : indexArray) {

            // check each column or row
            collection.add(my_grid.getAllSquaresInAGridColumn(index));
            collection.add(my_grid.getAllSquaresInAGridRow(index));
        }


        // iterate through the collection


        int count = -1;

        for (ArrayList<Square> setList : collection) {

            count = count + 1;

            boolean firstMatchWasInAColumnAndNotRow = false;

            if (count % 2 == 0) {
                firstMatchWasInAColumnAndNotRow = true;
            }


            // working with an array of cells in either a column or array from here


            List<Square> gridSquaresWithTwoOptions = new ArrayList<>();

            for (Square square : setList) {


                if (square.getNumberOfRemainingOptions() == 2) {
                    gridSquaresWithTwoOptions.add(square);
                }


            }


            if (gridSquaresWithTwoOptions.size() >= 2) {

                // search for matching pairs


                for (int i = 0; i < gridSquaresWithTwoOptions.size(); i = i + 1) {
                    for (int j = i + 1; j < gridSquaresWithTwoOptions.size(); j = j + 1) {

                        // check not same cell

                        if (gridSquaresWithTwoOptions.get(i).getCellNumber() != gridSquaresWithTwoOptions.get(j).getCellNumber()) {


                            int option1ForSquare1 = gridSquaresWithTwoOptions.get(i).getOptionsStillPossible().get(0);
                            int option2ForSquare1 = gridSquaresWithTwoOptions.get(i).getOptionsStillPossible().get(1);
                            int option1ForSquare2 = gridSquaresWithTwoOptions.get(j).getOptionsStillPossible().get(0);
                            int option2ForSquare2 = gridSquaresWithTwoOptions.get(j).getOptionsStillPossible().get(1);


                            // need a common option but not two as that would be a pair

                            int match = -1;
                            int numberOfMatches = 0;

                            if (option1ForSquare1 == option1ForSquare2) {
                                numberOfMatches = numberOfMatches + 1;
                                match = option1ForSquare1;
                            }
                            if (option1ForSquare1 == option2ForSquare2) {
                                numberOfMatches = numberOfMatches + 1;
                                match = option1ForSquare1;
                            }
                            if (option2ForSquare1 == option1ForSquare2) {
                                numberOfMatches = numberOfMatches + 1;
                                match = option2ForSquare1;
                            }
                            if (option2ForSquare1 == option2ForSquare2) {
                                numberOfMatches = numberOfMatches + 1;
                                match = option2ForSquare1;
                            }


                            if (numberOfMatches == 1) {


                                // we have two pairs with a single common option
                                // next step check intersecting rows and columns of the squares in other cells for a square with 2 options , each one a single
                                // common option with each square but not the same for each, ie the set should be a length of 3 only

                                Square square_i = gridSquaresWithTwoOptions.get(i);
                                Square square_j = gridSquaresWithTwoOptions.get(j);

                                int gridCellNumber_i = square_i.getGridColumnNumber();
                                int gridColumnNumber_i = square_i.getGridColumnNumber();
                                int gridRowNumber_i = square_i.getGridRowNumber();
                                int noneCommonOption_i = -1;
                                for (int option : square_i.getOptionsStillPossible()) {
                                    if (option != match) {
                                        noneCommonOption_i = option;
                                    }
                                }

                                int gridCellNumber_j = square_j.getGridColumnNumber();
                                int gridColumnNumber_j = square_j.getGridColumnNumber();
                                int gridRowNumber_j = square_j.getGridRowNumber();
                                int noneCommonOption_j = -1;
                                for (int option : square_j.getOptionsStillPossible()) {
                                    if (option != match) {
                                        noneCommonOption_j = option;
                                    }
                                }


                                ArrayList<ArrayList<Square>> rowsAndColumnQueue = new ArrayList<>();


                                if (firstMatchWasInAColumnAndNotRow) {

                                    rowsAndColumnQueue.add(my_grid.getAllSquaresInAGridRow(gridRowNumber_i));
                                    rowsAndColumnQueue.add(my_grid.getAllSquaresInAGridRow(gridRowNumber_j));

                                } else {

                                    rowsAndColumnQueue.add(my_grid.getAllSquaresInAGridColumn(gridColumnNumber_i));
                                    rowsAndColumnQueue.add(my_grid.getAllSquaresInAGridColumn(gridColumnNumber_j));
                                }


                                boolean square_iRowNotSquare_jRow = true;

                                for (ArrayList<Square> rowOrColumn : rowsAndColumnQueue) {


                                    for (Square square_k : rowOrColumn) {

                                        if (square_iRowNotSquare_jRow && firstMatchWasInAColumnAndNotRow) {

                                            if (square_k.getCellNumber() != gridCellNumber_i && square_k.getNumberOfRemainingOptions() == 2 && square_k.getOptionsStillPossible().contains(noneCommonOption_i)
                                                    && square_k.getOptionsStillPossible().contains(noneCommonOption_j)) {


                                                int gridColumnOfSquare_k = square_k.getGridColumnNumber();

                                                int squareNumberOfOppositeCorner = (16 * gridRowNumber_j) + gridColumnOfSquare_k;

                                                Square oppositeSquare = my_grid.getSingleSquare(squareNumberOfOppositeCorner);

                                                // can't be the same as the noneMatch of j

                                                if (oppositeSquare.checkIfOptionIsStillMarkedAsAPossibility(noneCommonOption_j) && !oppositeSquare.isHasBeenSolved()) {

                                                    if (oppositeSquare.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(noneCommonOption_j)) {

                                                        System.out.println("----- #Rule13 ----  Y Wing Found! Square " + oppositeSquare.getSquareNumberAsString() + " could not be a " + Integer.toHexString(noneCommonOption_j).toUpperCase());

                                                        setIterationTextViewText("----- #Rule13 ----- Y Wing Found!\nSquare " + oppositeSquare.getSquareNumberAsString() + " could not be a " + Integer.toHexString(noneCommonOption_j).toUpperCase());
                                                        return true;
                                                    }

                                                }


                                            }

                                        } else if (square_iRowNotSquare_jRow == false && firstMatchWasInAColumnAndNotRow) {

                                            if (square_k.getCellNumber() != gridCellNumber_j && square_k.getNumberOfRemainingOptions() == 2 && square_k.getOptionsStillPossible().contains(noneCommonOption_i)
                                                    && square_k.getOptionsStillPossible().contains(noneCommonOption_j)) {


                                                int gridColumnOfSquare_k = square_k.getGridColumnNumber();

                                                int squareNumberOfOppositeCorner = (16 * gridRowNumber_i) + gridColumnOfSquare_k;

                                                Square oppositeSquare = my_grid.getSingleSquare(squareNumberOfOppositeCorner);

                                                // can't be the same as the noneMatch of j

                                                if (oppositeSquare.checkIfOptionIsStillMarkedAsAPossibility(noneCommonOption_i) && !oppositeSquare.isHasBeenSolved()) {

                                                    if (oppositeSquare.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(noneCommonOption_i)) {

                                                        System.out.println("----- #Rule13 ----  Y Wing Found! Square " + oppositeSquare.getSquareNumberAsString() + " could not be a " + Integer.toHexString(noneCommonOption_i).toUpperCase());
                                                        setIterationTextViewText("----- #Rule13 ----- Y Wing Found!\nSquare " + oppositeSquare.getSquareNumberAsString() + " could not be a " + Integer.toHexString(noneCommonOption_i).toUpperCase());

                                                        return true;
                                                    }

                                                }

                                            }

                                        } else if (square_iRowNotSquare_jRow && firstMatchWasInAColumnAndNotRow == false) {

                                            if (square_k.getCellNumber() != gridCellNumber_i && square_k.getNumberOfRemainingOptions() == 2 && square_k.getOptionsStillPossible().contains(noneCommonOption_i)
                                                    && square_k.getOptionsStillPossible().contains(noneCommonOption_j)) {


                                                int gridRowOfSquare_k = square_k.getGridRowNumber();

                                                int squareNumberOfOppositeCorner = (16 * gridRowOfSquare_k) + gridColumnNumber_j;

                                                Square oppositeSquare = my_grid.getSingleSquare(squareNumberOfOppositeCorner);

                                                // can't be the same as the noneMatch of j

                                                if (oppositeSquare.checkIfOptionIsStillMarkedAsAPossibility(noneCommonOption_j) && !oppositeSquare.isHasBeenSolved()) {

                                                    if (oppositeSquare.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(noneCommonOption_j)) {

                                                        System.out.println("----- #Rule13 ----  Y Wing Found! Square " + oppositeSquare.getSquareNumberAsString() + " could not be a " + Integer.toHexString(noneCommonOption_j).toUpperCase());

                                                        setIterationTextViewText("----- #Rule13 ----- Y Wing Found!\nSquare " + oppositeSquare.getSquareNumberAsString() + " could not be a " + Integer.toHexString(noneCommonOption_j).toUpperCase());

                                                        return true;
                                                    }

                                                }

                                            }

                                        } else if (square_iRowNotSquare_jRow == false && firstMatchWasInAColumnAndNotRow == false) {

                                            if (square_k.getCellNumber() != gridCellNumber_j && square_k.getNumberOfRemainingOptions() == 2 && square_k.getOptionsStillPossible().contains(noneCommonOption_i)
                                                    && square_k.getOptionsStillPossible().contains(noneCommonOption_j)) {


                                                int gridRowOfSquare_k = square_k.getGridRowNumber();

                                                int squareNumberOfOppositeCorner = (16 * gridRowOfSquare_k) + gridColumnNumber_i;

                                                Square oppositeSquare = my_grid.getSingleSquare(squareNumberOfOppositeCorner);

                                                // can't be the same as the noneMatch of j

                                                if (oppositeSquare.checkIfOptionIsStillMarkedAsAPossibility(noneCommonOption_i) && !oppositeSquare.isHasBeenSolved()) {

                                                    if (oppositeSquare.eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(noneCommonOption_i)) {

                                                        System.out.println("----- #Rule13 ----  Y Wing Found! Square " + oppositeSquare.getSquareNumberAsString() + " could not be a " + Integer.toHexString(noneCommonOption_i).toUpperCase());

                                                        setIterationTextViewText("----- #Rule13 ----- Y Wing Found!\nSquare " + oppositeSquare.getSquareNumberAsString() + " could not be a " + Integer.toHexString(noneCommonOption_i).toUpperCase());

                                                        return true;
                                                    }

                                                }

                                            }

                                        }


                                    }
                                    square_iRowNotSquare_jRow = false;
                                }


                            }
                        }
                    }

                }

            }

        }

        if (singleAction){

            setIterationTextViewText("----- #Rule13 ----- Y Wing!\nNo solution found");
        }

        return false;
    }







    protected boolean backTrackingBFSUninformedSearchReturnSuccess(AsyncTask asyncTask) {

        disableAllButtonsButCancelBackTracking();

        final long timeStart = System.currentTimeMillis();

        final Grid myGrid = Grid.getInstance();

        ArrayList<Square> allUnsolvedSquaresSorted = null;

        if (heuristicType == 0) {

            allUnsolvedSquaresSorted = myGrid.getAllUnsolvedSquaresSortedBySquareNumber();

        } else if (heuristicType == 1) {

            allUnsolvedSquaresSorted = myGrid.getAllUnsolvedSquaresSortedByCustomCellOrder();

        }  else if (heuristicType == 3) {

            allUnsolvedSquaresSorted = myGrid.getAllUnsolvedSquaresSortedByHeuristic2();

        }else {

            // default 2

            allUnsolvedSquaresSorted = myGrid.getAllUnsolvedSquaresSortedByHeuristic1();

        }

//        Collections.reverse(allUnsolvedSquaresSorted);

        int goalIndex = allUnsolvedSquaresSorted.size();

        int currentSquareIndex = 0;

        double iterationCount = 0;

        int count = 0;

        boolean unsolved = true;
        boolean unsolvable = false;

        while (unsolved == true && unsolvable == false) {

            boolean escapeLoop = false;

            count = count + 1;
            iterationCount = iterationCount + 1;

            if (asyncTask.isCancelled()) {
                break;
            }

            while (escapeLoop == false) {

                // working loop


                if (currentSquareIndex == goalIndex) {
                    // reached end and should be finished solution
                    // escape all
                    unsolved = false;

                    break;

                } else if (currentSquareIndex < 0) {

                    // all combinations tried, unsolvable
                    unsolvable = true;
                    break;
                }


                final Square currentSquare = allUnsolvedSquaresSorted.get(currentSquareIndex);


                ArrayList<Integer> possibleOptions = currentSquare.getOptionsStillPossible();


//                Collections.reverse(possibleOptions);

                int maxIndexOfCurrentOptionsArray = possibleOptions.size();

                final double itCount = iterationCount;


                if (count > 35432l) { // stops the refresh of the screen hogging the cpu

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // WORK on UI thread here

                            Grid grid = Grid.getInstance();


                            long timeEnd = System.currentTimeMillis();
                            long timeDelta = timeEnd - timeStart;

                            updateIterationsTextView(itCount, timeDelta);

                            ArrayList<GridButton> allButtons = grid.getGridButtonArrayList();

                            for (GridButton gridButton : allButtons) {


                                gridButton.setTextToDiscoveredSolution();
                            }

                        }
                    });

                    count = 0;
                }


                boolean needsValidOption = true;
                while (needsValidOption) {

                    int currentDiscovered = currentSquare.getDiscoveredAnswer();

                    int currentOptionIndex;

                    if (currentDiscovered >= 0) {
                        // if not reached this square yet set first possible option

                        currentOptionIndex = possibleOptions.indexOf(currentDiscovered) + 1;
                    } else {

                        currentOptionIndex = 0;
                    }


                    if (currentOptionIndex == maxIndexOfCurrentOptionsArray) {

                        // no more options to test , set this square to undiscovered, retreat one square index and advance it's discovered by 1
                        currentSquare.setDiscoveredAnswer(-1);
                        currentSquareIndex = currentSquareIndex - 1;
                        escapeLoop = true;
                        break;

                    } else {


                        // advance to next option index, set next discovered and retest

                        int nextOption = possibleOptions.get(currentOptionIndex);

                        currentSquare.setDiscoveredAnswer(nextOption);
                        currentDiscovered = nextOption;


                        // check if valid, if so set as discovered and move to next index
                        // else try find a further valid, if no index move back and index and advance the discovered to
                        // the next valid option.


                        if (doesSquareProposedSolutionGiveAnyGridConflicts(currentSquare, currentDiscovered) == false) {

                            // valid so advance
                            currentSquareIndex = currentSquareIndex + 1;

                            escapeLoop = true;
                            break;

                        }

                    }

                }


            }


        }





        if (unsolved == false) {

            for (Square square : allUnsolvedSquaresSorted) {

                square.setSolutionWithoutEliminationReturnTrueIfStoredNumberMatchedTheCorrectAnswer(square.getDiscoveredAnswer());

            }

            long timeEnd = System.currentTimeMillis();
            long timeDelta = timeEnd - timeStart;

            updateIterationsTextView(iterationCount, timeDelta);


            return true;
        } else {

            reenableAllButtons();
            return false;
        }


    }











    protected void reenableAllButtons(){

        // renable all buttons etc (all nasty hack jobs :( not really supposed to be updating the gui from asynctasks

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                for (Button button : allFunctionButtons) {

                    button.setEnabled(true);

                    if (button.getTag() != null) {
                        // must be back tracking button as only one with a tag

                        button.setText("Back Tracking");
                    }

                }

                Sudoku.nullifyBackTrackingTask();
            }
        });

    }

    protected void disableAllButtonsButCancelBackTracking(){

        // renable all buttons etc (all nasty hack jobs :( not really supposed to be updating the gui from asynctasks

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                for (Button button : allFunctionButtons) {

                    button.setEnabled(false);

                    if (button.getTag() != null) {
                        // must be back tracking button as only one with a tag

                        button.setText("Cancel Back Tracking");
                        button.setEnabled(true);
                    }

                }

            }
        });

    }

}