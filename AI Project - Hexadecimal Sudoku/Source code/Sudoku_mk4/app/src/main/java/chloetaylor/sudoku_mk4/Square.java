package chloetaylor.sudoku_mk4;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Chloe on 26/02/2018.
 */

public class Square implements Serializable {

    private int squareNumber;
    private int cellNumber;
    private int cellColumnNumber;
    private int cellRowNumber;
    private int gridColumnNumber;
    private int gridRowNumber;
    private int columnWithinACellNumber;
    private int rowWithinACellNumber;


    private boolean hasBeenSolved = false;
    private boolean wasAPuzzleStarterSolution = false;
    private int discoveredAnswer = -1;
    private int correctAnswer = -1;

    private boolean[] optionPossibilityArray = {true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true};


    Square(int squareNumber, int cellNumber, int cellColumnNumber, int cellRowNumber, int columnWithinACellNumber, int rowWithinACellNumber, int gridColumnNumber, int gridRowNumber) {

        this.squareNumber = squareNumber;
        this.cellNumber = cellNumber;
        this.cellColumnNumber = cellColumnNumber;
        this.cellRowNumber = cellRowNumber;
        this.columnWithinACellNumber = columnWithinACellNumber;
        this.rowWithinACellNumber = rowWithinACellNumber;
        this.gridColumnNumber = gridColumnNumber;
        this.gridRowNumber = gridRowNumber;


    }


    public int getSquareNumber() {
        return squareNumber;
    }

    public String getSquareNumberAsString() {
        return Integer.toString(squareNumber);
    }

    public int getCellNumber() {
        return cellNumber;
    }

    public int getCellColumnNumber() {
        return cellColumnNumber;
    }

    public int getCellRowNumber() {
        return cellRowNumber;
    }

    public int getGridColumnNumber() {
        return gridColumnNumber;
    }

    public int getGridRowNumber() {
        return gridRowNumber;
    }

    public int getColumnWithinACellNumber() {
        return columnWithinACellNumber;
    }

    public int getRowWithinACellNumber() {
        return rowWithinACellNumber;
    }


    public boolean isHasBeenSolved() {
        return hasBeenSolved;
    }

    public boolean isWasAPuzzleStarterSolution() {
        return wasAPuzzleStarterSolution;
    }

    public int getDiscoveredAnswer() {
        return discoveredAnswer;
    }


    public void setDiscoveredAnswer(int discoveredAnswer) {
        this.discoveredAnswer = discoveredAnswer;
    }

    public String getDiscoveredAnswerAsHexString() {

        return Integer.toHexString(discoveredAnswer).toUpperCase();

    }


    public int getCorrectAnswer() {
        return correctAnswer;
    }

    public void setPuzzleStarterFromHex(String valueAsHex) {

        String temp = valueAsHex.trim();

        if (!temp.equals("")) {
            discoveredAnswer = Integer.parseInt(temp, 16);
            wasAPuzzleStarterSolution = true;
            hasBeenSolved = true;

            optionPossibilityArray = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
            optionPossibilityArray[discoveredAnswer] = true;
        }

    }

    public void setCorrectAnswerAsHex(String hexSuppliedAnswer) {

        String temp = hexSuppliedAnswer.trim();

        if (!temp.equals("")) {
            correctAnswer = Integer.parseInt(temp, 16);
        }

    }


    public String getCorrectAnswerAsHexString() {

        return Integer.toHexString(correctAnswer).toUpperCase();

    }



    public ArrayList<Integer> getOptionsStillPossible() {


        ArrayList<Integer> holdingList = new ArrayList<>();

        int[] optionsToCheckForArray = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

        for (int option : optionsToCheckForArray) {

            if (optionPossibilityArray[option] == true) {
                holdingList.add(option);
            }
        }

        return holdingList;
    }


    protected int getNumberOfRemainingOptions() {


        int count = 0;

        for (boolean option : optionPossibilityArray) {
            if (option == true) {
                count = count + 1;
            }
        }

        return count;
    }

    protected boolean checkIfOptionIsStillMarkedAsAPossibility(int option) {

        return optionPossibilityArray[option];

    }


    protected boolean eliminateOptionAsIntReturnTrueIfWhatWasMarkedAsAValidOptionIsRemoved(int option) {

        boolean removed = false;

        if (hasBeenSolved == false && optionPossibilityArray[option] == true) {

            optionPossibilityArray[option] = false;
            removed = true;

            if (option == correctAnswer){
                System.out.println("Error - Removed the option for the correct answer");
            }


            if (getNumberOfRemainingOptions() == 0) {
                System.out.println("Error - The last remaining option was removed (Squares should have at least one option ie the solution)");
            }


        }

        return removed;
    }


    protected boolean setSolutionWithoutEliminationReturnTrueIfStoredNumberMatchedTheCorrectAnswer(int numberToStore) {


        optionPossibilityArray = new boolean[]{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false};
        optionPossibilityArray[numberToStore] = true;
        discoveredAnswer = numberToStore;
        hasBeenSolved = true;

        if (numberToStore != correctAnswer) {
            return false;
        }

    return true;
    }


    protected String getRemainingOptionsString() {

        ArrayList<Integer> remainingOptions = getOptionsStillPossible();

        String remainingString = "Remaining options :\n";


        for (int option : remainingOptions) {
            if (option ==correctAnswer) {
                remainingString = remainingString + "<font color='#FFD700'>" + Integer.toHexString(option).toUpperCase() + "</font> ";
            } else {
                remainingString = remainingString + Integer.toHexString(option).toUpperCase() + " ";
            }
        }

        return remainingString;
    }


    protected boolean setSolutionIfThereIsASoleRemainingOptionReturnIfSquareWasAlreadyMarkedAsDiscovered() {

        boolean valid = false;

        if (getNumberOfRemainingOptions() == 1 && hasBeenSolved == false) {


            discoveredAnswer = getOptionsStillPossible().get(0);
            hasBeenSolved = true;
            valid = true;

            if (discoveredAnswer != correctAnswer){
                System.out.println("----- ERROR #Rule1 -----  square " + Integer.toString(squareNumber) + " was set to the wrong answer");

            }


        }

        return valid;

    }


}