package chloetaylor.sudoku_mk4;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.AlignmentSpan;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;

/**
 * Created by Chloe on 24/02/2018.
 */

public class GridButton extends android.support.v7.widget.AppCompatButton implements Serializable {


    private int squareNumber;


    @SuppressLint("RestrictedApi")
    public GridButton(Context context, int squareNumber) {
        super(context);

        this.squareNumber = squareNumber;

        this.setText("");

        if (Build.VERSION.SDK_INT >= 26) {
            this.setAutoSizeTextTypeWithDefaults(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
        }



        this.setPadding(0, 0, 0, 0);
        this.setTextColor(this.getResources().getColor(R.color.black, null));


    }

    public GridButton(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public GridButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int WidthSpec, int HeightSpec) {
        int newHeightSpec = WidthSpec;


        super.onMeasure(WidthSpec, newHeightSpec);
    }


    public int getSquareNumber() {
        return squareNumber;
    }

    protected void setTextToFoundSolution() {

        Square square = Grid.getInstance().getSingleSquare(squareNumber);

        if (square.isHasBeenSolved() == true) {


            if (square.isWasAPuzzleStarterSolution()) {
                this.setTypeface(null, Typeface.BOLD);
                this.setTextColor(this.getResources().getColor(R.color.darkRed, null));
            } else {
                this.setTextColor(this.getResources().getColor(R.color.black, null));
            }

            int discoveredAnswer = square.getDiscoveredAnswer();

            if (discoveredAnswer >= 0) {


                this.setText(Integer.toHexString(discoveredAnswer).toUpperCase());

            } else {
                this.setText("");
            }

        }else {
            this.setText("");
        }
    }

    protected void setTextToDiscoveredSolution() {

        Square square = Grid.getInstance().getSingleSquare(squareNumber);

        if (!square.isHasBeenSolved()) {

            this.setTextColor(this.getResources().getColor(R.color.grey, null));

            int discoveredAnswer = square.getDiscoveredAnswer();

            if (discoveredAnswer >= 0) {



                this.setText(Integer.toHexString(discoveredAnswer).toUpperCase());

            } else {
                this.setText("");
            }

        }
    }




}
    