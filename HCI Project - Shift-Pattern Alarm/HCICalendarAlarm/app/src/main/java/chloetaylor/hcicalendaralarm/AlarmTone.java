package chloetaylor.hcicalendaralarm;

/**
 * Created by Chloe on 20/10/2017.
 */

public class AlarmTone {

    private String alarmToneTitle;
    private String alarmToneUri;
    private String alarmToneID;

    private int alarmVolume;


    public AlarmTone(String alarmToneID, String alarmToneTitle, String alarmToneUri, int alarmVolume) {
        this.alarmToneID = alarmToneID;
        this.alarmToneTitle = alarmToneTitle;
        this.alarmToneUri = alarmToneUri;
        this.alarmVolume = alarmVolume;
    }

    public String getAlarmToneTitle() {
        return alarmToneTitle;
    }

    public void setAlarmToneTitle(String alarmToneTitle) {
        this.alarmToneTitle = alarmToneTitle;
    }

    public String getAlarmToneUri() {
        return alarmToneUri;
    }

    public void setAlarmToneUri(String alarmToneUri) {
        this.alarmToneUri = alarmToneUri;
    }

    public String getAlarmToneID() {
        return alarmToneID;
    }

    public void setAlarmToneID(String alarmToneID) {
        this.alarmToneID = alarmToneID;
    }

    public int getAlarmVolume() {
        return alarmVolume;
    }

    public void setAlarmVolume(int alarmVolume) {
        this.alarmVolume = alarmVolume;
    }

    @Override
    public String toString() {
        return this.alarmToneTitle;            // What to display in the Spinner list.
    }

}
