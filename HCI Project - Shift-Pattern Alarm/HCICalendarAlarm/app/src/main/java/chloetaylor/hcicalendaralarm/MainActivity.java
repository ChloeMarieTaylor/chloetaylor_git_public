package chloetaylor.hcicalendaralarm;

import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.icu.util.GregorianCalendar;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Layout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.AlignmentSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

// terrible hack job of code done purely for ease of demonstration. Certainly not a fine example of my
// coding skills


public class MainActivity extends AppCompatActivity {

private boolean firstRun = true;

private int datesSelected [] [] = {
{0,0}, // rows shown, week advance
{0,0,0,0,0,0,0}, // row 1 buttons
{0,0,0,0,0,0,0}, // row 2 buttons
{0,0,0,0,0,0,0}, // row 3 buttons
{0,0,0,0,0,0,0}, // row 4 buttons
{0,0,0,0,0,0,0}, // row 5 buttons
{0,0,0,0,0,0,0}, // row 6 buttons
{0,0,0,0,0,0,0}, // row 7 buttons
{0,0,0,0,0,0,0} // row 8 buttons
};

private int timeSelected [][] = {
{0,0}, // time hours/mins
{0}, // alarmTone Index
{0} // alarm Vibrate
};

private boolean timeToggle24hr = true;

private String alarmLabelName = "Alarm 1";

private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
= new BottomNavigationView.OnNavigationItemSelectedListener() {

@Override
public boolean onNavigationItemSelected(@NonNull MenuItem item) {

FrameLayout content = (FrameLayout) findViewById(R.id.content);

LinearLayout inflatedLayoutTime = content.findViewById(R.id.main_time_frame);
LinearLayout inflatedLayoutDate = content.findViewById(R.id.main_date_frame);
LinearLayout inflatedLayoutRepeat = content.findViewById(R.id.main_repeat_frame);

final LinearLayout row1 = (LinearLayout) findViewById(R.id.row1);
final Button button_row1_1 = (Button) findViewById(R.id.button_row1_1);
final Button button_row1_2 = (Button) findViewById(R.id.button_row1_2);
final Button button_row1_3 = (Button) findViewById(R.id.button_row1_3);
final Button button_row1_4 = (Button) findViewById(R.id.button_row1_4);
final Button button_row1_5 = (Button) findViewById(R.id.button_row1_5);
final Button button_row1_6 = (Button) findViewById(R.id.button_row1_6);
final Button button_row1_7 = (Button) findViewById(R.id.button_row1_7);
final Button button_row1_8 = (Button) findViewById(R.id.button_row1_8);

final LinearLayout row2 = (LinearLayout) findViewById(R.id.row2);
final Button button_row2_1 = (Button) findViewById(R.id.button_row2_1);
final Button button_row2_2 = (Button) findViewById(R.id.button_row2_2);
final Button button_row2_3 = (Button) findViewById(R.id.button_row2_3);
final Button button_row2_4 = (Button) findViewById(R.id.button_row2_4);
final Button button_row2_5 = (Button) findViewById(R.id.button_row2_5);
final Button button_row2_6 = (Button) findViewById(R.id.button_row2_6);
final Button button_row2_7 = (Button) findViewById(R.id.button_row2_7);
final Button button_row2_8 = (Button) findViewById(R.id.button_row2_8);

final LinearLayout row3 = (LinearLayout) findViewById(R.id.row3);
final Button button_row3_1 = (Button) findViewById(R.id.button_row3_1);
final Button button_row3_2 = (Button) findViewById(R.id.button_row3_2);
final Button button_row3_3 = (Button) findViewById(R.id.button_row3_3);
final Button button_row3_4 = (Button) findViewById(R.id.button_row3_4);
final Button button_row3_5 = (Button) findViewById(R.id.button_row3_5);
final Button button_row3_6 = (Button) findViewById(R.id.button_row3_6);
final Button button_row3_7 = (Button) findViewById(R.id.button_row3_7);
final Button button_row3_8 = (Button) findViewById(R.id.button_row3_8);

final LinearLayout row4 = (LinearLayout) findViewById(R.id.row4);
final Button button_row4_1 = (Button) findViewById(R.id.button_row4_1);
final Button button_row4_2 = (Button) findViewById(R.id.button_row4_2);
final Button button_row4_3 = (Button) findViewById(R.id.button_row4_3);
final Button button_row4_4 = (Button) findViewById(R.id.button_row4_4);
final Button button_row4_5 = (Button) findViewById(R.id.button_row4_5);
final Button button_row4_6 = (Button) findViewById(R.id.button_row4_6);
final Button button_row4_7 = (Button) findViewById(R.id.button_row4_7);
final Button button_row4_8 = (Button) findViewById(R.id.button_row4_8);

final LinearLayout row5 = (LinearLayout) findViewById(R.id.row5);
final Button button_row5_1 = (Button) findViewById(R.id.button_row5_1);
final Button button_row5_2 = (Button) findViewById(R.id.button_row5_2);
final Button button_row5_3 = (Button) findViewById(R.id.button_row5_3);
final Button button_row5_4 = (Button) findViewById(R.id.button_row5_4);
final Button button_row5_5 = (Button) findViewById(R.id.button_row5_5);
final Button button_row5_6 = (Button) findViewById(R.id.button_row5_6);
final Button button_row5_7 = (Button) findViewById(R.id.button_row5_7);
final Button button_row5_8 = (Button) findViewById(R.id.button_row5_8);

final LinearLayout row6 = (LinearLayout) findViewById(R.id.row6);
final Button button_row6_1 = (Button) findViewById(R.id.button_row6_1);
final Button button_row6_2 = (Button) findViewById(R.id.button_row6_2);
final Button button_row6_3 = (Button) findViewById(R.id.button_row6_3);
final Button button_row6_4 = (Button) findViewById(R.id.button_row6_4);
final Button button_row6_5 = (Button) findViewById(R.id.button_row6_5);
final Button button_row6_6 = (Button) findViewById(R.id.button_row6_6);
final Button button_row6_7 = (Button) findViewById(R.id.button_row6_7);
final Button button_row6_8 = (Button) findViewById(R.id.button_row6_8);

final LinearLayout row7 = (LinearLayout) findViewById(R.id.row7);
final Button button_row7_1 = (Button) findViewById(R.id.button_row7_1);
final Button button_row7_2 = (Button) findViewById(R.id.button_row7_2);
final Button button_row7_3 = (Button) findViewById(R.id.button_row7_3);
final Button button_row7_4 = (Button) findViewById(R.id.button_row7_4);
final Button button_row7_5 = (Button) findViewById(R.id.button_row7_5);
final Button button_row7_6 = (Button) findViewById(R.id.button_row7_6);
final Button button_row7_7 = (Button) findViewById(R.id.button_row7_7);
final Button button_row7_8 = (Button) findViewById(R.id.button_row7_8);

final LinearLayout row8 = (LinearLayout) findViewById(R.id.row8);
final Button button_row8_1 = (Button) findViewById(R.id.button_row8_1);
final Button button_row8_2 = (Button) findViewById(R.id.button_row8_2);
final Button button_row8_3 = (Button) findViewById(R.id.button_row8_3);
final Button button_row8_4 = (Button) findViewById(R.id.button_row8_4);
final Button button_row8_5 = (Button) findViewById(R.id.button_row8_5);
final Button button_row8_6 = (Button) findViewById(R.id.button_row8_6);
final Button button_row8_7 = (Button) findViewById(R.id.button_row8_7);
final Button button_row8_8 = (Button) findViewById(R.id.button_row8_8);


final TextView week1 = (TextView) findViewById(R.id.textView_week1);
final TextView week2 = (TextView) findViewById(R.id.textView_week2);
final TextView week3 = (TextView) findViewById(R.id.textView_week3);
final TextView week4 = (TextView) findViewById(R.id.textView_week4);
final TextView week5 = (TextView) findViewById(R.id.textView_week5);
final TextView week6 = (TextView) findViewById(R.id.textView_week6);
final TextView week7 = (TextView) findViewById(R.id.textView_week7);
final TextView week8 = (TextView) findViewById(R.id.textView_week8);

final Calendar c2 = Calendar.getInstance();
c2.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

new GregorianCalendar();
Calendar cal_switch;

cal_switch = Calendar.getInstance();
cal_switch.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

cal_switch.add(Calendar.DAY_OF_MONTH, (7 * datesSelected[0][1]));

SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");
String wk1 = formatter.format(cal_switch);

Calendar cal2 = cal_switch;

cal2.add(Calendar.DAY_OF_MONTH, 6);
String wk1end = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 1);

String wk2 = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 6);
String wk2end = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 1);

String wk3 = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 6);
String wk3end = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 1);

String wk4 = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 6);
String wk4end = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 1);

String wk5 = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 6);
String wk5end = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 1);

String wk6 = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 6);
String wk6end = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 1);

String wk7 = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 6);
String wk7end = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 1);

String wk8 = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 6);
String wk8end = formatter.format(cal2);
cal2.add(Calendar.DAY_OF_MONTH, 1);



final BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.navigation);

switch (item.getItemId()) {
case R.id.navigation_time:


inflatedLayoutTime.setVisibility(View.VISIBLE);
inflatedLayoutDate.setVisibility(View.GONE);
inflatedLayoutRepeat.setVisibility(View.GONE);

content.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this) {
public void onSwipeTop() {
}

public void onSwipeRight() {

bottomNavigationView.setSelectedItemId(R.id.navigation_repeat);

}

public void onSwipeLeft() {


bottomNavigationView.setSelectedItemId(R.id.navigation_date);
}

public void onSwipeBottom() {
}

});


return true;
case R.id.navigation_date:


inflatedLayoutTime.setVisibility(View.GONE);
inflatedLayoutDate.setVisibility(View.VISIBLE);
inflatedLayoutRepeat.setVisibility(View.GONE);

content.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this) {
public void onSwipeTop() {
}

public void onSwipeRight() {

bottomNavigationView.setSelectedItemId(R.id.navigation_time);

}

public void onSwipeLeft() {


bottomNavigationView.setSelectedItemId(R.id.navigation_repeat);
}

public void onSwipeBottom() {
}

});



if (datesSelected[1][0] == 1) {
button_row1_1.setActivated(true);
}
if (datesSelected[1][1] == 1) {
button_row1_2.setActivated(true);
}
if (datesSelected[1][2] == 1) {
button_row1_3.setActivated(true);
}
if (datesSelected[1][3] == 1) {
button_row1_4.setActivated(true);
}
if (datesSelected[1][4] == 1) {
button_row1_5.setActivated(true);
}
if (datesSelected[1][5] == 1) {
button_row1_6.setActivated(true);
}
if (datesSelected[1][6] == 1) {
button_row1_7.setActivated(true);
}

if (datesSelected[2][0] == 1) {
button_row2_1.setActivated(true);
}
if (datesSelected[2][1] == 1) {
button_row2_2.setActivated(true);
}
if (datesSelected[2][2] == 1) {
button_row2_3.setActivated(true);
}
if (datesSelected[2][3] == 1) {
button_row2_4.setActivated(true);
}
if (datesSelected[2][4] == 1) {
button_row2_5.setActivated(true);
}
if (datesSelected[2][5] == 1) {
button_row2_6.setActivated(true);
}
if (datesSelected[2][6] == 1) {
button_row2_7.setActivated(true);
}

if (datesSelected[3][0] == 1) {
button_row3_1.setActivated(true);
}
if (datesSelected[3][1] == 1) {
button_row3_2.setActivated(true);
}
if (datesSelected[3][2] == 1) {
button_row3_3.setActivated(true);
}
if (datesSelected[3][3] == 1) {
button_row3_4.setActivated(true);
}
if (datesSelected[3][4] == 1) {
button_row3_5.setActivated(true);
}
if (datesSelected[3][5] == 1) {
button_row3_6.setActivated(true);
}
if (datesSelected[3][6] == 1) {
button_row3_7.setActivated(true);
}

if (datesSelected[4][0] == 1) {
button_row4_1.setActivated(true);
}
if (datesSelected[4][1] == 1) {
button_row4_2.setActivated(true);
}
if (datesSelected[4][2] == 1) {
button_row4_3.setActivated(true);
}
if (datesSelected[4][3] == 1) {
button_row4_4.setActivated(true);
}
if (datesSelected[4][4] == 1) {
button_row4_5.setActivated(true);
}
if (datesSelected[4][5] == 1) {
button_row4_6.setActivated(true);
}
if (datesSelected[4][6] == 1) {
button_row4_7.setActivated(true);
}

if (datesSelected[5][0] == 1) {
button_row5_1.setActivated(true);
}
if (datesSelected[5][1] == 1) {
button_row5_2.setActivated(true);
}
if (datesSelected[5][2] == 1) {
button_row5_3.setActivated(true);
}
if (datesSelected[5][3] == 1) {
button_row5_4.setActivated(true);
}
if (datesSelected[5][4] == 1) {
button_row5_5.setActivated(true);
}
if (datesSelected[5][5] == 1) {
button_row5_6.setActivated(true);
}
if (datesSelected[5][6] == 1) {
button_row5_7.setActivated(true);
}

if (datesSelected[6][0] == 1) {
button_row6_1.setActivated(true);
}
if (datesSelected[6][1] == 1) {
button_row6_2.setActivated(true);
}
if (datesSelected[6][2] == 1) {
button_row6_3.setActivated(true);
}
if (datesSelected[6][3] == 1) {
button_row6_4.setActivated(true);
}
if (datesSelected[6][4] == 1) {
button_row6_5.setActivated(true);
}
if (datesSelected[6][5] == 1) {
button_row6_6.setActivated(true);
}
if (datesSelected[6][6] == 1) {
button_row6_7.setActivated(true);
}

if (datesSelected[7][0] == 1) {
button_row7_1.setActivated(true);
}
if (datesSelected[7][1] == 1) {
button_row7_2.setActivated(true);
}
if (datesSelected[7][2] == 1) {
button_row7_3.setActivated(true);
}
if (datesSelected[7][3] == 1) {
button_row7_4.setActivated(true);
}
if (datesSelected[7][4] == 1) {
button_row7_5.setActivated(true);
}
if (datesSelected[7][5] == 1) {
button_row7_6.setActivated(true);
}
if (datesSelected[7][6] == 1) {
button_row7_7.setActivated(true);
}

if (datesSelected[8][0] == 1) {
button_row8_1.setActivated(true);
}
if (datesSelected[8][1] == 1) {
button_row8_2.setActivated(true);
}
if (datesSelected[8][2] == 1) {
button_row8_3.setActivated(true);
}
if (datesSelected[8][3] == 1) {
button_row8_4.setActivated(true);
}
if (datesSelected[8][4] == 1) {
button_row8_5.setActivated(true);
}
if (datesSelected[8][5] == 1) {
button_row8_6.setActivated(true);
}
if (datesSelected[8][6] == 1) {
button_row8_7.setActivated(true);
}

if (datesSelected[0][0] == 0) {

button_row1_8.setText("+");
button_row2_8.setText("+");
button_row3_8.setText("+");
button_row4_8.setText("+");
button_row5_8.setText("+");
button_row6_8.setText("+");
button_row7_8.setText("+");

row1.setVisibility(View.VISIBLE);
row2.setVisibility(View.INVISIBLE);
row3.setVisibility(View.INVISIBLE);
row4.setVisibility(View.INVISIBLE);
row5.setVisibility(View.INVISIBLE);
row6.setVisibility(View.INVISIBLE);
row7.setVisibility(View.INVISIBLE);
row8.setVisibility(View.INVISIBLE);

week1.setVisibility(View.VISIBLE);
week2.setVisibility(View.INVISIBLE);
week3.setVisibility(View.INVISIBLE);
week4.setVisibility(View.INVISIBLE);
week5.setVisibility(View.INVISIBLE);
week6.setVisibility(View.INVISIBLE);
week7.setVisibility(View.INVISIBLE);
week8.setVisibility(View.INVISIBLE);

} else if (datesSelected[0][0] == 1) {

button_row1_8.setText("-");
button_row2_8.setText("+");
button_row3_8.setText("+");
button_row4_8.setText("+");
button_row5_8.setText("+");
button_row6_8.setText("+");
button_row7_8.setText("+");

row1.setVisibility(View.VISIBLE);
row2.setVisibility(View.VISIBLE);
row3.setVisibility(View.INVISIBLE);
row4.setVisibility(View.INVISIBLE);
row5.setVisibility(View.INVISIBLE);
row6.setVisibility(View.INVISIBLE);
row7.setVisibility(View.INVISIBLE);
row8.setVisibility(View.INVISIBLE);

week1.setVisibility(View.VISIBLE);
week2.setVisibility(View.VISIBLE);
week3.setVisibility(View.INVISIBLE);
week4.setVisibility(View.INVISIBLE);
week5.setVisibility(View.INVISIBLE);
week6.setVisibility(View.INVISIBLE);
week7.setVisibility(View.INVISIBLE);
week8.setVisibility(View.INVISIBLE);

} else if (datesSelected[0][0] == 2) {

button_row1_8.setVisibility(View.INVISIBLE);
button_row2_8.setText("-");
button_row3_8.setText("+");
button_row4_8.setText("+");
button_row5_8.setText("+");
button_row6_8.setText("+");
button_row7_8.setText("+");

row1.setVisibility(View.VISIBLE);
row2.setVisibility(View.VISIBLE);
row3.setVisibility(View.VISIBLE);
row4.setVisibility(View.INVISIBLE);
row5.setVisibility(View.INVISIBLE);
row6.setVisibility(View.INVISIBLE);
row7.setVisibility(View.INVISIBLE);
row8.setVisibility(View.INVISIBLE);

week1.setVisibility(View.VISIBLE);
week2.setVisibility(View.VISIBLE);
week3.setVisibility(View.VISIBLE);
week4.setVisibility(View.INVISIBLE);
week5.setVisibility(View.INVISIBLE);
week6.setVisibility(View.INVISIBLE);
week7.setVisibility(View.INVISIBLE);
week8.setVisibility(View.INVISIBLE);

} else if (datesSelected[0][0] == 3) {

button_row1_8.setVisibility(View.INVISIBLE);
button_row2_8.setVisibility(View.INVISIBLE);
button_row3_8.setText("-");
button_row4_8.setText("+");
button_row5_8.setText("+");
button_row6_8.setText("+");
button_row7_8.setText("+");

row1.setVisibility(View.VISIBLE);
row2.setVisibility(View.VISIBLE);
row3.setVisibility(View.VISIBLE);
row4.setVisibility(View.VISIBLE);
row5.setVisibility(View.INVISIBLE);
row6.setVisibility(View.INVISIBLE);
row7.setVisibility(View.INVISIBLE);
row8.setVisibility(View.INVISIBLE);

week1.setVisibility(View.VISIBLE);
week2.setVisibility(View.VISIBLE);
week3.setVisibility(View.VISIBLE);
week4.setVisibility(View.VISIBLE);
week5.setVisibility(View.INVISIBLE);
week6.setVisibility(View.INVISIBLE);
week7.setVisibility(View.INVISIBLE);
week8.setVisibility(View.INVISIBLE);

} else if (datesSelected[0][0] == 4) {

button_row1_8.setVisibility(View.INVISIBLE);
button_row2_8.setVisibility(View.INVISIBLE);
button_row3_8.setVisibility(View.INVISIBLE);
button_row4_8.setText("-");
button_row5_8.setText("+");
button_row6_8.setText("+");
button_row7_8.setText("+");

row1.setVisibility(View.VISIBLE);
row2.setVisibility(View.VISIBLE);
row3.setVisibility(View.VISIBLE);
row4.setVisibility(View.VISIBLE);
row5.setVisibility(View.VISIBLE);
row6.setVisibility(View.INVISIBLE);
row7.setVisibility(View.INVISIBLE);
row8.setVisibility(View.INVISIBLE);

week1.setVisibility(View.VISIBLE);
week2.setVisibility(View.VISIBLE);
week3.setVisibility(View.VISIBLE);
week4.setVisibility(View.VISIBLE);
week5.setVisibility(View.VISIBLE);
week6.setVisibility(View.INVISIBLE);
week7.setVisibility(View.INVISIBLE);
week8.setVisibility(View.INVISIBLE);

} else if (datesSelected[0][0] == 5) {

button_row1_8.setVisibility(View.INVISIBLE);
button_row2_8.setVisibility(View.INVISIBLE);
button_row3_8.setVisibility(View.INVISIBLE);
button_row4_8.setVisibility(View.INVISIBLE);
button_row5_8.setText("-");
button_row6_8.setText("+");
button_row7_8.setText("+");

row1.setVisibility(View.VISIBLE);
row2.setVisibility(View.VISIBLE);
row3.setVisibility(View.VISIBLE);
row4.setVisibility(View.VISIBLE);
row5.setVisibility(View.VISIBLE);
row6.setVisibility(View.VISIBLE);
row7.setVisibility(View.INVISIBLE);
row8.setVisibility(View.INVISIBLE);

week1.setVisibility(View.VISIBLE);
week2.setVisibility(View.VISIBLE);
week3.setVisibility(View.VISIBLE);
week4.setVisibility(View.VISIBLE);
week5.setVisibility(View.VISIBLE);
week6.setVisibility(View.VISIBLE);
week7.setVisibility(View.INVISIBLE);
week8.setVisibility(View.INVISIBLE);

} else if (datesSelected[0][0] == 6) {

button_row1_8.setVisibility(View.INVISIBLE);
button_row2_8.setVisibility(View.INVISIBLE);
button_row3_8.setVisibility(View.INVISIBLE);
button_row4_8.setVisibility(View.INVISIBLE);
button_row5_8.setVisibility(View.INVISIBLE);
button_row6_8.setText("-");
button_row6_8.setText("+");

row1.setVisibility(View.VISIBLE);
row2.setVisibility(View.VISIBLE);
row3.setVisibility(View.VISIBLE);
row4.setVisibility(View.VISIBLE);
row5.setVisibility(View.VISIBLE);
row6.setVisibility(View.VISIBLE);
row7.setVisibility(View.VISIBLE);
row8.setVisibility(View.INVISIBLE);

week1.setVisibility(View.VISIBLE);
week2.setVisibility(View.VISIBLE);
week3.setVisibility(View.VISIBLE);
week4.setVisibility(View.VISIBLE);
week5.setVisibility(View.VISIBLE);
week6.setVisibility(View.VISIBLE);
week7.setVisibility(View.VISIBLE);
week8.setVisibility(View.INVISIBLE);

} else if (datesSelected[0][0] == 7) {

button_row1_8.setVisibility(View.INVISIBLE);
button_row2_8.setVisibility(View.INVISIBLE);
button_row3_8.setVisibility(View.INVISIBLE);
button_row4_8.setVisibility(View.INVISIBLE);
button_row5_8.setVisibility(View.INVISIBLE);
button_row6_8.setVisibility(View.INVISIBLE);
button_row6_8.setText("-");

row1.setVisibility(View.VISIBLE);
row2.setVisibility(View.VISIBLE);
row3.setVisibility(View.VISIBLE);
row4.setVisibility(View.VISIBLE);
row5.setVisibility(View.VISIBLE);
row6.setVisibility(View.VISIBLE);
row7.setVisibility(View.VISIBLE);
row8.setVisibility(View.VISIBLE);

week1.setVisibility(View.VISIBLE);
week2.setVisibility(View.VISIBLE);
week3.setVisibility(View.VISIBLE);
week4.setVisibility(View.VISIBLE);
week5.setVisibility(View.VISIBLE);
week6.setVisibility(View.VISIBLE);
week7.setVisibility(View.VISIBLE);
week8.setVisibility(View.VISIBLE);

}




week1.setText("Week 1 - " + wk1);
week2.setText("Week 2 - " + wk2);
week3.setText("Week 3 - " + wk3);
week4.setText("Week 4 - " + wk4);
week5.setText("Week 5 - " + wk5);
week6.setText("Week 6 - " + wk6);
week7.setText("Week 7 - " + wk7);
week8.setText("Week 8 - " + wk8);


return true;

case R.id.navigation_repeat:

   


inflatedLayoutTime.setVisibility(View.GONE);
inflatedLayoutDate.setVisibility(View.GONE);
inflatedLayoutRepeat.setVisibility(View.VISIBLE);

content.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this) {
public void onSwipeTop() {
}

public void onSwipeRight() {

bottomNavigationView.setSelectedItemId(R.id.navigation_date);

}

public void onSwipeLeft() {


bottomNavigationView.setSelectedItemId(R.id.navigation_time);
}

public void onSwipeBottom() {
}

});

final TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);

int hour = timePicker.getHour();
int minute = timePicker.getMinute();

TextView summaryTime = (TextView) findViewById(R.id.textView_time);
TextView summary = (TextView) findViewById(R.id.textView_summary);
TextView summaryStart = (TextView) findViewById(R.id.textView_summary2);
final TextView summaryEnd = (TextView) findViewById(R.id.textView_summary3);

summaryTime.setText("Alarm time - " + String.format("%02d", hour) + ":" + String.format("%02d", minute));


int numberOfWeeks = datesSelected[0][0];








// repeat section


final NumberPicker numberPicker = (NumberPicker) findViewById(R.id.numberPicker);

switch(numberOfWeeks) {

case 0 :
summary.setText("Single week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 1 :
summary.setText("Two week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 2 :
summary.setText("Three week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 3 :
summary.setText("Four week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 4 :
summary.setText("Five week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 5 :
summary.setText("Six week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 6 :
summary.setText("Seven week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 7 :
summary.setText("Eight week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
default :
System.out.println("Invalid grade");
break;


}

final TextView numberRepeats = (TextView) findViewById(R.id.numberPickerText);
final TextView repeatUntilDate = (TextView) findViewById(R.id.numberPickerText2);

numberPicker.setMinValue(1);

// find max number of repeats in 2 years

int period = datesSelected[0][0] + 1;

numberPicker.setMaxValue((int) Math.floor(104 / period));




final String [] dates = new String [(730 - (7 * datesSelected[0][1]))];

new GregorianCalendar();
Calendar cal_days = Calendar.getInstance();


cal_days.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

cal_days.add(Calendar.DAY_OF_MONTH, (7 * datesSelected[0][1]));

SimpleDateFormat formatter2 = new SimpleDateFormat("d MMM yyyy, EEE");


for (int i = 0; i < (730 - (7 * datesSelected[0][1])); i = i + 1) {

String day = formatter2.format(cal_days);

dates[i] = day;
cal_days.add(Calendar.DAY_OF_MONTH, 1);

}






final NumberPicker numberPickerDays = (NumberPicker) findViewById(R.id.numberPicker2);


numberPickerDays.setMinValue(0);
numberPickerDays.setMaxValue(dates.length - 1);

numberPickerDays.setDisplayedValues(dates);


    final LinearLayout picker1Box = (LinearLayout) findViewById(R.id.picker1_box);
    final LinearLayout picker2Box = (LinearLayout) findViewById(R.id.picker2_box);


    picker1Box.setOnTouchListener( new TextView.OnTouchListener(){

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            picker1Box.setBackgroundResource(R.drawable.button_border_red);
            picker2Box.setBackgroundResource(R.drawable.button_border);
            return false;
        }


    });

    picker2Box.setOnTouchListener( new TextView.OnTouchListener(){

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            picker1Box.setBackgroundResource(R.drawable.button_border);
            picker2Box.setBackgroundResource(R.drawable.button_border_red);
            return false;
        }


    });




numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
@Override
public void onValueChange(NumberPicker numberPicker, final int i, final int i2) {

if (i2 == 1) {
numberRepeats.setText("Add a single occurrance (maximum duration span is 2 years)");
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
} else if (i2 == 2){
numberRepeats.setText("Repeat an extra " + Integer.toString(i2 - 1) + " time (maximum duration span is 2 years)");


summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
}

else {
numberRepeats.setText("Repeat an extra " + Integer.toString(i2 - 1) + " times (maximum duration span is 2 years)");
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
}

repeatUntilDate.setText("Repeat until " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()) + " (max duration span is 2 years)");

int arrayIndex = Arrays.asList(dates).indexOf(changeDateFormatReverse(returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue())));

numberPickerDays.setValue(arrayIndex);



    picker1Box.setBackgroundResource(R.drawable.button_border_red);
    picker2Box.setBackgroundResource(R.drawable.button_border);

    TextView numberPickerText = (TextView) findViewById(R.id.numberPickerText);
    TextView numberPickerText2 = (TextView) findViewById(R.id.numberPickerText2);

    numberPicker.setOnTouchListener( new TextView.OnTouchListener(){

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            picker1Box.setBackgroundResource(R.drawable.button_border_red);
            picker2Box.setBackgroundResource(R.drawable.button_border);
            return false;
        }


    });


    numberPickerDays.setOnTouchListener( new TextView.OnTouchListener(){

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            picker1Box.setBackgroundResource(R.drawable.button_border);
            picker2Box.setBackgroundResource(R.drawable.button_border_red);
            return false;
        }


    });



}
});



numberPickerDays.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
@Override
public void onValueChange(NumberPicker numberPickerDays, final int i, final int i2) {

summaryEnd.setText("ending: " + changeDateFormat(dates[i2]));
repeatUntilDate.setText("Repeat until "+ changeDateFormat(dates[i2]) + " (max duration span is 2 years)");


int numberOfDaysTillTheDate = i2 + 1;

int numberOfDaysInShift = 7 * (datesSelected[0][0] + 1);


int repeat = (int) Math.floor(numberOfDaysTillTheDate / numberOfDaysInShift);

if (repeat == 0){
repeat = 1;
}
numberPicker.setValue(repeat);



LinearLayout picker1Box = (LinearLayout) findViewById(R.id.picker1_box);
LinearLayout picker2Box = (LinearLayout) findViewById(R.id.picker2_box);

picker1Box.setBackgroundResource(R.drawable.button_border);
picker2Box.setBackgroundResource(R.drawable.button_border_red);

}
});




}
return true;
}

};




@Override
protected void onCreate(Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
setContentView(R.layout.activity_main);


final BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


FrameLayout content = (FrameLayout) findViewById(R.id.content);

LayoutInflater inflater = LayoutInflater.from(this);

View inflatedLayoutTime = inflater.inflate(R.layout.activity_main_time, null, false);
View inflatedLayoutDate = inflater.inflate(R.layout.activity_main_date, null, false);
View inflatedLayoutRepeat = inflater.inflate(R.layout.activity_main_repeat, null, false);




content.addView(inflatedLayoutTime);

content.addView(inflatedLayoutDate);
inflatedLayoutDate.setVisibility(View.GONE);

content.addView(inflatedLayoutRepeat);
inflatedLayoutRepeat.setVisibility(View.GONE);


LinearLayout layoutTime = content.findViewById(R.id.main_time_frame);
LinearLayout layoutDate = content.findViewById(R.id.main_date_frame);
LinearLayout layoutRepeat = content.findViewById(R.id.main_repeat_frame);



final BottomNavigationView bottomNavigationView = (BottomNavigationView)findViewById(R.id.navigation);

content.setOnTouchListener(new OnSwipeTouchListener(MainActivity.this) {
public void onSwipeTop() {
}
public void onSwipeRight() {

bottomNavigationView.setSelectedItemId(R.id.navigation_date);

}
public void onSwipeLeft() {


bottomNavigationView.setSelectedItemId(R.id.navigation_repeat);
}

public void onSwipeBottom() {
}

});


final TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);

timePicker.setIs24HourView(true);

    final int id = Resources.getSystem().getIdentifier("ampm_layout", "id", "android");
    final View amPmLayout = timePicker.findViewById(id);

amPmLayout.setVisibility(View.INVISIBLE);

timePicker.setHour(0);
timePicker.setMinute(0);

final Button timeToggleButton = (Button) findViewById(R.id.timeToggleButton);

timeToggleButton.setOnClickListener(new Button.OnClickListener(){



    @Override
    public void onClick(View view) {
        if (timeToggle24hr == true){
            timePicker.setIs24HourView(false);
            amPmLayout.setVisibility(View.VISIBLE);
            timeToggle24hr = false;
            timeToggleButton.setText("24 Hour");
        } else {
            timePicker.setIs24HourView(true);
            amPmLayout.setVisibility(View.INVISIBLE);
            timeToggle24hr = true;
            timeToggleButton.setText("12 Hour");
        }
    }
});

timePicker.hasFocus();

List<AlarmTone> alarmTonesList = getAlarmTones();

final EditText alarmLabel = (EditText) findViewById(R.id.editText_alarm_label);

alarmLabel.setText(alarmLabelName);

alarmLabel.setOnTouchListener(new View.OnTouchListener(){


    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        alarmLabel.setSelectAllOnFocus(true);
        return false;
    }
});

alarmLabel.addTextChangedListener(new TextWatcher() {


@Override
public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

}

@Override
public void onTextChanged(CharSequence s, int start,
  int before, int count) {

}

@Override
public void afterTextChanged(Editable editable) {

alarmLabelName = editable.toString();
}
});




final Spinner tone_spinner = (Spinner) findViewById(R.id.spinner_alarm_tone);
ArrayAdapter<AlarmTone> spinnerArrayAdapter = new ArrayAdapter<AlarmTone>
(this, android.R.layout.simple_spinner_item,
alarmTonesList); //selected item will look like a spinner set from XML
spinnerArrayAdapter.setDropDownViewResource(android.R.layout
.simple_spinner_dropdown_item);
tone_spinner.setAdapter(spinnerArrayAdapter);



tone_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
@Override
public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
// your code here

if (firstRun == false) {

AlarmTone spinnnerSelectedItem = (AlarmTone) tone_spinner.getSelectedItem();

SeekBar seekVolume = (SeekBar) findViewById(R.id.seekBarVolume);


// get alarm volume

int MAX_VOLUME = 100;

int soundVolume = seekVolume.getProgress() * 10;

final float volume = (float) (1 - (Math.log(MAX_VOLUME - soundVolume) / Math.log(MAX_VOLUME)));



Uri notification = Uri.parse(spinnnerSelectedItem.getAlarmToneUri() + "/" + spinnnerSelectedItem.getAlarmToneID());
final MediaPlayer mp = MediaPlayer.create(getApplicationContext(), notification);

mp.setVolume(volume, volume);
mp.start();

new Handler().postDelayed(new Runnable() {
@Override
public void run() {
mp.stop();
}
}, 5000);//millisec
} else {

firstRun = false;
}

final LinearLayout row1 = (LinearLayout) findViewById(R.id.row1);
final Button button_row1_1 = (Button) findViewById(R.id.button_row1_1);
final Button button_row1_2 = (Button) findViewById(R.id.button_row1_2);
final Button button_row1_3 = (Button) findViewById(R.id.button_row1_3);
final Button button_row1_4 = (Button) findViewById(R.id.button_row1_4);
final Button button_row1_5 = (Button) findViewById(R.id.button_row1_5);
final Button button_row1_6 = (Button) findViewById(R.id.button_row1_6);
final Button button_row1_7 = (Button) findViewById(R.id.button_row1_7);
final Button button_row1_8 = (Button) findViewById(R.id.button_row1_8);

final LinearLayout row2 = (LinearLayout) findViewById(R.id.row2);
final Button button_row2_1 = (Button) findViewById(R.id.button_row2_1);
final Button button_row2_2 = (Button) findViewById(R.id.button_row2_2);
final Button button_row2_3 = (Button) findViewById(R.id.button_row2_3);
final Button button_row2_4 = (Button) findViewById(R.id.button_row2_4);
final Button button_row2_5 = (Button) findViewById(R.id.button_row2_5);
final Button button_row2_6 = (Button) findViewById(R.id.button_row2_6);
final Button button_row2_7 = (Button) findViewById(R.id.button_row2_7);
final Button button_row2_8 = (Button) findViewById(R.id.button_row2_8);

final LinearLayout row3 = (LinearLayout) findViewById(R.id.row3);
final Button button_row3_1 = (Button) findViewById(R.id.button_row3_1);
final Button button_row3_2 = (Button) findViewById(R.id.button_row3_2);
final Button button_row3_3 = (Button) findViewById(R.id.button_row3_3);
final Button button_row3_4 = (Button) findViewById(R.id.button_row3_4);
final Button button_row3_5 = (Button) findViewById(R.id.button_row3_5);
final Button button_row3_6 = (Button) findViewById(R.id.button_row3_6);
final Button button_row3_7 = (Button) findViewById(R.id.button_row3_7);
final Button button_row3_8 = (Button) findViewById(R.id.button_row3_8);

final LinearLayout row4 = (LinearLayout) findViewById(R.id.row4);
final Button button_row4_1 = (Button) findViewById(R.id.button_row4_1);
final Button button_row4_2 = (Button) findViewById(R.id.button_row4_2);
final Button button_row4_3 = (Button) findViewById(R.id.button_row4_3);
final Button button_row4_4 = (Button) findViewById(R.id.button_row4_4);
final Button button_row4_5 = (Button) findViewById(R.id.button_row4_5);
final Button button_row4_6 = (Button) findViewById(R.id.button_row4_6);
final Button button_row4_7 = (Button) findViewById(R.id.button_row4_7);
final Button button_row4_8 = (Button) findViewById(R.id.button_row4_8);

final LinearLayout row5 = (LinearLayout) findViewById(R.id.row5);
final Button button_row5_1 = (Button) findViewById(R.id.button_row5_1);
final Button button_row5_2 = (Button) findViewById(R.id.button_row5_2);
final Button button_row5_3 = (Button) findViewById(R.id.button_row5_3);
final Button button_row5_4 = (Button) findViewById(R.id.button_row5_4);
final Button button_row5_5 = (Button) findViewById(R.id.button_row5_5);
final Button button_row5_6 = (Button) findViewById(R.id.button_row5_6);
final Button button_row5_7 = (Button) findViewById(R.id.button_row5_7);
final Button button_row5_8 = (Button) findViewById(R.id.button_row5_8);

final LinearLayout row6 = (LinearLayout) findViewById(R.id.row6);
final Button button_row6_1 = (Button) findViewById(R.id.button_row6_1);
final Button button_row6_2 = (Button) findViewById(R.id.button_row6_2);
final Button button_row6_3 = (Button) findViewById(R.id.button_row6_3);
final Button button_row6_4 = (Button) findViewById(R.id.button_row6_4);
final Button button_row6_5 = (Button) findViewById(R.id.button_row6_5);
final Button button_row6_6 = (Button) findViewById(R.id.button_row6_6);
final Button button_row6_7 = (Button) findViewById(R.id.button_row6_7);
final Button button_row6_8 = (Button) findViewById(R.id.button_row6_8);

final LinearLayout row7 = (LinearLayout) findViewById(R.id.row7);
final Button button_row7_1 = (Button) findViewById(R.id.button_row7_1);
final Button button_row7_2 = (Button) findViewById(R.id.button_row7_2);
final Button button_row7_3 = (Button) findViewById(R.id.button_row7_3);
final Button button_row7_4 = (Button) findViewById(R.id.button_row7_4);
final Button button_row7_5 = (Button) findViewById(R.id.button_row7_5);
final Button button_row7_6 = (Button) findViewById(R.id.button_row7_6);
final Button button_row7_7 = (Button) findViewById(R.id.button_row7_7);
final Button button_row7_8 = (Button) findViewById(R.id.button_row7_8);

final LinearLayout row8 = (LinearLayout) findViewById(R.id.row8);
final Button button_row8_1 = (Button) findViewById(R.id.button_row8_1);
final Button button_row8_2 = (Button) findViewById(R.id.button_row8_2);
final Button button_row8_3 = (Button) findViewById(R.id.button_row8_3);
final Button button_row8_4 = (Button) findViewById(R.id.button_row8_4);
final Button button_row8_5 = (Button) findViewById(R.id.button_row8_5);
final Button button_row8_6 = (Button) findViewById(R.id.button_row8_6);
final Button button_row8_7 = (Button) findViewById(R.id.button_row8_7);
final Button button_row8_8 = (Button) findViewById(R.id.button_row8_8);

final TextView week1 = (TextView) findViewById(R.id.textView_week1);
final TextView week2 = (TextView) findViewById(R.id.textView_week2);
final TextView week3 = (TextView) findViewById(R.id.textView_week3);
final TextView week4 = (TextView) findViewById(R.id.textView_week4);
final TextView week5 = (TextView) findViewById(R.id.textView_week5);
final TextView week6 = (TextView) findViewById(R.id.textView_week6);
final TextView week7 = (TextView) findViewById(R.id.textView_week7);
final TextView week8 = (TextView) findViewById(R.id.textView_week8);

button_row8_8.setVisibility(View.INVISIBLE);



row2.setVisibility(View.INVISIBLE);
row3.setVisibility(View.INVISIBLE);
row4.setVisibility(View.INVISIBLE);
row5.setVisibility(View.INVISIBLE);
row6.setVisibility(View.INVISIBLE);
row7.setVisibility(View.INVISIBLE);
row8.setVisibility(View.INVISIBLE);

week2.setVisibility(View.INVISIBLE);
week3.setVisibility(View.INVISIBLE);
week4.setVisibility(View.INVISIBLE);
week5.setVisibility(View.INVISIBLE);
week6.setVisibility(View.INVISIBLE);
week7.setVisibility(View.INVISIBLE);
week8.setVisibility(View.INVISIBLE);


button_row1_1.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row1_1.isActivated()) {
button_row1_1.setActivated(false);
datesSelected[1][0] = 0;
button_row1_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row1_1.setActivated(true);
datesSelected[1][0] = 1;
button_row1_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row1_2.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row1_2.isActivated()) {
button_row1_2.setActivated(false);
datesSelected[1][1] = 0;
button_row1_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row1_2.setActivated(true);
datesSelected[1][1] = 1;
button_row1_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row1_3.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row1_3.isActivated()) {
button_row1_3.setActivated(false);
datesSelected[1][2] = 0;
button_row1_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row1_3.setActivated(true);
datesSelected[1][2] = 1;
button_row1_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row1_4.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row1_4.isActivated()) {
button_row1_4.setActivated(false);
datesSelected[1][3] = 0;
button_row1_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row1_4.setActivated(true);
datesSelected[1][3] = 1;
button_row1_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row1_5.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row1_5.isActivated()) {
button_row1_5.setActivated(false);
datesSelected[1][4] = 0;
button_row1_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row1_5.setActivated(true);
datesSelected[1][4] = 1;
button_row1_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row1_6.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row1_6.isActivated()) {
button_row1_6.setActivated(false);
datesSelected[1][5] = 0;
button_row1_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row1_6.setActivated(true);
datesSelected[1][5] = 1;
button_row1_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row1_7.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row1_7.isActivated()) {
button_row1_7.setActivated(false);
datesSelected[1][6] = 0;
button_row1_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row1_7.setActivated(true);
datesSelected[1][6] = 1;
button_row1_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row1_8.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row1_8.getText().equals("+")){
row2.setVisibility(View.VISIBLE);
button_row2_8.setText("+");
button_row1_8.setText("-");
datesSelected [0][0] = 1;
week2.setVisibility(View.VISIBLE);
} else if (button_row1_8.getText().equals("-")){
row2.setVisibility(View.INVISIBLE);
button_row2_8.setText("+");
button_row1_8.setText("+");
datesSelected [0][0] = 0;
week2.setVisibility(View.INVISIBLE);
}
}
});



button_row2_1.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row2_1.isActivated()) {
button_row2_1.setActivated(false);
datesSelected[2][0] = 0;
button_row2_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row2_1.setActivated(true);
datesSelected[2][0] = 1;
button_row2_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row2_2.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row2_2.isActivated()) {
button_row2_2.setActivated(false);
datesSelected[2][1] = 0;
button_row2_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row2_2.setActivated(true);
datesSelected[2][1] = 1;
button_row2_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row2_3.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row2_3.isActivated()) {
button_row2_3.setActivated(false);
datesSelected[2][2] = 0;
button_row2_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row2_3.setActivated(true);
datesSelected[2][2] = 1;
button_row2_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row2_4.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row2_4.isActivated()) {
button_row2_4.setActivated(false);
datesSelected[2][3] = 0;
button_row2_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row2_4.setActivated(true);
datesSelected[2][3] = 1;
button_row2_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row2_5.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row2_5.isActivated()) {
button_row2_5.setActivated(false);
datesSelected[2][4] = 0;
button_row2_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row2_5.setActivated(true);
datesSelected[2][4] = 1;
button_row2_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row2_6.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row2_6.isActivated()) {
button_row2_6.setActivated(false);
datesSelected[2][5] = 0;
button_row2_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row2_6.setActivated(true);
datesSelected[2][5] = 1;
button_row2_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row2_7.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row2_7.isActivated()) {
button_row2_7.setActivated(false);
datesSelected[2][6] = 0;
button_row2_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row2_7.setActivated(true);
datesSelected[2][6] = 1;
button_row2_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row2_8.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row2_8.getText().equals("+")){
row3.setVisibility(View.VISIBLE);
button_row3_8.setText("+");
button_row2_8.setText("-");
button_row1_8.setText("");
button_row1_8.setVisibility(View.INVISIBLE);
week3.setVisibility(View.VISIBLE);
datesSelected [0][0] = 2;
} else if (button_row2_8.getText().equals("-")){
row3.setVisibility(View.INVISIBLE);
button_row3_8.setText("+");
button_row2_8.setText("+");
button_row1_8.setText("-");
button_row1_8.setVisibility(View.VISIBLE);
week3.setVisibility(View.INVISIBLE);
datesSelected [0][0] = 1;
}
}
});



button_row3_1.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row3_1.isActivated()) {
button_row3_1.setActivated(false);
datesSelected[3][0] = 0;
button_row3_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row3_1.setActivated(true);
datesSelected[3][0] = 1;
button_row3_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row3_2.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row3_2.isActivated()) {
button_row3_2.setActivated(false);
datesSelected[3][1] = 0;
button_row3_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row3_2.setActivated(true);
datesSelected[3][1] = 1;
button_row3_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row3_3.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row3_3.isActivated()) {
button_row3_3.setActivated(false);
datesSelected[3][2] = 0;
button_row3_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row3_3.setActivated(true);
datesSelected[3][2] = 1;
button_row3_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row3_4.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row3_4.isActivated()) {
button_row3_4.setActivated(false);
datesSelected[3][3] = 0;
button_row3_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row3_4.setActivated(true);
datesSelected[3][3] = 1;
button_row3_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row3_5.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row3_5.isActivated()) {
button_row3_5.setActivated(false);
datesSelected[3][4] = 0;
button_row3_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row3_5.setActivated(true);
datesSelected[3][4] = 1;
button_row3_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row3_6.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row3_6.isActivated()) {
button_row3_6.setActivated(false);
datesSelected[3][5] = 0;
button_row3_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row3_6.setActivated(true);
datesSelected[3][5] = 1;
button_row3_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row3_7.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row3_7.isActivated()) {
button_row3_7.setActivated(false);
datesSelected[3][6] = 0;
button_row3_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row3_7.setActivated(true);
datesSelected[3][6] = 1;
button_row3_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});

button_row3_8.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row3_8.getText().equals("+")){
row4.setVisibility(View.VISIBLE);
button_row4_8.setText("+");
button_row3_8.setText("-");
button_row2_8.setText("");
button_row1_8.setText("");
button_row2_8.setVisibility(View.INVISIBLE);
week4.setVisibility(View.VISIBLE);
datesSelected [0][0] = 3;
} else if (button_row3_8.getText().equals("-")){
row4.setVisibility(View.INVISIBLE);
button_row4_8.setText("+");
button_row3_8.setText("+");
button_row2_8.setText("-");
button_row1_8.setText("");
button_row2_8.setVisibility(View.VISIBLE);
week4.setVisibility(View.INVISIBLE);
datesSelected [0][0] = 2;
}
}
});



button_row4_1.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row4_1.isActivated()) {
button_row4_1.setActivated(false);
datesSelected[4][0] = 0;
button_row4_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row4_1.setActivated(true);
datesSelected[4][0] = 1;
button_row4_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row4_2.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row4_2.isActivated()) {
button_row4_2.setActivated(false);
datesSelected[4][1] = 0;
button_row4_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row4_2.setActivated(true);
datesSelected[4][1] = 1;
button_row4_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row4_3.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row4_3.isActivated()) {
button_row4_3.setActivated(false);
datesSelected[4][2] = 0;
button_row4_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row4_3.setActivated(true);
datesSelected[4][2] = 1;
button_row4_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row4_4.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row4_4.isActivated()) {
button_row4_4.setActivated(false);
datesSelected[4][3] = 0;
button_row4_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row4_4.setActivated(true);
datesSelected[4][3] = 1;
button_row4_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row4_5.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row4_5.isActivated()) {
button_row4_5.setActivated(false);
datesSelected[4][4] = 0;
button_row4_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row4_5.setActivated(true);
datesSelected[4][4] = 1;
button_row4_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row4_6.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row4_6.isActivated()) {
button_row4_6.setActivated(false);
datesSelected[4][5] = 0;
button_row4_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row4_6.setActivated(true);
datesSelected[4][5] = 1;
button_row4_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row4_7.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row4_7.isActivated()) {
button_row4_7.setActivated(false);
datesSelected[4][6] = 0;
button_row4_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row4_7.setActivated(true);
datesSelected[4][6] = 1;
button_row4_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row4_8.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row4_8.getText().equals("+")){
row5.setVisibility(View.VISIBLE);
button_row5_8.setText("+");
button_row4_8.setText("-");
button_row3_8.setText("");
button_row2_8.setText("");
button_row1_8.setText("");
button_row3_8.setVisibility(View.INVISIBLE);
week5.setVisibility(View.VISIBLE);
datesSelected [0][0] = 4;
} else if (button_row4_8.getText().equals("-")){
row5.setVisibility(View.INVISIBLE);
button_row5_8.setText("+");
button_row4_8.setText("+");
button_row3_8.setText("-");
button_row2_8.setText("");
button_row1_8.setText("");
button_row3_8.setVisibility(View.VISIBLE);
week5.setVisibility(View.INVISIBLE);
datesSelected [0][0] = 3;
}
}
});


button_row5_1.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row5_1.isActivated()) {
button_row5_1.setActivated(false);
datesSelected[5][0] = 0;
button_row5_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row5_1.setActivated(true);
datesSelected[5][0] = 1;
button_row5_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row5_2.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row5_2.isActivated()) {
button_row5_2.setActivated(false);
datesSelected[5][1] = 0;
button_row5_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row5_2.setActivated(true);
datesSelected[5][1] = 1;
button_row5_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row5_3.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row5_3.isActivated()) {
button_row5_3.setActivated(false);
datesSelected[5][2] = 0;
button_row5_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row5_3.setActivated(true);
datesSelected[5][2] = 1;
button_row5_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row5_4.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row5_4.isActivated()) {
button_row5_4.setActivated(false);
datesSelected[5][3] = 0;
button_row5_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row5_4.setActivated(true);
datesSelected[5][3] = 1;
button_row5_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row5_5.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row5_5.isActivated()) {
button_row5_5.setActivated(false);
datesSelected[5][4] = 0;
button_row5_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row5_5.setActivated(true);
datesSelected[5][4] = 1;
button_row5_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row5_6.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row5_6.isActivated()) {
button_row5_6.setActivated(false);
datesSelected[5][5] = 0;
button_row5_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row5_6.setActivated(true);
datesSelected[5][5] = 1;
button_row5_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row5_7.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row5_7.isActivated()) {
button_row5_7.setActivated(false);
datesSelected[5][6] = 0;
button_row5_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row5_7.setActivated(true);
datesSelected[5][6] = 1;
button_row5_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});

button_row5_8.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row5_8.getText().equals("+")){
row6.setVisibility(View.VISIBLE);
button_row6_8.setText("+");
button_row5_8.setText("-");
button_row4_8.setText("");
button_row3_8.setText("");
button_row2_8.setText("");
button_row1_8.setText("");
button_row4_8.setVisibility(View.INVISIBLE);
week6.setVisibility(View.VISIBLE);
datesSelected [0][0] = 5;
} else if (button_row5_8.getText().equals("-")){
row6.setVisibility(View.INVISIBLE);
button_row6_8.setText("+");
button_row5_8.setText("+");
button_row4_8.setText("-");
button_row3_8.setText("");
button_row2_8.setText("");
button_row1_8.setText("");
button_row4_8.setVisibility(View.VISIBLE);
week6.setVisibility(View.INVISIBLE);
datesSelected [0][0] = 4;
}
}
});

button_row6_1.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row6_1.isActivated()) {
button_row6_1.setActivated(false);
datesSelected[6][0] = 0;
button_row6_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row6_1.setActivated(true);
datesSelected[6][0] = 1;
button_row6_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row6_2.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row6_2.isActivated()) {
button_row6_2.setActivated(false);
datesSelected[6][1] = 0;
button_row6_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row6_2.setActivated(true);
datesSelected[6][1] = 1;
button_row6_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row6_3.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row6_3.isActivated()) {
button_row6_3.setActivated(false);
datesSelected[6][2] = 0;
button_row6_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row6_3.setActivated(true);
datesSelected[6][2] = 1;
button_row6_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row6_4.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row6_4.isActivated()) {
button_row6_4.setActivated(false);
datesSelected[6][3] = 0;
button_row6_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row6_4.setActivated(true);
datesSelected[6][3] = 1;
button_row6_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row6_5.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row6_5.isActivated()) {
button_row6_5.setActivated(false);
datesSelected[6][4] = 0;
button_row6_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row6_5.setActivated(true);
datesSelected[6][4] = 1;
button_row6_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row6_6.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row6_6.isActivated()) {
button_row6_6.setActivated(false);
datesSelected[6][5] = 0;
button_row6_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row6_6.setActivated(true);
datesSelected[6][5] = 1;
button_row6_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row6_7.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row6_7.isActivated()) {
button_row6_7.setActivated(false);
datesSelected[6][6] = 0;
button_row6_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row6_7.setActivated(true);
datesSelected[6][6] = 1;
button_row6_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});

button_row6_8.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row6_8.getText().equals("+")){
row7.setVisibility(View.VISIBLE);
button_row7_8.setText("+");
button_row6_8.setText("-");
button_row5_8.setText("");
button_row4_8.setText("");
button_row3_8.setText("");
button_row2_8.setText("");
button_row1_8.setText("");
button_row5_8.setVisibility(View.INVISIBLE);
week7.setVisibility(View.VISIBLE);
datesSelected [0][0] = 6;
} else if (button_row6_8.getText().equals("-")){
row7.setVisibility(View.INVISIBLE);
button_row7_8.setText("+");
button_row6_8.setText("+");
button_row5_8.setText("-");
button_row4_8.setText("");
button_row3_8.setText("");
button_row2_8.setText("");
button_row1_8.setText("");
button_row5_8.setVisibility(View.VISIBLE);
week7.setVisibility(View.INVISIBLE);
datesSelected [0][0] = 5;
}
}
});

button_row7_1.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row7_1.isActivated()) {
button_row7_1.setActivated(false);
datesSelected[7][0] = 0;
button_row7_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row7_1.setActivated(true);
datesSelected[7][0] = 1;
button_row7_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row7_2.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row7_2.isActivated()) {
button_row7_2.setActivated(false);
datesSelected[7][1] = 0;
button_row7_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row7_2.setActivated(true);
datesSelected[7][1] = 1;
button_row7_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row7_3.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row7_3.isActivated()) {
button_row7_3.setActivated(false);
datesSelected[7][2] = 0;
button_row7_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row7_3.setActivated(true);
datesSelected[7][2] = 1;
button_row7_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row7_4.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row7_4.isActivated()) {
button_row7_4.setActivated(false);
datesSelected[7][3] = 0;
button_row7_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row7_4.setActivated(true);
datesSelected[7][3] = 1;
button_row7_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row7_5.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row7_5.isActivated()) {
button_row7_5.setActivated(false);
datesSelected[7][4] = 0;
button_row7_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row7_5.setActivated(true);
datesSelected[7][4] = 1;
button_row7_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row7_6.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row7_6.isActivated()) {
button_row7_6.setActivated(false);
datesSelected[7][5] = 0;
button_row7_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row7_6.setActivated(true);
datesSelected[7][5] = 1;
button_row7_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row7_7.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row7_7.isActivated()) {
button_row7_7.setActivated(false);
datesSelected[7][6] = 0;
button_row7_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row7_7.setActivated(true);
datesSelected[7][6] = 1;
button_row7_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});

button_row7_8.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row7_8.getText().equals("+")){
row8.setVisibility(View.VISIBLE);
button_row8_8.setText("");
button_row7_8.setText("-");
button_row6_8.setText("");
button_row5_8.setText("");
button_row4_8.setText("");
button_row3_8.setText("");
button_row2_8.setText("");
button_row1_8.setText("");
button_row6_8.setVisibility(View.INVISIBLE);
week8.setVisibility(View.VISIBLE);
datesSelected [0][0] = 7;
} else if (button_row7_8.getText().equals("-")){
row8.setVisibility(View.INVISIBLE);
button_row8_8.setText("+");
button_row7_8.setText("+");
button_row6_8.setText("-");
button_row5_8.setText("");
button_row4_8.setText("");
button_row3_8.setText("");
button_row2_8.setText("");
button_row1_8.setText("");
button_row6_8.setVisibility(View.VISIBLE);
week8.setVisibility(View.INVISIBLE);
datesSelected [0][0] = 6;
}
}
});

button_row8_1.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row8_1.isActivated()) {
button_row8_1.setActivated(false);
datesSelected[8][0] = 0;
button_row8_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row8_1.setActivated(true);
datesSelected[8][0] = 1;
button_row8_1.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row8_2.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row8_2.isActivated()) {
button_row8_2.setActivated(false);
datesSelected[8][1] = 0;
button_row8_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row8_2.setActivated(true);
datesSelected[8][1] = 1;
button_row8_2.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row8_3.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row8_3.isActivated()) {
button_row8_3.setActivated(false);
datesSelected[8][2] = 0;
button_row8_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row8_3.setActivated(true);
datesSelected[8][2] = 1;
button_row8_3.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row8_4.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row8_4.isActivated()) {
button_row8_4.setActivated(false);
datesSelected[8][3] = 0;
button_row8_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row8_4.setActivated(true);
datesSelected[8][3] = 1;
button_row8_4.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row8_5.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row8_5.isActivated()) {
button_row8_5.setActivated(false);
datesSelected[8][4] = 0;
button_row8_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row8_5.setActivated(true);
datesSelected[8][4] = 1;
button_row8_5.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row8_6.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row8_6.isActivated()) {
button_row8_6.setActivated(false);
datesSelected[8][5] = 0;
button_row8_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row8_6.setActivated(true);
datesSelected[8][5] = 1;
button_row8_6.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


button_row8_7.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (button_row8_7.isActivated()) {
button_row8_7.setActivated(false);
datesSelected[8][6] = 0;
button_row8_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.black));
} else {
button_row8_7.setActivated(true);
datesSelected[8][6] = 1;
button_row8_7.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
}
}
});


final Calendar c = Calendar.getInstance();
c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

Calendar cal = new GregorianCalendar();

cal = Calendar.getInstance();
cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);


SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");
String wk1 = formatter.format(cal);

Calendar cal2 = cal;
cal2.add(Calendar.DAY_OF_MONTH, 7);
String wk2 = formatter.format(cal2);

Calendar cal3 = cal;
cal3.add(Calendar.DAY_OF_MONTH, 7);
String wk3 = formatter.format(cal3);

Calendar cal4 = cal;
cal4.add(Calendar.DAY_OF_MONTH, 7);
String wk4 = formatter.format(cal4);

Calendar cal5 = cal;
cal5.add(Calendar.DAY_OF_MONTH, 7);
String wk5 = formatter.format(cal5);

Calendar cal6 = cal;
cal6.add(Calendar.DAY_OF_MONTH, 7);
String wk6 = formatter.format(cal6);

Calendar cal7 = cal;
cal7.add(Calendar.DAY_OF_MONTH, 7);
String wk7 = formatter.format(cal7);

Calendar cal8 = cal;
cal8.add(Calendar.DAY_OF_MONTH, 7);
String wk8 = formatter.format(cal8);

week1.setText("Week 1 - " + wk1) ;
week2.setText("Week 2 - " + wk2) ;
week3.setText("Week 3 - " + wk3) ;
week4.setText("Week 4 - " + wk4) ;
week5.setText("Week 5 - " + wk5) ;
week6.setText("Week 6 - " + wk6) ;
week7.setText("Week 7 - " + wk7) ;
week8.setText("Week 8 - " + wk8) ;
//

Button week_advance = (Button)findViewById(R.id.week_advance_button);
final Button week_decrease = (Button) findViewById(R.id.week_decrease_button);



week_decrease.setVisibility(View.INVISIBLE);

week_advance.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

datesSelected[0][1] = datesSelected[0][1] + 1;

if (datesSelected[0][1] > 0){
week_decrease.setVisibility(View.VISIBLE);
}

Calendar c = Calendar.getInstance();
c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

Calendar cal = new GregorianCalendar();

cal = Calendar.getInstance();
cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

cal.add(Calendar.DAY_OF_MONTH, (7 * datesSelected[0][1]));

SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");
String wk1 = formatter.format(cal);

Calendar cal2 = cal;
cal2.add(Calendar.DAY_OF_MONTH, 7);
String wk2 = formatter.format(cal2);

Calendar cal3 = cal;
cal3.add(Calendar.DAY_OF_MONTH, 7);
String wk3 = formatter.format(cal3);

Calendar cal4 = cal;
cal4.add(Calendar.DAY_OF_MONTH, 7);
String wk4 = formatter.format(cal4);

Calendar cal5 = cal;
cal5.add(Calendar.DAY_OF_MONTH, 7);
String wk5 = formatter.format(cal5);

Calendar cal6 = cal;
cal6.add(Calendar.DAY_OF_MONTH, 7);
String wk6 = formatter.format(cal6);

Calendar cal7 = cal;
cal7.add(Calendar.DAY_OF_MONTH, 7);
String wk7 = formatter.format(cal7);

Calendar cal8 = cal;
cal8.add(Calendar.DAY_OF_MONTH, 7);
String wk8 = formatter.format(cal8);

week1.setText("Week 1 - " + wk1) ;
week2.setText("Week 2 - " + wk2) ;
week3.setText("Week 3 - " + wk3) ;
week4.setText("Week 4 - " + wk4) ;
week5.setText("Week 5 - " + wk5) ;
week6.setText("Week 6 - " + wk6) ;
week7.setText("Week 7 - " + wk7) ;
week8.setText("Week 8 - " + wk8) ;
}
});

week_decrease.setOnClickListener(new View.OnClickListener() {
public void onClick(View v) {
// Perform action on click

if (datesSelected[0][1] > 0) {
datesSelected[0][1] = datesSelected[0][1] - 1;

if (datesSelected[0][1] < 1){
week_decrease.setVisibility(View.INVISIBLE);
}

Calendar c = Calendar.getInstance();
c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

Calendar cal = new GregorianCalendar();

cal = Calendar.getInstance();
cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

cal.add(Calendar.DAY_OF_MONTH, (7 * datesSelected[0][1]));

SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");
String wk1 = formatter.format(cal);

Calendar cal2 = cal;
cal2.add(Calendar.DAY_OF_MONTH, 7);
String wk2 = formatter.format(cal2);

Calendar cal3 = cal;
cal3.add(Calendar.DAY_OF_MONTH, 7);
String wk3 = formatter.format(cal3);

Calendar cal4 = cal;
cal4.add(Calendar.DAY_OF_MONTH, 7);
String wk4 = formatter.format(cal4);

Calendar cal5 = cal;
cal5.add(Calendar.DAY_OF_MONTH, 7);
String wk5 = formatter.format(cal5);

Calendar cal6 = cal;
cal6.add(Calendar.DAY_OF_MONTH, 7);
String wk6 = formatter.format(cal6);

Calendar cal7 = cal;
cal7.add(Calendar.DAY_OF_MONTH, 7);
String wk7 = formatter.format(cal7);

Calendar cal8 = cal;
cal8.add(Calendar.DAY_OF_MONTH, 7);
String wk8 = formatter.format(cal8);

week1.setText("Week 1 - " + wk1);
week2.setText("Week 2 - " + wk2);
week3.setText("Week 3 - " + wk3);
week4.setText("Week 4 - " + wk4);
week5.setText("Week 5 - " + wk5);
week6.setText("Week 6 - " + wk6);
week7.setText("Week 7 - " + wk7);
week8.setText("Week 8 - " + wk8);
}

NumberPicker numberPicker = (NumberPicker) findViewById(R.id.numberPicker);
numberPicker.setMinValue(1);

// find max number of repeats in 2 years

int period = datesSelected[0][0] + 1;

numberPicker.setMaxValue((int) Math.floor(104 / period));

}
});



// repeat section

TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);

int hour = timePicker.getHour();
int minute = timePicker.getMinute();

TextView summaryTime = (TextView) findViewById(R.id.textView_time);
TextView summary = (TextView) findViewById(R.id.textView_summary);
TextView summaryStart = (TextView) findViewById(R.id.textView_summary2);
final TextView summaryEnd = (TextView) findViewById(R.id.textView_summary3);

summaryTime.setText("Alarm time - " + String.format("%02d", hour) + ":" + String.format("%02d", minute));

int numberOfWeeks = datesSelected[0][0];

final NumberPicker numberPicker = (NumberPicker) findViewById(R.id.numberPicker);

switch(numberOfWeeks) {

case 0 :
summary.setText("Single week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 1 :
summary.setText("Two week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 2 :
summary.setText("Three week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 3 :
summary.setText("Four week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 4 :
summary.setText("Five week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 5 :
summary.setText("Six week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 6 :
summary.setText("Seven week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
case 7 :
summary.setText("Eight week pattern");
summaryStart.setText("beginning: " + week1.getText().toString().substring(9));
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
break;
default :
System.out.println("Invalid grade");
break;
}

final TextView numberRepeats = (TextView) findViewById(R.id.numberPickerText);
final TextView repeatUntilDate = (TextView) findViewById(R.id.numberPickerText2);

numberPicker.setMinValue(1);

// find max number of repeats in 2 years

int period = datesSelected[0][0] + 1;

numberPicker.setMaxValue((int) Math.floor(104 / period));




final String [] dates = new String [(730 - (7 * datesSelected[0][1]))];

new GregorianCalendar();
Calendar cal_days = Calendar.getInstance();


cal_days.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

cal_days.add(Calendar.DAY_OF_MONTH, (7 * datesSelected[0][1]));

SimpleDateFormat formatter2 = new SimpleDateFormat("d MMM yyyy, EEE");


for (int i = 0; i < (730 - (7 * datesSelected[0][1])); i = i + 1) {

String day = formatter2.format(cal_days);

dates[i] = day;
cal_days.add(Calendar.DAY_OF_MONTH, 1);

}





final NumberPicker numberPickerDays = (NumberPicker) findViewById(R.id.numberPicker2);


numberPickerDays.setMinValue(0);
numberPickerDays.setMaxValue(dates.length - 1);

numberPickerDays.setDisplayedValues(dates);

repeatUntilDate.setText("Repeat until " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()) + " (max duration span is 2 years)");


numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
@Override
public void onValueChange(NumberPicker numberPicker, final int i, final int i2) {


LinearLayout picker1Box = (LinearLayout) findViewById(R.id.picker1_box);
LinearLayout picker2Box = (LinearLayout) findViewById(R.id.picker2_box);

picker1Box.setBackgroundResource(R.drawable.button_border_red);
picker2Box.setBackgroundResource(R.drawable.button_border);

if (i2 == 1) {
numberRepeats.setText("Add a single occurrance (maximum duration span is 2 years)");
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
} else if (i2 == 2){
numberRepeats.setText("Repeat an extra " + Integer.toString(i2 - 1) + " time (maximum duration span is 2 years)");


summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
}

else {
numberRepeats.setText("Repeat an extra " + Integer.toString(i2 - 1) + " times (maximum duration span is 2 years)");
summaryEnd.setText("ending: " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()));
}
repeatUntilDate.setText("Repeat until " + returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue()) + " (max duration span is 2 years)");

int arrayIndex = Arrays.asList(dates).indexOf(changeDateFormatReverse(returnEndDate(week1.getText().toString().substring(9), datesSelected[0][0], numberPicker.getValue())));

numberPickerDays.setValue(arrayIndex);

}
});



numberPickerDays.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
@Override
public void onValueChange(NumberPicker numberPickerDays, final int i, final int i2) {

summaryEnd.setText("ending: " + changeDateFormat(dates[i2]));
repeatUntilDate.setText("Repeat until " + changeDateFormat(dates[i2]) + " (max duration span is 2 years)");

int numberOfDaysTillTheDate = i2 + 1;

int numberOfDaysInShift = 7 * (datesSelected[0][0] + 1);


int repeat = (int) Math.floor(numberOfDaysTillTheDate / numberOfDaysInShift);

if (repeat == 0){
repeat = 1;
}
numberPicker.setValue(repeat);

LinearLayout picker1Box = (LinearLayout) findViewById(R.id.picker1_box);
LinearLayout picker2Box = (LinearLayout) findViewById(R.id.picker2_box);

picker1Box.setBackgroundResource(R.drawable.button_border);
picker2Box.setBackgroundResource(R.drawable.button_border_red);

}
});


Button confirmButton = (Button) findViewById(R.id.confirm_button);

confirmButton.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

showPopup(findViewById(R.id.content), datesSelected);

}
});

Button resetButton = (Button) findViewById(R.id.reset_button);

resetButton.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {



}
});

}

@Override
public void onNothingSelected(AdapterView<?> parentView) {
// your code here



}


});


Switch vibrateSwitch = (Switch) findViewById(R.id.switch_vibrate);

vibrateSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
@Override
public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
// TODO Auto-generated method stub
if (buttonView.isChecked()) {
//do something   }
Vibrator v = (Vibrator) getSystemService(getApplicationContext().VIBRATOR_SERVICE);
// Vibrate for 500 milliseconds
v.vibrate(500);
} else {
//do something
}
}

});

}


public List<AlarmTone> getAlarmTones() {

List<AlarmTone> alarmTonesList = new ArrayList();

RingtoneManager manager = new RingtoneManager(this);
manager.setType(RingtoneManager.TYPE_ALARM);
Cursor cursor = manager.getCursor();

SeekBar seekVolume = (SeekBar) findViewById(R.id.seekBarVolume);


// get alarm volume

int MAX_VOLUME = 100;

int soundVolume = seekVolume.getProgress() * 10;

final float volume = (float) (1 - (Math.log(MAX_VOLUME - soundVolume) / Math.log(MAX_VOLUME)));

while (cursor.moveToNext()) {
String id = cursor.getString(RingtoneManager.ID_COLUMN_INDEX);
String uri = cursor.getString(RingtoneManager.URI_COLUMN_INDEX);
String title = cursor.getString(RingtoneManager.TITLE_COLUMN_INDEX);


AlarmTone alarmTone = new AlarmTone(id, title, uri, soundVolume);

alarmTonesList.add(alarmTone);
}

return alarmTonesList;
}




private void makeToastMessage(String toastMessage) {

Spannable centeredText = new SpannableString(toastMessage);
centeredText.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
0, toastMessage.length() - 1,
Spannable.SPAN_INCLUSIVE_INCLUSIVE);

Toast.makeText(getApplicationContext(), centeredText, Toast.LENGTH_LONG).show();

}

private void makeToastMessageShort(String toastMessage) {

Spannable centeredText = new SpannableString(toastMessage);
centeredText.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER),
0, toastMessage.length() - 1,
Spannable.SPAN_INCLUSIVE_INCLUSIVE);

Toast.makeText(getApplicationContext(), centeredText, Toast.LENGTH_SHORT).show();

}

private String returnEndDate(String startDate, int shiftLengthInWeeks, int numberOfRepeats) {

String weekEndingSunday = "";

SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy");
try {
Date endDate = dateFormat.parse(startDate);
Calendar calendar = Calendar.getInstance();
calendar.setTime(endDate);

calendar.add(Calendar.DAY_OF_YEAR, -1);
calendar.add(Calendar.DAY_OF_YEAR, (7 * ((shiftLengthInWeeks + 1) * numberOfRepeats)));

   weekEndingSunday = dateFormat.format(calendar);
} catch (ParseException e) {
e.printStackTrace();
}

return weekEndingSunday;
}

private String changeDateFormat(String date) {

String convertedDate = "";

SimpleDateFormat formatterStart = new SimpleDateFormat("d MMM yyyy, EEE");

SimpleDateFormat formatterReturn = new SimpleDateFormat("EEE, d MMM yyyy");
try {
Date thisDate = formatterStart.parse(date);
Calendar calendar = Calendar.getInstance();
calendar.setTime(thisDate);

convertedDate = formatterReturn.format(calendar);
} catch (ParseException e) {
e.printStackTrace();
}

return convertedDate;
}

private String changeDateFormatReverse(String date) {

String convertedDate = "";

SimpleDateFormat formatterReturn = new SimpleDateFormat("d MMM yyyy, EEE");

SimpleDateFormat formatterStart = new SimpleDateFormat("EEE, d MMM yyyy");
try {
Date thisDate = formatterStart.parse(date);
Calendar calendar = Calendar.getInstance();
calendar.setTime(thisDate);

convertedDate = formatterReturn.format(calendar);
} catch (ParseException e) {
e.printStackTrace();
}

return convertedDate;
}

public void showPopup(View anchorView, final int datesSelected [][]) {


final View popupView = getLayoutInflater().inflate(R.layout.popup, null, false);

final PopupWindow popupWindow = new PopupWindow(
popupView, RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
popupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);

// Initialize more widgets from `popup_layout.xml`


// If the PopupWindow should be focusable
popupWindow.setFocusable(true);
popupWindow.setOutsideTouchable(false);

// If you need the PopupWindow to dismiss when when touched outside
popupWindow.setBackgroundDrawable(new ColorDrawable());


TimePicker timePicker = anchorView.findViewById(R.id.timePicker);

int hour = timePicker.getHour();
int minute = timePicker.getMinute();


//
//private int datesSelected [] [] = {
//{0,0}, // rows shown, week advance
//{0,0,0,0,0,0,0}, // row 1 buttons
//{0,0,0,0,0,0,0}, // row 2 buttons
//{0,0,0,0,0,0,0}, // row 3 buttons
//{0,0,0,0,0,0,0}, // row 4 buttons
//{0,0,0,0,0,0,0}, // row 5 buttons
//{0,0,0,0,0,0,0}, // row 6 buttons
//{0,0,0,0,0,0,0}, // row 7 buttons
//{0,0,0,0,0,0,0} // row 8 buttons
//};
//




TextView  timeText = popupView.findViewById(R.id.popup_text1);

timeText.setText("Alarm time - " + String.format("%02d", hour) + ":" + String.format("%02d", minute));


TextView  week1Text = popupView.findViewById(R.id.popup_text2);

String week1TextString = "Week 1  -  ";

if (datesSelected[1][0] == 1){

  week1TextString = week1TextString + "Mon ";
   } else {
week1TextString = week1TextString + "";
}

if (datesSelected[1][1] == 1){

week1TextString = week1TextString + "Tue ";
} else {
week1TextString = week1TextString + "";
}

if (datesSelected[1][2] == 1){

week1TextString = week1TextString + "Wed ";
} else {
week1TextString = week1TextString + "";
}

if (datesSelected[1][3] == 1){

week1TextString = week1TextString + "Thu ";
} else {
week1TextString = week1TextString + "";
}

if (datesSelected[1][4] == 1){

week1TextString = week1TextString + "Fri ";
} else {
week1TextString = week1TextString + "";
}

if (datesSelected[1][5] == 1){

week1TextString = week1TextString + "Sat ";
} else {
week1TextString = week1TextString + "";
}

if (datesSelected[1][6] == 1){

week1TextString = week1TextString + "Sun ";
} else {
week1TextString = week1TextString + "";
}

if (datesSelected[0][0] >= 0) {
week1Text.setText(week1TextString);
}

TextView  week2Text = popupView.findViewById(R.id.popup_text3);

String week2TextString = "Week 2  -  ";

if (datesSelected[2][0] == 1){

week2TextString = week2TextString + "Mon ";
} else {
week2TextString = week2TextString + "";
}

if (datesSelected[2][1] == 1){

week2TextString = week2TextString + "Tue ";
} else {
week2TextString = week2TextString + "";
}

if (datesSelected[2][2] == 1){

week2TextString = week2TextString + "Wed ";
} else {
week2TextString = week2TextString + "";
}

if (datesSelected[2][3] == 1){

week2TextString = week2TextString + "Thu ";
} else {
week2TextString = week2TextString + "";
}

if (datesSelected[2][4] == 1){

week2TextString = week2TextString + "Fri ";
} else {
week2TextString = week2TextString + "";
}

if (datesSelected[2][5] == 1){

week2TextString = week2TextString + "Sat ";
} else {
week2TextString = week2TextString + "";
}

if (datesSelected[2][6] == 1){

week2TextString = week2TextString + "Sun ";
} else {
week2TextString = week2TextString + "";
}

if (datesSelected[0][0] >= 1) {
week2Text.setText(week2TextString);
}




TextView  week3Text = popupView.findViewById(R.id.popup_text4);

String week3TextString = "Week 3  -  ";

if (datesSelected[3][0] == 1){

week3TextString = week3TextString + "Mon ";
} else {
week3TextString = week3TextString + "";
}

if (datesSelected[3][1] == 1){

week3TextString = week3TextString + "Tue ";
} else {
week3TextString = week3TextString + "";
}

if (datesSelected[3][2] == 1){

week3TextString = week3TextString + "Wed ";
} else {
week3TextString = week3TextString + "";
}

if (datesSelected[3][3] == 1){

week3TextString = week3TextString + "Thu ";
} else {
week3TextString = week3TextString + "";
}

if (datesSelected[3][4] == 1){

week3TextString = week3TextString + "Fri ";
} else {
week3TextString = week3TextString + "";
}

if (datesSelected[3][5] == 1){

week3TextString = week3TextString + "Sat ";
} else {
week3TextString = week3TextString + "";
}

if (datesSelected[3][6] == 1){

week3TextString = week3TextString + "Sun ";
} else {
week3TextString = week3TextString + "";
}

if (datesSelected[0][0] >= 2) {
week3Text.setText(week3TextString);
}



TextView  week4Text = popupView.findViewById(R.id.popup_text5);

String week4TextString = "Week 4  -  ";

if (datesSelected[4][0] == 1){

week4TextString = week4TextString + "Mon ";
} else {
week4TextString = week4TextString + "";
}

if (datesSelected[4][1] == 1){

week4TextString = week4TextString + "Tue ";
} else {
week4TextString = week4TextString + "";
}

if (datesSelected[4][2] == 1){

week4TextString = week4TextString + "Wed ";
} else {
week4TextString = week4TextString + "";
}

if (datesSelected[4][3] == 1){

week4TextString = week4TextString + "Thu ";
} else {
week4TextString = week4TextString + "";
}

if (datesSelected[4][4] == 1){

week4TextString = week4TextString + "Fri ";
} else {
week4TextString = week4TextString + "";
}

if (datesSelected[4][5] == 1){

week4TextString = week4TextString + "Sat ";
} else {
week4TextString = week4TextString + "";
}

if (datesSelected[4][6] == 1){

week4TextString = week4TextString + "Sun ";
} else {
week4TextString = week4TextString + "";
}

if (datesSelected[0][0] >= 3) {
week4Text.setText(week4TextString);
}

TextView  week5Text = popupView.findViewById(R.id.popup_text6);

String week5TextString = "Week 5  -  ";

if (datesSelected[5][0] == 1){

week5TextString = week5TextString + "Mon ";
} else {
week5TextString = week5TextString + "";
}

if (datesSelected[5][1] == 1){

week5TextString = week5TextString + "Tue ";
} else {
week5TextString = week5TextString + "";
}

if (datesSelected[5][2] == 1){

week5TextString = week5TextString + "Wed ";
} else {
week5TextString = week5TextString + "";
}

if (datesSelected[5][3] == 1){

week5TextString = week5TextString + "Thu ";
} else {
week5TextString = week5TextString + "";
}

if (datesSelected[5][4] == 1){

week5TextString = week5TextString + "Fri ";
} else {
week5TextString = week5TextString + "";
}

if (datesSelected[5][5] == 1){

week5TextString = week5TextString + "Sat ";
} else {
week5TextString = week5TextString + "";
}

if (datesSelected[5][6] == 1){

week5TextString = week5TextString + "Sun ";
} else {
week5TextString = week5TextString + "";
}

if (datesSelected[0][0] >= 4) {
week5Text.setText(week5TextString);
}


TextView  week6Text = popupView.findViewById(R.id.popup_text7);

String week6TextString = "Week 6  -  ";

if (datesSelected[6][0] == 1){

week6TextString = week6TextString + "Mon ";
} else {
week6TextString = week6TextString + "";
}

if (datesSelected[6][1] == 1){

week6TextString = week6TextString + "Tue ";
} else {
week6TextString = week6TextString + "";
}

if (datesSelected[6][2] == 1){

week6TextString = week6TextString + "Wed ";
} else {
week6TextString = week6TextString + "";
}

if (datesSelected[6][3] == 1){

week6TextString = week6TextString + "Thu ";
} else {
week6TextString = week6TextString + "";
}

if (datesSelected[6][4] == 1){

week6TextString = week6TextString + "Fri ";
} else {
week6TextString = week6TextString + "";
}

if (datesSelected[6][5] == 1){

week6TextString = week6TextString + "Sat ";
} else {
week6TextString = week6TextString + "";
}

if (datesSelected[6][6] == 1){

week6TextString = week6TextString + "Sun ";
} else {
week6TextString = week6TextString + "";
}

if (datesSelected[0][0] >= 5) {
week6Text.setText(week6TextString);
}

TextView  week7Text = popupView.findViewById(R.id.popup_text8);

String week7TextString = "Week 7  -  ";

if (datesSelected[7][0] == 1){

week7TextString = week7TextString + "Mon ";
} else {
week7TextString = week7TextString + "";
}

if (datesSelected[7][1] == 1){

week7TextString = week7TextString + "Tue ";
} else {
week7TextString = week7TextString + "";
}

if (datesSelected[7][2] == 1){

week7TextString = week7TextString + "Wed ";
} else {
week7TextString = week7TextString + "";
}

if (datesSelected[7][3] == 1){

week7TextString = week7TextString + "Thu ";
} else {
week7TextString = week7TextString + "";
}

if (datesSelected[7][4] == 1){

week7TextString = week7TextString + "Fri ";
} else {
week7TextString = week7TextString + "";
}

if (datesSelected[7][5] == 1){

week7TextString = week7TextString + "Sat ";
} else {
week7TextString = week7TextString + "";
}

if (datesSelected[7][6] == 1){

week7TextString = week7TextString + "Sun ";
} else {
week7TextString = week7TextString + "";
}

if (datesSelected[0][0] >= 6) {
week7Text.setText(week7TextString);
}

TextView  week8Text = popupView.findViewById(R.id.popup_text9);

    String week8TextString = "Week 8  -  ";

    if (datesSelected[8][0] == 1){

        week8TextString = week8TextString + "Mon ";
    } else {
        week8TextString = week8TextString + "";
    }

    if (datesSelected[8][1] == 1){

        week8TextString = week8TextString + "Tue ";
    } else {
        week8TextString = week8TextString + "";
    }

    if (datesSelected[8][2] == 1){

        week8TextString = week8TextString + "Wed ";
    } else {
        week8TextString = week8TextString + "";
    }

    if (datesSelected[8][3] == 1){

        week8TextString = week8TextString + "Thu ";
    } else {
        week8TextString = week8TextString + "";
    }

    if (datesSelected[8][4] == 1){

        week8TextString = week8TextString + "Fri ";
    } else {
        week8TextString = week8TextString + "";
    }

    if (datesSelected[8][5] == 1){

        week8TextString = week8TextString + "Sat ";
    } else {
        week8TextString = week8TextString + "";
    }

    if (datesSelected[8][6] == 1){

        week8TextString = week8TextString + "Sun ";
    } else {
        week8TextString = week8TextString + "";
    }

    if (datesSelected[0][0] >= 7) {
        week8Text.setText(week8TextString);
    }


TextView  repeatText = popupView.findViewById(R.id.popup_text10);


Button no = popupView.findViewById(R.id.popup_no);

no.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View v) {

popupWindow.dismiss();

}
});


}



}