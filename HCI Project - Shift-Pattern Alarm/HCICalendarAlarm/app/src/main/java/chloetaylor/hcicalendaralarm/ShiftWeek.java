package chloetaylor.hcicalendaralarm;

/**
 * Created by Chloe on 24/10/2017.
 */

public class ShiftWeek {

    int weekDate;
    int shiftPatternIndex;
    boolean monday;
    boolean tuesday;
    boolean wednesday;
    boolean thursday;
    boolean friday;
    boolean saturday;
    boolean sunday;


    ShiftWeek (int weekDate, int shiftPatternIndex, boolean monday, boolean tuesday, boolean wednesday, boolean thursday, boolean friday, boolean saturday, boolean sunday){

        this.shiftPatternIndex = shiftPatternIndex;
        this.weekDate = weekDate;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
        this.saturday = saturday;
         this.sunday = sunday;


    }

    public int getWeekDate() {
        return weekDate;
    }

    public void setWeekDate(int weekDate) {
        this.weekDate = weekDate;
    }

    public int getShiftPatternIndex() {
        return shiftPatternIndex;
    }

    public void setShiftPatternIndex(int shiftPatternIndex) {
        this.shiftPatternIndex = shiftPatternIndex;
    }

    public boolean isMonday() {
        return monday;
    }

    public void setMonday(boolean monday) {
        this.monday = monday;
    }

    public boolean isTuesday() {
        return tuesday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday = tuesday;
    }

    public boolean isWednesday() {
        return wednesday;
    }

    public void setWednesday(boolean wednesday) {
        this.wednesday = wednesday;
    }

    public boolean isThursday() {
        return thursday;
    }

    public void setThursday(boolean thursday) {
        this.thursday = thursday;
    }

    public boolean isFriday() {
        return friday;
    }

    public void setFriday(boolean friday) {
        this.friday = friday;
    }

    public boolean isSaturday() {
        return saturday;
    }

    public void setSaturday(boolean saturday) {
        this.saturday = saturday;
    }

    public boolean isSunday() {
        return sunday;
    }

    public void setSunday(boolean sunday) {
        this.sunday = sunday;
    }
}
