package chloetaylor.hcicalendaralarm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chloe on 24/10/2017.
 */

public class Alarm {

    private int index;
    private String label;
    private AlarmTone alarmTone;
    private List<ShiftWeek> shiftWeekList = new ArrayList<>();
    private int numberOfInstancesOfShiftPattern = 1;
    private boolean vibrate;


    Alarm (int index, String label, AlarmTone alarmTone, int numberOfInstancesOfShiftPattern, boolean vibrate){

        this.index = index;
        this.alarmTone = alarmTone;
        this.numberOfInstancesOfShiftPattern = numberOfInstancesOfShiftPattern;
        this.label = label;
        this.vibrate = vibrate;


    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public AlarmTone getAlarmTone() {
        return alarmTone;
    }

    public void setAlarmTone(AlarmTone alarmTone) {
        this.alarmTone = alarmTone;
    }


    public List<ShiftWeek> getShiftWeekList() {
        return shiftWeekList;
    }


    public void addShiftWeekList(ShiftWeek shiftWeek) {
        shiftWeekList.add(shiftWeek);
    }

    public void removeShiftWeekList(int index) {
        for (ShiftWeek shiftWeek : this.shiftWeekList) {
            if (shiftWeek.getShiftPatternIndex() == index){
                shiftWeekList.remove(index);
            }
        }
    }

    public int getNumberOfInstancesOfShiftPattern() {
        return numberOfInstancesOfShiftPattern;
    }

    public void setNumberOfInstancesOfShiftPattern(int numberOfInstancesOfShiftPattern) {
        this.numberOfInstancesOfShiftPattern = numberOfInstancesOfShiftPattern;
    }
}
