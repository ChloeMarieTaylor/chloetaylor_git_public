# Import necessary packages
from __future__ import print_function

import copy

try:
    from Tkinter import *
except ImportError:
    from tkinter import *

try:
    import ttk

    py3 = False
except ImportError:
    import tkinter.ttk as ttk

    py3 = True

import os
import sys
import numpy as np
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw


def average_img(datamatrix):
    # method using numpy mean function

    x = np.array(datamatrix)
    out = x.mean(axis=0)
    return out


# Create data matrix from a list of images
def createDataMatrix(images):
    print("-----------------------------------------------\nCreating data matrix", end=" ... ")
    ''' 
    Allocate space for all images in one data matrix.
  The size of the data matrix is
  
  ( w  * h  * 3, numImages )
  
  where,
  
  w = width of an image in the dataset.
  h = height of an image in the dataset.
  3 is for the 3 color channels.
  '''

    numImages = len(images)
    sz = images[0].shape
    print (sz)
    data = np.zeros((numImages, sz[0] * sz[1] * sz[2]), dtype=np.float32)
    for i in xrange(0, numImages):
        image2 = images[i].flatten()
        data[i, :] = image2

    print("data matrix created... ")
    return data


# Read images from the directory
def readImages(path):
    print("Reading images from " + path, end="...")
    # Create array of array of images.
    images = []
    # List all files in the directory and read points from text files one by one
    for filePath in sorted(os.listdir(path)):
        fileExt = os.path.splitext(filePath)[1]
        if fileExt in [".jpg", ".jpeg"]:

            # Add to array of images
            imagePath = os.path.join(path, filePath)
            im = Image.open(imagePath)

            if im is None:
                print("image:{} not read properly".format(imagePath))
            else:
                filesnames.append(imagePath)
                # Convert image to floating point
                im = np.float32(im) / 255.0
                # Add image to list
                images.append(im)

    numImages = len(images)
    # Exit if no image found
    if numImages == 0:
        print("No images found")
        sys.exit(0)

    print(str(numImages) + " files read.")
    return images


if __name__ == '__main__':

    filesnames = []

    directoryPath = "results_faces"
    eigenfacedirectory = "results_eigenfaces_faces"
    differencefacesdirectory = "results_difference_faces_faces"

    if not os.path.exists(directoryPath):
        os.makedirs(directoryPath)

    if not os.path.exists(eigenfacedirectory):
        os.makedirs(eigenfacedirectory)

    if not os.path.exists(differencefacesdirectory):
        os.makedirs(differencefacesdirectory)

    # Directory containing images
    dirName = "faces"

    # Read images
    images = readImages(dirName)

    # Size of images
    sz = images[0].shape

    workLoadCount = 0

    alldata = createDataMatrix(images)

    allmean = average_img(alldata)

    allaverageFace = allmean.reshape(sz)
    allaverageFaceImage = Image.fromarray(np.uint8(allaverageFace * 255))
    allaverageFaceImage.save(eigenfacedirectory + "\\" + "averageface.png")

    alldata2 = alldata - np.mean(alldata, axis=0)

    cov = np.cov(alldata2)

    eigenvalues, eigenvectors = np.linalg.eig(cov)

    eigenFaces = np.dot(eigenvectors, alldata2)

    imagecount = 0
    for data in eigenFaces:
        eigenface = data.reshape(sz)
        eigenfaceImage = Image.fromarray(np.uint8(eigenface * 255)).convert('LA')
        eigenfilename = "eigenface_" + filesnames[imagecount]
        eigenfilename = eigenfilename.replace('.jpg', '.png')
        eigenfilename = eigenfilename.replace(dirName + '\\', '')
        eigenfaceImage.save(eigenfacedirectory + "\\" + eigenfilename)
        imagecount = imagecount + 1

    alldata = alldata - np.mean(alldata, axis=0)

    imagecount = 0
    for data in alldata:
        deviation = np.subtract(data, allmean)
        differenceface = deviation.reshape(sz)
        differencefaceImage = Image.fromarray(np.uint8(differenceface * 255)).convert('LA')
        differencefacefilename = "differenceface_" + filesnames[imagecount]
        differencefacefilename = differencefacefilename.replace('.jpg', '.png')
        differencefacefilename = differencefacefilename.replace(dirName + '\\', '')
        differencefaceImage.save(differencefacesdirectory + "\\" + differencefacefilename)
        imagecount = imagecount + 1

    allbestScores = []
    allmeanScores = []

    for image in images:

        print("\nWorking... image " + filesnames[workLoadCount] + "  (" + str(workLoadCount + 1) + " / " +
              str(len(images)) + ")")

        resultfilename = "result_" + filesnames[workLoadCount]
        textfilename = resultfilename.replace('.jpg', '.csv')
        textfilename = textfilename.replace(dirName + '\\', '')
        resultfilename = resultfilename.replace('.jpg', '.png')
        resultfilename = resultfilename.replace(dirName + '\\', '')

        print(textfilename)

        workingImages = []
        workingImages = copy.deepcopy(images)

        testImage = workingImages[workLoadCount]
        testImagePath = filesnames[workLoadCount]
        del workingImages[workLoadCount]

        workingFileNames = []
        workingFileNames = copy.deepcopy(filesnames)
        del workingFileNames[workLoadCount]

        # Create data matrix for PCA.
        data = createDataMatrix(workingImages)

        mean = average_img(data)

        averageFace = mean.reshape(sz)

        differencefaces = []

        for workingImage in workingImages:
            output = np.subtract(workingImage, averageFace)
            differencefaces.append(output)

        finaltestimage = np.subtract(testImage, averageFace)

        closestmatchlist = []

        allScores = []

        index = 0;
        for differentimage in differencefaces:
            distance = np.linalg.norm(finaltestimage - differentimage)
            imagePath = workingFileNames[index]
            closestmatchtuple = (str(imagePath), abs(distance))
            closestmatchlist.append(closestmatchtuple)
            allScores.append(abs(distance))
            index = index + 1

        closestmatchlist = sorted(closestmatchlist, key=lambda x: x[1])
        allScores = sorted(allScores)

        outtextFile = open(directoryPath + "\\" + textfilename, "w")

        # begin csv file
        outtextFile.write("\"image name\",\"distance score\"\n")

        for closestmatch in closestmatchlist:
            index2 = closestmatch[0]
            print("image: " + str(closestmatch[0]) + "    distance: " + str(closestmatch[1]))
            outtextFile.write(str(closestmatch[0]) + "," + str(closestmatch[1]) + "\n")

        outtextFile.close()

        bestResult = closestmatchlist[0]

        # font = ImageFont.truetype(<font-file>, <font-size>)
        # font-file should be present in provided path.
        font = ImageFont.truetype("sans-serif.ttf", 20)

        template = Image.open('template.png')

        draw = ImageDraw.Draw(template)
        # draw.text((x, y),"Sample Text",(r,g,b))
        draw.text((670, 400), testImagePath, (0, 0, 0), font=font)

        detailsString = "Best Score: " + str(allScores[0]) + ",\n" + \
                        "Worst Score: " + str(allScores[len(allScores) - 1]) + ",\n" + \
                        "Mean Score: " + str(np.mean(allScores)) + ",\n" + \
                        "Std. Deviation: " + str(np.std(allScores))

        allbestScores.append(allScores[0])
        allmeanScores.append(np.mean(allScores))

        print("-----------------------------------------------\nBest match: " +
              str(bestResult[0]) + "     score: " + str(bestResult[1]))
        print("Population mean: " + str(np.mean(allmeanScores)) + "\n\n\n")

        draw.text((940, 120), detailsString, (0, 0, 0), font=font)

        testImageOriginal = Image.open(str(testImagePath))
        template.paste(testImageOriginal, (643, 63))

        bestMatchOriginalPath = closestmatchlist[0][0]
        bestMatchOriginal = Image.open(str(bestMatchOriginalPath))
        template.paste(bestMatchOriginal, (214, 543))
        draw2 = ImageDraw.Draw(template)
        draw2.text((241, 880), str(bestMatchOriginalPath), (0, 0, 0), font=font)
        bestMatchOriginalScore = closestmatchlist[0][1]
        draw2.text((241, 910), "distance: " + str(bestMatchOriginalScore), (0, 0, 0), font=font)

        secondBestMatchOriginalPath = closestmatchlist[1][0]
        secondBestMatchOriginal = Image.open(str(secondBestMatchOriginalPath))
        template.paste(secondBestMatchOriginal, (520, 543))
        draw3 = ImageDraw.Draw(template)
        draw3.text((547, 880), str(secondBestMatchOriginalPath), (0, 0, 0), font=font)
        secondBestMatchOriginalScore = closestmatchlist[1][1]
        draw3.text((547, 910), "distance: " + str(secondBestMatchOriginalScore), (0, 0, 0), font=font)

        thirdBestMatchOriginalPath = closestmatchlist[2][0]
        thirdBestMatchOriginal = Image.open(str(thirdBestMatchOriginalPath))
        template.paste(thirdBestMatchOriginal, (826, 543))
        draw4 = ImageDraw.Draw(template)
        draw4.text((853, 880), str(thirdBestMatchOriginalPath), (0, 0, 0), font=font)
        thirdBestMatchOriginalScore = closestmatchlist[2][1]
        draw3.text((853, 910), "distance: " + str(thirdBestMatchOriginalScore), (0, 0, 0), font=font)

        template.save(directoryPath + "\\" + resultfilename)

        # root = Tk()
        # root.geometry('1280x960')
        # root.title("Top 3 results scorecard")
        # canvas = Canvas(root)
        # canvas.pack(side=TOP, expand=True, fill=BOTH)
        #
        # image = ImageTk.PhotoImage(template)
        # imagesprite = canvas.create_image(0, 0, image=image, anchor=NW)

        #
        #
        #

        workLoadCount = workLoadCount + 1

    # root.mainloop()

