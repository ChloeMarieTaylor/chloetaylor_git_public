<?php

// connect to the database
     include('connect.php');

$prime1 = 19; // this normally would be hidden as it is the primes
$prime2 = 29;

$modulus = $prime1 * $prime2;
$phiM = ($prime1 - 1)*($prime2 - 1); // not used but was needed to work out the keys

$decryptNumber = 101;  // this normally would be hidden as it is the unlock key
$encryptNumber = 5;
//

    $payload = $_POST["payload"];

    $payload = mysqli_real_escape_string($conn, $payload);

    $decoded = "";
    $buffer = "";

    $carry = 0;

    for ($i = 0; $i < strlen($payload); $i = $i + 1){

    $carry = $carry + 1;

    $buffer = $buffer.substr($payload, $i, 1);

    if ($carry == 2){
      $dec = hexdec($buffer);
      $add = utf8_decode(chr($dec));
      $decoded = $decoded.$add;
      $buffer = "";
      $carry = 0;
    }
  }

$data = json_decode($decoded);

$check = $data->check;
$intTemp = $data->intTemp;
$extTemp = $data->extTemp;
$heartRate = $data->heartRate;

$total = ($intTemp / 2) + ($extTemp * 5) + ($heartRate /4);

$checkStage2 = $check / $modulus;

$decryptedCheck = bcpowmod ($checkStage2, $decryptNumber, $modulus);

if ($decryptedCheck == $total && $total != 0){

$returnString = "";

if (mysqli_connect_errno())
   {
   $returnString = "connectionError";
   } else {

$sql = "INSERT INTO Wellbeing_monitor (IntTemp, ExtTemp, HeartRate) Values ('".$intTemp."','".$extTemp."','".$heartRate."')";



if ($conn->query($sql) === TRUE) {
    $returnString = "success";
} else {
    $returnString = "failure";
}

}

$conn->close();

} else {
  $returnString = "checkFailed";
}

echo json_encode($returnString);
?>
